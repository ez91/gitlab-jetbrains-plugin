import com.github.gradle.node.npm.task.NpmTask
import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.DetektCreateBaselineTask
import org.jetbrains.changelog.Changelog
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.dsl.KotlinVersion

// Read properties from gradle.properties to be used in here
fun properties(key: String) = project.findProperty(key)?.toString() ?: error("Property $key not found")

plugins {
  java
  jacoco
  alias(libs.plugins.jetbrains.kotlin.jvm)
  alias(libs.plugins.jetbrains.kotlin.serialization)
  alias(libs.plugins.jetbrains.intellij)
  alias(libs.plugins.jetbrains.changelog)
  alias(libs.plugins.gitlab.arturbosch.detekt)
  alias(libs.plugins.apollographql.apollo3)
  alias(libs.plugins.github.gmazzo.buildconfig)
  alias(libs.plugins.github.gradle.node)
}

group = "com.gitlab.plugin"

val alphaReleaseChannel = "alpha"
val alphaVersionSuffix = "-alpha.${System.getenv("CI_PIPELINE_IID")}+${System.getenv("CI_COMMIT_SHORT_SHA")}"
val releaseChannel = System.getenv("RELEASE_CHANNEL").orEmpty()
val versionSuffix = if (releaseChannel == alphaReleaseChannel) alphaVersionSuffix else ""
version = "${properties("plugin.version")}$versionSuffix"

val isLocalBuild = System.getenv("CI").isNullOrBlank()
val webviewSourceDir = "${project.projectDir}/webview"

repositories {
  mavenCentral()
  maven("https://packages.jetbrains.team/maven/p/ij/intellij-dependencies")
}

kotlin {
  compilerOptions {
    jvmTarget = JvmTarget.JVM_17
    languageVersion = KotlinVersion.KOTLIN_1_9
    apiVersion = KotlinVersion.KOTLIN_1_9
  }
}

buildConfig {
  packageName(group.toString())

  buildConfigField(
    "String",
    "RELEASE_CHANNEL",
    "\"$releaseChannel\""
  )

  buildConfigField(
    "String",
    "SNOWPLOW_COLLECTOR_URL",
    "\"${System.getenv("SNOWPLOW_COLLECTOR_URL") ?: "https://snowplow.trx.gitlab.net"}\""
  )

  buildConfigField(
    "Integer",
    "SNOWPLOW_EMITTER_BATCH_SIZE",
    System.getenv("SNOWPLOW_EMITTER_BATCH_SIZE")?.toInt() ?: 50
  )
}

detekt {
  buildUponDefaultConfig = true // preconfigure defaults
  allRules = true // activate all available (even unstable) rules.
  config.setFrom("detekt.yml")
}

tasks.withType<Detekt>().configureEach {
  jvmTarget = JavaVersion.VERSION_17.toString()

  exclude("com/gitlab/plugin/graphql/**") // ignore generated sources

  reports {
    html.required.set(true) // observe findings in your browser with structure and code snippets
  }
}

tasks.withType<DetektCreateBaselineTask>().configureEach {
  exclude("com/gitlab/plugin/graphql/**") // ignore generated sources
}

configurations.implementation {
  exclude("org.jetbrains.kotlinx", "kotlinx-coroutines-core")
}

dependencies {
  implementation(libs.apollo.graphql.runtime) {
    // okio is still on version 3.2.0 on the latest apollo3 version (3.8.4) which causes CVE-2023-3635 scan.
    exclude(group = "com.squareup.okio", module = "okio")
  }
  implementation(libs.kotlinx.datetime)
  implementation(libs.ktor.client.auth)
  implementation(libs.ktor.client.contentNegotiation)
  implementation(libs.ktor.client.logging)
  implementation(libs.ktor.client.okhttp)
  implementation(libs.ktor.serialization.gson)
  implementation(libs.ktor.serialization.kotlinxJson)
  implementation(libs.logback.classic)
  implementation(libs.snowplow.java.tracker) {
    capabilities {
      requireCapability("com.snowplowanalytics:snowplow-java-tracker-okhttp-support")
      api(libs.jackson.datatype)
    }
  }

  testImplementation(libs.apollo.graphql.mockserver)
  testImplementation(libs.apollo.graphql.testingSupport)
  testImplementation(libs.junit.jupiter)
  testImplementation(libs.junit.jupiter.api)
  testImplementation(libs.junit.jupiter.params)
  testImplementation(libs.kotest.assertions.core)
  testImplementation(libs.kotest.framework.datatest)
  testImplementation(libs.kotest.runner.junit5)
  testImplementation(libs.kotlin.test.junit)
  testImplementation(libs.kotlinx.coroutines.test)
  testImplementation(libs.ktor.client.mock)
  testImplementation(libs.mockk)
  testImplementation(libs.mockserver.junit.jupiter)
  testImplementation(libs.okhttp3.logging.interceptor)
  testImplementation(libs.remote.fixtures)
  testImplementation(libs.remote.robot)
  testImplementation(libs.turbine)
  testImplementation(libs.video.recorder.junit5) {
    exclude(group = "log4j", module = "log4j")
  }

  testRuntimeOnly(libs.junit.jupiter.engine)
  testRuntimeOnly(libs.junit.platform.launcher)
  testRuntimeOnly(libs.junit.platform.suite.engine)

  // Linting
  detektPlugins(libs.detekt.formatting)
}

apollo {
  service("GitLab") {
    generateDataBuilders.set(true)
    packageName.set("com.gitlab.plugin.graphql")
    introspection {
      endpointUrl = System.getenv("GITLAB_GRAPHQL_ENDPOINT") ?: "https://gitlab.com/api/graphql"
      schemaFile.set(file("src/main/graphql/schema.graphqls"))
    }

    fun mapGitLabScalar(
      graphQLName: String,
      targetName: String = "com.gitlab.plugin.graphql.scalars.${graphQLName}",
      expression: String = "${targetName}Adapter",
    ) = mapScalar(graphQLName, targetName, expression)

    mapGitLabScalar("AiModelID")
    mapGitLabScalar("NamespaceID")
    mapGitLabScalar("UserID")
  }
}

// Configure Gradle IntelliJ Plugin
// Read more: https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html
intellij {
  version = properties("platform.version")
  type = properties("platform.type")

  plugins = listOf("Git4Idea")
}

// Configure Gradle Changelog Plugin - read more: https://github.com/JetBrains/gradle-changelog-plugin
changelog {
  groups = listOf("Added", "Changed", "Removed", "Fixed")

  repositoryUrl =
    System.getenv("CI_PROJECT_URL") ?: "https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin"
}

node {
  download = !isLocalBuild
  version = properties("node.version")
  npmInstallCommand = "ci"
  nodeProjectDir = file(webviewSourceDir)
}

tasks {
  // Set the JVM compatibility versions
  withType<JavaCompile> {
    sourceCompatibility = properties("platform.java.version")
    targetCompatibility = properties("platform.java.version")
  }

  wrapper {
    gradleVersion = properties("gradle.version")
  }

  patchPluginXml {
    sinceBuild = properties("plugin.minBuild")
    untilBuild = properties("plugin.maxBuild")

    changeNotes = provider {
      changelog.renderItem(
        changelog
          .getLatest()
          .withHeader(false)
          .withEmptySections(false),
        Changelog.OutputType.HTML
      )
    }
  }

  signPlugin {
    certificateChain = System.getenv("CERTIFICATE_CHAIN")
    privateKey = System.getenv("PRIVATE_KEY")
    password = System.getenv("PRIVATE_KEY_PASSWORD")
  }

  publishPlugin {
    token = System.getenv("PUBLISH_TOKEN")
    channels = listOf(releaseChannel)
  }

  test {
    description = "Runs the test suite. Set project property `-Pe2eTests` to run the E2E tests."
    useJUnitPlatform()

    if (!project.hasProperty("e2eTests")) {
      filter {
        excludeTestsMatching("com.gitlab.plugin.e2eTest.*")
      }
    } else {
      testLogging {
        events("passed", "skipped", "failed", "standardOut", "standardError")
      }
      filter {
        includeTestsMatching("com.gitlab.plugin.e2eTest.*")
      }
    }
  }

  withType<Test> {
    configure<JacocoTaskExtension> {
      isIncludeNoLocationClasses = true
      excludes = listOf("jdk.internal.*")
    }
  }

  jacocoTestReport {
    reports {
      xml.required.set(true)
      html.required.set(false)
    }
    classDirectories.setFrom(instrumentCode)
  }

  jacocoTestCoverageVerification {
    classDirectories.setFrom(instrumentCode)
  }

  runPluginVerifier {
    ideVersions = listOf("${properties("platform.type")}-${properties("platform.version")}")
    localPaths = emptyList()
  }

  runIdeForUiTests {
    systemProperty("ide.mac.message.dialogs.as.sheets", "false")
    systemProperty("jb.privacy.policy.text", "<!--999.999-->")

    // https://youtrack.jetbrains.com/issue/IJPL-59368/IDE-crashes-due-to-chrome-sandbox-is-owned-by-root-and-has-mode-error-when-IDE-is-launching-the-JCEF-in-a-sandbox
    systemProperty("ide.browser.jcef.sandbox.enable", "false")

    // requirement to use JCefBrowserFixture
    systemProperty("ide.browser.jcef.jsQueryPoolSize", "10000")

    // to enable debugging of browser
    systemProperty("ide.browser.jcef.debug.port", -1)
    systemProperty("idea.is.internal", "true")
    systemProperty("ide.browser.jcef.contextMenu.devTools.enabled", "true")

    systemProperty("jb.consents.confirmation.enabled", "false")
    systemProperty("ide.mac.file.chooser.native", "false")
    systemProperty("jbScreenMenuBar.enabled", "false")
    systemProperty("apple.laf.useScreenMenuBar", "false")

    systemProperty("robot-server.port", "8082") // default port 8580
    systemProperty("idea.trust.all.projects", "true")
    systemProperty("ide.show.tips.on.startup.default.value", "false")

    // Avoid slow operations assertion https://youtrack.jetbrains.com/issue/IDEA-275489
    systemProperty("ide.slow.operations.assertion", "false")
  }

  downloadRobotServerPlugin {
    version = libs.versions.remoterobot.get()
  }

  // Build webview for Duo Chat
  register<NpmTask>("npmRunBuild") {
    dependsOn(npmInstall)
    npmCommand.set(listOf("run", "build"))
    workingDir.set(file(webviewSourceDir))
    inputs.files(fileTree(webviewSourceDir))
    outputs.dir("$webviewSourceDir/dist")
  }

  register<Copy>("copyWebviewAssets") {
    dependsOn("npmRunBuild")
    from("$webviewSourceDir/dist")
    into("${project.projectDir}/src/main/resources/webview")
  }

  withType<ProcessResources> {
    dependsOn("copyWebviewAssets")
  }
}
