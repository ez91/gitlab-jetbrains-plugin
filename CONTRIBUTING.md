# Contributing to the GitLab Plugin for JetBrains

## Developer Certificate of Origin + License

By contributing to GitLab B.V., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab B.V.
Except for the license granted herein to GitLab B.V. and recipients of software
distributed by GitLab B.V., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

All Documentation content that resides under the [docs/ directory](/docs) of this
repository is licensed under Creative Commons:
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

_This notice should stay as the first item in the CONTRIBUTING.md file._

---

Thank you for your interest in contributing to the GitLab Plugin for JetBrains! This guide details how to contribute
to this plugin in a way that is easy for everyone. These are mostly guidelines, not rules.
Use your best judgement, and feel free to propose changes to this document in a merge request.

## Code of Conduct

We want to create a welcoming environment for everyone who is interested in contributing.
Visit our [Code of Conduct page](https://about.gitlab.com/community/contribute/code-of-conduct/)
to learn more about our commitment to an open and welcoming environment.

## Get started

### Report issues

Create a [new issue from the "Bug" template](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/new?issuable_template=Bug)
and follow the instructions in the template.

### Propose features

Create a
[new issue from the "Feature Proposal" template](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/new?issuable_template=Feature%20Proposal)
and follow the instructions in the template.

### Your first code contribution?

New to the project? Look at issues [labeled with `quick win`](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues?label_name[]=quick%20win).

### Configure your development environment

For instructions on configuring your development environment, see [Development environment setup](docs/dev/development_environment_setup.md).

### Run the end-to-end tests

There are end-to-end tests that execute against a real IntelliJ instance. To run the end-to-end tests or to debug end-to-end test failures, see:
[End-to-end testing](docs/dev/end_to_end_testing.md).

### Add new icons

To add new icons to the plugin, add an SVG file (named exactly with the name of new icon)
to `src/main/resources/icons`.

### Open a merge request

Steps to opening a merge request to contribute code to the GitLab Plugin for Jetbrains is similar to any other open source project.
You develop in a separate branch of your own fork and the merge request should have a related issue open in the project.
Any Merge Request you wish to open in order to contribute to the GitLab Plugin for Jetbrains, be sure you have followed through the steps from [Configuring Development Environment](#configure-your-development-environment).
Use the [Suggested reviewers](https://docs.gitlab.com/ee/user/project/merge_requests/reviews/#suggested-reviewers) feature to find a reviewer when your MR is ready.

## Development Resources

For a list of useful resources related to plugin development and learning Kotlin, see [Development resources](docs/dev/development_resources.md).

## Becoming a maintainer

If you want to become a maintainer of this project, open a [maintainer onboarding issue](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/blob/main/.gitlab/issue_templates/Maintainer%20Onboarding.md), assign it to yourself, and follow the steps.
