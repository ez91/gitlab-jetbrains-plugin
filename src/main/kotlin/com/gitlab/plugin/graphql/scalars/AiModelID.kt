package com.gitlab.plugin.graphql.scalars

class AiModelID(gid: String) : GitLabScalar(gid)

val AiModelIDAdapter =
  GitLabScalarAdapter { AiModelID(it) }
