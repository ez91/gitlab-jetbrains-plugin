package com.gitlab.plugin.graphql.scalars

/**
 * A base class for serializing different scalars to/from JSON.
 */
open class GitLabScalar(val value: String) {
  override fun equals(other: Any?): Boolean {
    return other is UserID && other.value == value
  }

  override fun hashCode(): Int {
    return value.hashCode()
  }
}
