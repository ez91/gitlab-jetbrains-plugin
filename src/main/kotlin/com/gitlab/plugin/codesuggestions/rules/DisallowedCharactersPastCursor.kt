package com.gitlab.plugin.codesuggestions.rules

import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.util.TextRange

/**
Suggestions should not be provided unless one of the following is true:
- The cursor is at the end of the current line
- There are only special characters after the cursor on the current line

For more details, see https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/96.
 */
class DisallowedCharactersPastCursor(logger: Logger = Logger.getInstance(DisallowedCharactersPastCursor::class.java)) :
  BaseSkipRule(logger) {
  private val allowedCharactersPastCursor = """^\s*[)}\]"'`]*\s*[:{;,]?\s*$""".toRegex()

  override fun shouldSkipSuggestion(request: InlineCompletionRequest): Boolean {
    val currentPosition = request.endOffset
    val document = request.document
    val lineEndOffset = document.getLineEndOffset(document.getLineNumber(currentPosition))

    // Line end check
    if (currentPosition == lineEndOffset) return false

    val lineEndContent = document.getText(TextRange(currentPosition, lineEndOffset))

    // Allowed characters past cursor check
    if (allowedCharactersPastCursor.matches(lineEndContent)) return false

    log("disallowed characters past cursor")
    return true
  }
}
