package com.gitlab.plugin.codesuggestions.rules

import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.diagnostic.Logger

/**
Suggestions should not be provided for languages that are not supported by the model.
 */
class UnsupportedFileExtension(logger: Logger = Logger.getInstance(UnsupportedFileExtension::class.java)) :
  BaseSkipRule(logger) {
  companion object {
    val SUPPORTED_EXTENSIONS = setOf(
      "cpp", "c++", "cc", "cp", "cxx", // C++
      "cs", "csx", // C#
      "go", // Go
      "sql", // Google SQL
      "java", // Java
      "js", "jsx", // JavaScript
      "kt", "ktm", "kts", // Kotlin
      "md", // Markdown
      "php", "php3", "php4", "php5", "phps", "phpt", // PHP
      "py", // Python
      "rb", "ruby", // Ruby
      "rs", // Rust
      "scala", // Scala
      "swift", // Swift
      "ts", "tsx", // TypeScript
      "tf", "hcl", // Terraform/Terragrunt
      "sh", // Shell scripts
      "html", // HTML
      "css", // CSS
      // Below extensions are added for broad compatibility, but aren't explicitly
      // supported by the code suggestions service
      "haml",
      "handlebars;hbs;mustache",
      "svelte",
      "vue", // Vue.js
    )
  }

  override fun shouldSkipSuggestion(request: InlineCompletionRequest): Boolean {
    val extension = request.file.fileType.defaultExtension

    if (extension in SUPPORTED_EXTENSIONS) return false

    log("file extension \"$extension\" is not supported")
    return true
  }
}
