package com.gitlab.plugin.codesuggestions.telemetry

import com.gitlab.plugin.GitLabBundle
import com.intellij.openapi.application.ApplicationInfo
import com.intellij.openapi.application.ApplicationNamesInfo
import com.snowplowanalytics.snowplow.tracker.payload.SelfDescribingJson

data class IdeExtensionVersionContext(
  val ideName: String?,
  val ideVersion: String?,
  val ideVendor: String?,
  val extensionName: String?,
  val extensionVersion: String?
) {
  companion object {
    const val SCHEMA = "iglu:com.gitlab/ide_extension_version/jsonschema/1-0-0"

    fun build() = IdeExtensionVersionContext(
      ideName = ApplicationNamesInfo.getInstance().fullProductName,
      ideVersion = ApplicationInfo.getInstance().build.asString(),
      ideVendor = ApplicationInfo.getInstance().shortCompanyName,
      extensionName = GitLabBundle.plugin()?.name,
      extensionVersion = GitLabBundle.plugin()?.version
    ).toSelfDescribingJson()
  }

  fun toSelfDescribingJson() = SelfDescribingJson(
    SCHEMA,
    mapOf(
      "ide_name" to ideName,
      "ide_version" to ideVersion,
      "ide_vendor" to ideVendor,
      "extension_name" to extensionName,
      "extension_version" to extensionVersion
    )
  )
}
