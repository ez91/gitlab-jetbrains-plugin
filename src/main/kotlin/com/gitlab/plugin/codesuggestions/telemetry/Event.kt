package com.gitlab.plugin.codesuggestions.telemetry

data class Event(val type: Type, val context: Context) {
  val action by type::action

  enum class Type(val action: String) {
    ERROR("suggestion_error"),
    REQUESTED("suggestion_requested"),
    ACCEPTED("suggestion_accepted"),
    LOADED("suggestion_loaded"),
    CANCELLED("suggestion_cancelled"),
    NOT_PROVIDED("suggestion_not_provided"),
    SHOWN("suggestion_shown"),
    REJECTED("suggestion_rejected")
  }

  data class Context(
    val uuid: String,
    val requestId: String? = null,
    val modelEngine: String? = null,
    val modelName: String? = null,
    val language: String? = null,
    val prefixLength: Int? = null,
    val suffixLength: Int? = null,
    val apiStatusCode: Int? = null
  )
}
