package com.gitlab.plugin.codesuggestions.telemetry

import com.gitlab.plugin.codesuggestions.gutter.GutterIconInlineCompletionEventListener
import com.intellij.codeInsight.inline.completion.InlineCompletion
import com.intellij.openapi.editor.event.EditorFactoryEvent
import com.intellij.openapi.editor.event.EditorFactoryListener

internal class EditorListener : EditorFactoryListener {
  private val telemetryInlineCompletionEventListener = InlineCompletionEventListener()

  override fun editorCreated(event: EditorFactoryEvent) {
    val inlineCompletionHandler = InlineCompletion.getHandlerOrNull(event.editor)

    inlineCompletionHandler?.addEventListener(telemetryInlineCompletionEventListener)
    inlineCompletionHandler?.addEventListener(GutterIconInlineCompletionEventListener())
  }
}
