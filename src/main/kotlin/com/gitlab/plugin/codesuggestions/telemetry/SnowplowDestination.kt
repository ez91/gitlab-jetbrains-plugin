package com.gitlab.plugin.codesuggestions.telemetry

import com.snowplowanalytics.snowplow.tracker.Tracker
import com.snowplowanalytics.snowplow.tracker.events.Structured

class SnowplowDestination(private val tracker: Tracker) : Telemetry.Destination {
  companion object {
    const val EVENT_CATEGORY = "code_suggestions"
    private val IDE_EXTENSION_VERSION_CONTEXT by lazy { IdeExtensionVersionContext.build() }
  }

  override fun event(event: Event) {
    with(Structured.builder()) {
      category(EVENT_CATEGORY)
      action(event.action)
      label(event.context.uuid)
      customContext(
        listOf(
          IDE_EXTENSION_VERSION_CONTEXT,
          CodeSuggestionsContext.build(event.context)
        )
      )
      build()
    }.let {
      tracker.track(it)
    }
  }
}
