package com.gitlab.plugin.codesuggestions.telemetry

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.snowplowanalytics.snowplow.tracker.payload.SelfDescribingJson

data class CodeSuggestionsContext(
  val gitlabRealm: String,
  val language: String?,
  val modelEngine: String?,
  val modelName: String?,
  val prefixLength: Int?,
  val suffixLength: Int?,
  val apiStatusCode: Int?
) {
  companion object {
    const val SCHEMA = "iglu:com.gitlab/code_suggestions_context/jsonschema/2-4-0"

    fun build(context: Event.Context) = CodeSuggestionsContext(
      gitlabRealm = DuoPersistentSettings.getInstance().gitlabRealm(),
      language = context.language,
      modelEngine = context.modelEngine,
      modelName = context.modelName,
      prefixLength = context.prefixLength,
      suffixLength = context.suffixLength,
      apiStatusCode = context.apiStatusCode
    ).toSelfDescribingJson()
  }

  fun toSelfDescribingJson() = SelfDescribingJson(
    SCHEMA,
    mapOf(
      "gitlab_realm" to gitlabRealm,
      "language" to language,
      "model_engine" to modelEngine,
      "model_name" to modelName,
      "prefix_length" to prefixLength,
      "suffix_length" to suffixLength,
      "api_status_code" to apiStatusCode
    )
  )
}
