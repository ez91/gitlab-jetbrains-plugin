package com.gitlab.plugin.codesuggestions.license

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.ui.Notification
import com.intellij.openapi.diagnostic.logger

class CodeSuggestionsLicenseStatus(
  private val graphQLApi: GraphQLApi,
  private val onLicenseChanged: () -> Unit
) {
  companion object {
    fun licenseMissingNotification() = Notification(
      GitLabBundle.message("notification.title.gitlab-duo"),
      GitLabBundle.message("code-suggestions.not-licensed")
    )
  }

  var isLicensed: Boolean = true
  private val logger = logger<CodeSuggestionsLicenseStatus>()

  // The current user duoCodeSuggestionsAvailable field is experimental but marked as deprecated to raise awareness.
  @Suppress("DEPRECATION")
  suspend fun validateLicense() {
    logger.info("Checking Code Suggestions license")

    val currentUser = graphQLApi.getCurrentUser()
    if (currentUser?.duoCodeSuggestionsAvailable == true) {
      licensed()
      logger.info("License check using the current user succeeded")
    } else {
      notLicensed()
      logger.info("License check using the current user failed")
    }
  }

  private fun notLicensed() {
    logger.info("Code Suggestions license not found")

    isLicensed = false
    onLicenseChanged()
  }

  private fun licensed() {
    logger.info("Code Suggestions license found")

    isLicensed = true
    onLicenseChanged()
  }
}
