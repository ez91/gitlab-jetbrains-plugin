package com.gitlab.plugin.codesuggestions

import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.util.removePrefix
import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.lang.Language
import com.intellij.openapi.project.Project

@Suppress("UnstableApiUsage")
data class SuggestionContext(private val request: InlineCompletionRequest) {
  val document by lazy { request.document }

  val prefix: String by lazy {
    document
      .charsSequence
      .take(request.endOffset)
      .toString()
  }

  val suffix: String by lazy {
    document
      .charsSequence
      .drop(request.endOffset)
      .toString()
  }

  val isCurrentLineEmpty by lazy {
    document
      .getLineNumber(request.endOffset)
      .let { line -> document.text.lines()[line].isBlank() }
  }

  val project: Project? by lazy { request.editor.project }

  val extension: String by lazy { request.file.fileType.defaultExtension }
  val language: Language by lazy { request.file.language }

  fun toRequestPayload() = Completion.Payload(
    currentFile = Completion.Payload.CodeSuggestionRequestFile(
      fileName = removePrefix(request.file.virtualFile.path, request.editor.project?.basePath) ?: request.file.name,
      contentAboveCursor = prefix,
      contentBelowCursor = suffix
    ),
    telemetry = emptyList()
  )
}
