package com.gitlab.plugin.codesuggestions

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.codesuggestions.formatting.SuggestionFormatter
import com.gitlab.plugin.codesuggestions.rules.*
import com.gitlab.plugin.services.DuoEnabledProjectService
import com.gitlab.plugin.services.ProjectContextService
import com.gitlab.plugin.ui.CompletionStrategy
import com.gitlab.plugin.ui.GitLabIcons
import com.intellij.codeInsight.inline.completion.*
import com.intellij.codeInsight.inline.completion.elements.InlineCompletionGrayTextElement
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange
import kotlinx.coroutines.CancellationException
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import javax.swing.JComponent
import javax.swing.JLabel
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds

private val BLOCKED_CHARS = setOf("[]", "{}", "()")
val DEBOUNCE_DELAY = 300.milliseconds

@Suppress("UnstableApiUsage")
internal class SuggestionsProvider : DebouncedInlineCompletionProvider() {
  override val id = InlineCompletionProviderID(this::class.java.name)

  private val logger = logger<SuggestionsProvider>()
  private val api: DuoApi by lazy { ProjectContextService.instance.duoApi }
  private val completionStrategy: CompletionStrategy by lazy { ProjectContextService.instance.completionStrategy }

  private val formatter: SuggestionFormatter = SuggestionFormatter()

  private val skipSuggestionRules = listOf(
    PrefixTooShort(),
    UnsupportedFileExtension(),
    CursorOutOfBounds(),
    CurrentlyDisplayingCompletion(),
    DisallowedCharactersPastCursor()
  )
  private val customTooltipPresentation by lazy {
    object : InlineCompletionProviderPresentation {
      override fun getTooltip(project: Project?): JComponent {
        @Suppress("HardCodedStringLiteral")
        return JLabel("GitLab Duo Code Suggestions", GitLabIcons.NotificationIcon, JLabel.LEFT)
      }
    }
  }

  override val providerPresentation: InlineCompletionProviderPresentation
    get() = customTooltipPresentation

  override suspend fun getSuggestionDebounced(request: InlineCompletionRequest): InlineCompletionSuggestion {
    if (skipSuggestionRules.any { it.shouldSkipSuggestion(request) }) return InlineCompletionSuggestion.empty()

    val context = SuggestionContext(request)
    val suggestionRetrievalTime = Clock.System.now()

    val response = completionStrategy.generateCompletions(context.toRequestPayload())
      ?: return InlineCompletionSuggestion.empty()

    val suggestionRetrievalDuration = Clock.System.now() - suggestionRetrievalTime

    if (response.choices.isEmpty()) {
      logger.debug("No suggestion was returned by the Code Suggestions API")
      return InlineCompletionSuggestion.empty()
    }

    logger.debug(
      "Retrieved suggestion for the request sent at (${suggestionRetrievalTime.toLocalDateTime(TimeZone.UTC)}) " +
        "with the duration of ($suggestionRetrievalDuration ms)"
    )

    return InlineCompletionSuggestion.withFlow {
      emit(InlineCompletionGrayTextElement(formatter.format(response.firstSuggestion, context)))
    }
  }

  override suspend fun getDebounceDelay(request: InlineCompletionRequest): Duration = DEBOUNCE_DELAY

  override fun shouldBeForced(request: InlineCompletionRequest): Boolean = false

  override suspend fun getSuggestion(request: InlineCompletionRequest): InlineCompletionSuggestion {
    return try {
      super.getSuggestion(request)
    } catch (e: CancellationException) {
      // Workaround to catch JobCancellationException thrown due to job cancel due to debounce.
      InlineCompletionSuggestion.empty()
    }
  }

  override fun isEnabled(event: InlineCompletionEvent): Boolean = !withinBoundingChar(event) && api.isApiEnabled() &&
    isDuoEnabledAtProjectLevel(event)

  private fun isDuoEnabledAtProjectLevel(event: InlineCompletionEvent): Boolean {
    val project = event.toRequest()?.editor?.project
      ?: return true

    return project.service<DuoEnabledProjectService>().isDuoEnabled()
  }

  private fun withinBoundingChar(event: InlineCompletionEvent): Boolean {
    val request = event.toRequest() ?: return false

    val currentPosition = request.endOffset
    return currentPosition > 1 &&
      BLOCKED_CHARS.contains(request.document.getText(TextRange(currentPosition - 2, currentPosition)))
  }
}
