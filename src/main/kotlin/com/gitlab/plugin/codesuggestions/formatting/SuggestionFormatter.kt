package com.gitlab.plugin.codesuggestions.formatting

import com.gitlab.plugin.codesuggestions.SuggestionContext
import com.intellij.openapi.application.readAction
import com.intellij.openapi.command.WriteCommandAction.runWriteCommandAction
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiFileFactory
import com.intellij.psi.codeStyle.CodeStyleManager

class SuggestionFormatter {
  private val logger = logger<SuggestionFormatter>()

  @Suppress("ReturnCount")
  suspend fun format(
    suggestion: String,
    context: SuggestionContext
  ): String {
    val trimmedSuggestion = when {
      context.isCurrentLineEmpty -> suggestion.trimStart('\n', '\t', ' ').trimEnd('\n', '\t', ' ')
      else -> suggestion.trimEnd('\n', '\t', ' ')
    }

    if (trimmedSuggestion.isInlineCompletion()) {
      return trimmedSuggestion
    }

    val project = context.project
      ?: return trimmedSuggestion

    val completionPart = trimmedSuggestion.lines().first() + '\n' // re-add newline to completion part
    val suggestionPart = trimmedSuggestion.drop(completionPart.length)

    val content = StringBuilder().apply {
      append(context.prefix)
      append(completionPart)
      append(suggestionPart)
      append(context.suffix)
    }

    val psiFile = readAction {
      PsiFileFactory
        .getInstance(project)
        .createFileFromText(context.language, content.toString())
    }

    val suggestionStartOffset = context.prefix.length + completionPart.length
    val suggestionEndOffset = context.prefix.length + trimmedSuggestion.length + 1 // format until after the suggestion
    runWriteCommandAction(project) {
      CodeStyleManager
        .getInstance(project)
        .adjustLineIndent(psiFile, TextRange(suggestionStartOffset, suggestionEndOffset))
    }

    val psiFileDocument = PsiDocumentManager.getInstance(project).getDocument(psiFile)
    if (psiFileDocument == null) {
      logger.error("Generated PsiFile has no document.")
      return trimmedSuggestion
    }

    val indentedSuggestionEndOffset = psiFileDocument
      .getLineNumber(suggestionStartOffset)
      .let { startLine -> startLine - 1 + suggestionPart.lines().size }
      .let { endLine -> psiFileDocument.getLineEndOffset(endLine) }

    return completionPart + psiFile.extractSuggestion(suggestionStartOffset, indentedSuggestionEndOffset)
  }

  private fun PsiFile.extractSuggestion(suggestionStartOffset: Int, suggestionEndOffset: Int): String {
    return text.filterIndexed { index, _ -> index in suggestionStartOffset until suggestionEndOffset }
  }

  private fun String.isInlineCompletion() = lines().size == 1
}
