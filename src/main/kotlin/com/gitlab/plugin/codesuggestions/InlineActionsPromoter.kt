package com.gitlab.plugin.codesuggestions

import com.intellij.codeInsight.inline.completion.InsertInlineCompletionAction
import com.intellij.codeInsight.inline.completion.session.InlineCompletionContext
import com.intellij.openapi.actionSystem.ActionPromoter
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.actionSystem.DataContext

private class InlineActionsPromoter : ActionPromoter {
  override fun promote(actions: List<AnAction>, context: DataContext): List<AnAction?> {
    val editor = CommonDataKeys.EDITOR.getData(context)

    if (editor?.let { InlineCompletionContext.getOrNull(it) } == null) {
      return emptyList()
    }

    return actions.filterIsInstance<InsertInlineCompletionAction>()
  }
}
