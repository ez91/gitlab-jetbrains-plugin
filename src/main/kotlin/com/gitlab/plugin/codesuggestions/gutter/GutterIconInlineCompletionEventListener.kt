package com.gitlab.plugin.codesuggestions.gutter

import com.gitlab.plugin.services.DuoContextService
import com.intellij.codeInsight.inline.completion.InlineCompletionEventAdapter
import com.intellij.codeInsight.inline.completion.InlineCompletionEventType
import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.editor.markup.HighlighterLayer
import com.intellij.openapi.editor.markup.RangeHighlighter

@Suppress("UnstableApiUsage")
class GutterIconInlineCompletionEventListener : InlineCompletionEventAdapter {
  private var highlighter: RangeHighlighter? = null

  @Suppress("UnnecessaryApply")
  override fun onShow(event: InlineCompletionEventType.Show) {
    highlighter?.apply { gutterIconRenderer = GutterIcons.Ready }
  }

  override fun onHide(event: InlineCompletionEventType.Hide) {
    highlighter?.dispose()
  }

  override fun onRequest(event: InlineCompletionEventType.Request) {
    highlighter?.dispose()

    if (!DuoContextService.instance.duoSettings.codeSuggestionsEnabled) {
      return
    }

    highlighter = event
      .request
      .editor
      .markupModel
      .addLineHighlighter(event.request.currentLine, HighlighterLayer.CARET_ROW, null)
      .apply { gutterIconRenderer = GutterIcons.Loading }
  }

  private val InlineCompletionRequest.currentLine: Int
    get() = document.getLineNumber(endOffset)
}
