package com.gitlab.plugin.codesuggestions.gutter

import com.intellij.openapi.editor.markup.GutterIconRenderer
import com.intellij.openapi.util.IconLoader
import javax.swing.Icon

object GutterIcons {
  object Loading : BaseCodeSuggestionGutterIconRenderer() {
    override fun getIcon(): Icon {
      return IconLoader.getIcon("/icons/gitlab-code-suggestions-loading.svg", javaClass)
    }
  }

  object Ready : BaseCodeSuggestionGutterIconRenderer() {
    override fun getIcon(): Icon {
      return IconLoader.getIcon("/icons/gitlab-code-suggestions-enabled.svg", javaClass)
    }
  }
}

abstract class BaseCodeSuggestionGutterIconRenderer : GutterIconRenderer() {
  // GutterIconRenderer enforces override of equals and hash code
  override fun equals(other: Any?): Boolean {
    return (other != null && other is GutterIconRenderer && other.icon == icon)
  }

  override fun hashCode(): Int {
    return icon.hashCode()
  }
}
