package com.gitlab.plugin.chat.view

import com.gitlab.plugin.actions.chat.DUO_CHAT_TOOL_WINDOW_ID
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.chat.view.abstraction.ChatBrowser
import com.gitlab.plugin.chat.view.abstraction.ChatView
import com.gitlab.plugin.chat.view.model.ChatViewMessage
import com.gitlab.plugin.chat.view.model.ClearChatMessage
import com.gitlab.plugin.chat.view.model.NewRecordMessage
import com.gitlab.plugin.chat.view.model.UpdateRecordMessage
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.wm.ToolWindowManager
import io.ktor.http.*

open class WebViewChatView(
  private val chatBrowser: ChatBrowser,
  private val toolWindowManager: ToolWindowManager,
  private val logger: Logger = logger<WebViewChatView>()
) : ChatView {
  companion object {
    const val RESOURCE_PATH_PREFIX = "webview/"
  }

  private var messageHandler: (message: ChatViewMessage) -> Unit = {}

  init {
    chatBrowser.addLocalResources()
    chatBrowser.onPostMessageReceived { json ->
      val message = ChatViewMessage.fromJson(json)
      messageHandler(message)
    }
  }

  override fun show() {
    if (DuoPersistentSettings.getInstance().duoChatEnabled) {
      ApplicationManager.getApplication().invokeLater {
        toolWindowManager.getToolWindow(DUO_CHAT_TOOL_WINDOW_ID)?.show()
          ?: logger.error("Tool window (${DUO_CHAT_TOOL_WINDOW_ID} unable to be shown.")
      }
    } else {
      ApplicationManager.getApplication().invokeLater {
        toolWindowManager.getToolWindow(DUO_CHAT_TOOL_WINDOW_ID)?.hide()
          ?: logger.error("Cannot retrieve (${DUO_CHAT_TOOL_WINDOW_ID}.")
      }
    }
  }

  override fun clear() = postMessage(ClearChatMessage)
  override fun addRecord(message: NewRecordMessage) = postMessage(message)
  override fun updateRecord(message: UpdateRecordMessage) = postMessage(message)
  override fun onMessage(block: (message: ChatViewMessage) -> Unit) {
    messageHandler = block
  }

  private fun postMessage(payload: ChatViewMessage) = chatBrowser.postMessage(payload.toJson())
  private fun ChatBrowser.addLocalResources() {
    addLocalResource("index.html", RESOURCE_PATH_PREFIX, ContentType.Text.Html.toString())
    addLocalResource("assets/app.js", RESOURCE_PATH_PREFIX, ContentType.Text.JavaScript.toString())
    addLocalResource("assets/index.css", RESOURCE_PATH_PREFIX, ContentType.Text.CSS.toString())
    addLocalResource(
      "assets/empty-activity-md.svg",
      RESOURCE_PATH_PREFIX,
      ContentType.Image.SVG.toString()
    )
    addLocalResource("assets/icons.svg", RESOURCE_PATH_PREFIX, ContentType.Image.SVG.toString())
  }
}
