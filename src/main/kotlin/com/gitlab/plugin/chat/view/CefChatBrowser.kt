package com.gitlab.plugin.chat.view

import com.gitlab.plugin.chat.view.abstraction.ChatBrowser
import com.gitlab.plugin.util.webview.*
import com.intellij.ide.ui.LafManagerListener
import com.intellij.ui.jcef.JBCefBrowser
import com.intellij.ui.jcef.JBCefBrowserBase
import com.intellij.ui.jcef.JBCefJSQuery
import com.intellij.ui.jcef.JCEFHtmlPanel
import com.intellij.util.application
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

private const val HOSTNAME = "localhost"
private const val PROTOCOL = "http"
private const val CHAT_URL = "http://localhost/index.html"

internal class CefChatBrowser : ChatBrowser {

  val browserComponent
    get() = browser.component

  private val browser = JCEFHtmlPanel(CHAT_URL)
  private val localRequestHandler = CefLocalRequestHandler(PROTOCOL, HOSTNAME)
  private var postMessageHandler: (String) -> Unit = {}

  init {
    browser.installIdeApi()
    browser.jbCefClient.addRequestHandler(localRequestHandler, browser.cefBrowser)
    browser.setInitialTheme()
    subscribeToThemeChanges()
  }

  override fun dispose() = browser.dispose()

  override fun addLocalResource(path: String, resourcePathPrefix: String, mimeType: String) {
    val resourceStream = javaClass.getResourceAsStream("/$resourcePathPrefix$path")
    checkNotNull(resourceStream) { "Failed to load $path in $resourcePathPrefix" }

    localRequestHandler.addResource("/$path") {
      CefStreamResourceHandler(resourceStream, mimeType, this@CefChatBrowser)
    }
  }

  @Serializable
  data class DetailPayload(val detail: String)

  override fun postMessage(payload: String) {
    // Serialize your payload object
    val detailPayload = DetailPayload(payload)
    val jsonPayload = Json.encodeToString(DetailPayload.serializer(), detailPayload)

    // Now jsonPayload contains the entire serialized object including the key 'detail'
    browser.executeJavaScript(
      """
        window.dispatchEvent(
          new CustomEvent('message', $jsonPayload)
        )
      """.trimIndent()
    )
  }

  override fun onPostMessageReceived(block: (String) -> Unit) {
    postMessageHandler = block
  }

  private fun onPostMessageReceived(payload: String) = postMessageHandler(payload)
  private fun setTheme() {
    val themeColors = ThemeInfoPayload.buildJson()
    browser.executeJavaScript(
      """
        window.dispatchEvent(
          new CustomEvent('GitLabDuoThemeChanged', {detail: {themeColors: $themeColors}}))
      """.trimMargin()
    )
  }

  private fun subscribeToThemeChanges() {
    application.messageBus.connect()
      .subscribe(
        LafManagerListener.TOPIC,
        LafManagerListener {
          setTheme()
        }
      )
  }

  private fun JBCefBrowser.installIdeApi() {
    // Query must be created before the browser launches
    val query =
      JBCefJSQuery
        .create(browser as JBCefBrowserBase)
        .addHandler(::onPostMessageReceived)

    addLoadEndHandler {
      executeJavaScript(
        """
        window['intellij'] = {
          postMessage: (payload) => {
            ${query.inject("JSON.stringify(payload)")}
          }
        }
        """.trimIndent()
      )
    }
  }

  private fun JBCefBrowser.setInitialTheme() = addLoadEndHandler {
    setTheme()
  }
}
