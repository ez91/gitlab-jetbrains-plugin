package com.gitlab.plugin.chat.api.abstraction

import com.gitlab.plugin.chat.api.model.AiActionResponse
import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.model.ChatRecordContext

interface ChatApiClient {
  suspend fun processNewUserPrompt(
    subscriptionId: String,
    question: String,
    context: ChatRecordContext? = null
  ): AiActionResponse?

  suspend fun subscribeToUpdates(
    subscriptionId: String,
    onMessageReceived: suspend (message: AiMessage) -> Unit
  )
  suspend fun clearChat(subscriptionId: String): AiActionResponse?
}
