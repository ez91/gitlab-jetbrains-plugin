package com.gitlab.plugin.chat.api.model

data class AiMessage(
  val requestId: String,
  val role: String,
  val content: String,
  val contentHtml: String,
  val timestamp: String,
  val errors: List<String> = emptyList(),
  val chunkId: Int? = null
)
