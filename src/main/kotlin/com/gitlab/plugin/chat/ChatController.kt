package com.gitlab.plugin.chat

import com.gitlab.plugin.chat.api.abstraction.ChatApiClient
import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.exceptions.ChatException
import com.gitlab.plugin.chat.extensions.fromSelectedEditor
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.chat.telemetry.trackFeedback
import com.gitlab.plugin.chat.view.abstraction.ChatView
import com.gitlab.plugin.chat.view.model.*
import com.gitlab.plugin.services.GitLabUserService
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import com.intellij.util.application
import io.ktor.client.plugins.*
import kotlinx.coroutines.*
import kotlinx.datetime.LocalDateTime
import java.util.*

class ChatController(
  private val chatApiClient: ChatApiClient,
  private val chatView: ChatView,
  private val project: Project,
  private val chatHistory: ChatHistory = ChatHistory(),
  private val logger: Logger = logger<ChatController>(),
  private val dispatcher: CoroutineDispatcher = Dispatchers.Default,
  private val userService: GitLabUserService = application.service<GitLabUserService>()
) {
  init {
    chatView.onMessage { message ->
      CoroutineScope(dispatcher).launch {
        handleViewOnMessage(message)
      }
    }
  }

  suspend fun clearChatWindow(subscriptionId: String) {
    try {
      val actionResponse = chatApiClient.clearChat(subscriptionId) ?: return
      if (actionResponse.aiAction.errors.isEmpty()) {
        chatHistory.clear()
        chatView.clear()
      }
    } catch (e: ResponseException) {
      logger.error(e.message)
    }
  }

  suspend fun processNewUserPrompt(prompt: NewUserPromptRequest) {
    val subscriptionId = UUID.randomUUID().toString()
    val user = userService.getCurrentUserId()

    // Hack: wait until startup finishes and userId is fetched.
    // Subscribing to responses from duo chat requires a userId. If we do not
    // wait here then we risk timing out later before we can subscribe to updates.
    var retryTimeout: Long = RETRY_TIMEOUT_MILLIS
    while (user == null && retryTimeout > 0) {
      retryTimeout -= RETRY_INTERVAL_MILLIS
      delay(RETRY_INTERVAL_MILLIS)
    }

    val actionResponse = chatApiClient.processNewUserPrompt(
      subscriptionId = subscriptionId,
      question = prompt.content,
      context = prompt.context
    ) ?: return

    if (actionResponse.aiAction.errors.isNotEmpty()) {
      throw ChatException("Error processing new user prompt: ${actionResponse.aiAction.errors.joinToString(", ")}")
    }

    val record = ChatRecord(
      id = subscriptionId,
      role = ChatRecord.Role.USER,
      type = prompt.type,
      state = ChatRecord.State.READY,
      requestId = actionResponse.aiAction.requestId,
      context = prompt.context,
      content = prompt.content,
      contentHtml = null,
      extras = null
    )

    record.addToChat()
    chatView.show()

    if (record.type == ChatRecord.Type.CLEAR_CHAT) {
      clearChatWindow(subscriptionId)
      return
    }

    // if the chat starts a new conversation, we don't need an assistant response
    if (record.type != ChatRecord.Type.NEW_CONVERSATION) {
      ChatRecord.pendingAssistantResponse(record).let { assistantChatRecord ->
        assistantChatRecord.addToChat()
        chatApiClient.subscribeToUpdates(subscriptionId, ::handleAiMessageUpdate)
      }
    }
  }

  private suspend fun handleViewOnMessage(message: ChatViewMessage) = when (message) {
    is AppReadyMessage -> {
      chatHistory.records.forEach { chatView.addRecord(NewRecordMessage(it)) }
    }

    is NewPromptMessage -> {
      processNewUserPrompt(
        NewUserPromptRequest(
          content = message.content,
          context = ChatRecordContext.fromSelectedEditor(project)
        )
      )
    }

    is TrackFeedbackMessage -> {
      trackFeedback(message)
    }

    else -> logger.warn("Unhandled chat-webview message: $message")
  }

  private fun handleAiMessageUpdate(message: AiMessage) {
    val record = chatHistory.findViaRequestId(message.requestId, ChatRecord.Role.fromValue(message.role))
    if (record == null) {
      logger.warn("No record found for requestId: ${message.requestId}")
      return
    }

    record.chunkId = message.chunkId
    record.content = message.content
    record.contentHtml = message.contentHtml
    record.errors.addAll(message.errors)

    val timestamp = runCatching { LocalDateTime.parse(message.timestamp) }.getOrNull()
    if (timestamp != null) record.timestamp = timestamp

    record.state = ChatRecord.State.READY
    chatView.updateRecord(UpdateRecordMessage(record))
  }

  private fun ChatRecord.addToChat() {
    chatHistory.addRecord(this)
    chatView.addRecord(NewRecordMessage(this))
  }

  companion object {
    private const val RETRY_TIMEOUT_MILLIS: Long = 10000
    private const val RETRY_INTERVAL_MILLIS: Long = 100
  }
}
