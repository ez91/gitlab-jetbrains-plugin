package com.gitlab.plugin.workspace

import com.intellij.util.messages.Topic
import com.intellij.util.messages.Topic.AppLevel

interface DidChangeConfigurationListener {
  companion object {
    @AppLevel
    val DID_CHANGE_CONFIGURATION_TOPIC = Topic(
      DidChangeConfigurationListener::class.java.name,
      DidChangeConfigurationListener::class.java
    )
  }

  fun onConfigurationChange(params: DidChangeConfigurationParams)
}
