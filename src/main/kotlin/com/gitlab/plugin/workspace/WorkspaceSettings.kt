package com.gitlab.plugin.workspace

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.Notification
import com.gitlab.plugin.ui.NotificationAction
import com.gitlab.plugin.ui.NotificationGroupType
import com.gitlab.plugin.util.LINKS.JETBRAINS_1PASSWORD_CLI_INTEGRATION_URL
import com.intellij.execution.ExecutionException
import com.intellij.execution.configurations.GeneralCommandLine
import com.intellij.execution.util.ExecUtil
import com.intellij.notification.NotificationType
import com.intellij.openapi.diagnostic.logger

// Using "Copy Secret Reference" in the 1Password app includes quotes so use a capture group to get the reference
// op://<vault-name>/<item-name>[/<section-name>]/<field-name>
val OP_SECRET_REFERENCE_PATTERN = Regex(
  "^(?:" +
    "\"(op://[^/]+/[^/]+(?:/[^/]+)?/[^/\"]+)\"" +
    "|" +
    "(op://[^/]+/[^/]+(?:/[^/]+)?/[^/\"]+)" +
    ")$"
)

data class WorkspaceSettings(
  val url: String,
  val codeSuggestionEnabled: Boolean,
  val duoChatEnabled: Boolean,
  val token: TokenSettings
)

sealed interface TokenSettings {
  fun getToken(): String

  /**
   * The plaintext personal access token via a keychain.
   */
  data class Keychain(private val value: String) : TokenSettings {
    private val logger = logger<Keychain>()

    override fun getToken(): String {
      logger.debug("Reading personal access token from system keychain.")
      return value
    }
  }

  /**
   * Holds a 1password-cli secret reference.
   */
  data class OPSecretReference(private val reference: String) : TokenSettings {
    private val logger = logger<OPSecretReference>()
    private val secretReference = get1PasswordSecretReference(reference)

    override fun getToken(): String = secretReference?.let {
      var token = ""
      try {
        val commandLine = GeneralCommandLine("op", "read", secretReference)
        val output = ExecUtil.execAndGetOutput(commandLine)

        if (output.exitCode == 0) {
          token = output.stdoutLines.firstOrNull().orEmpty()
        } else {
          logger.warn("Unable to read secret reference 'op read $secretReference' had an exit of ${output.exitCode}")
          display1PasswordIntegrationNotification(
            "Unable to read 1Password CLI secret reference. See idea.log for more details."
          )
        }
      } catch (ex: ExecutionException) {
        logger.warn("Unable to run 1password CLI", ex)

        display1PasswordIntegrationNotification(
          "Failed to execute command: op read $secretReference\nSee idea.log for more details."
        )
      }

      token
    }.orEmpty()

    private fun display1PasswordIntegrationNotification(message: String) {
      GitLabNotificationManager().sendNotification(
        Notification(
          title = GitLabBundle.message("notification.title.gitlab-duo"),
          message = message,
          actions = listOf(
            NotificationAction.link(
              title = GitLabBundle.message("settings.ui.gitlab.1password-cli-gitlab-docs-label"),
              url = JETBRAINS_1PASSWORD_CLI_INTEGRATION_URL,
            )
          )
        ),
        NotificationGroupType.IMPORTANT,
        NotificationType.ERROR
      )
    }

    private fun get1PasswordSecretReference(reference: String): String? = OP_SECRET_REFERENCE_PATTERN.find(reference)
      ?.groups
      ?.filterNotNull()
      ?.lastOrNull()
      ?.value
  }
}
