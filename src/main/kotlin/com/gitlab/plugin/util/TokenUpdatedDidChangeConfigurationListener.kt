package com.gitlab.plugin.util

import com.gitlab.plugin.api.duo.PatProvider
import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.services.ProjectContextService
import com.gitlab.plugin.workspace.DidChangeConfigurationListener
import com.gitlab.plugin.workspace.DidChangeConfigurationParams
import com.gitlab.plugin.workspace.TokenSettings
import com.intellij.openapi.components.service
import com.intellij.openapi.progress.runBackgroundableTask
import com.intellij.util.application
import kotlinx.coroutines.runBlocking

class TokenUpdatedDidChangeConfigurationListener(
  private val userService: GitLabUserService = application.service<GitLabUserService>(),
  private val patProvider: PatProvider = application.service<PatProvider>()
) : DidChangeConfigurationListener {

  override fun onConfigurationChange(params: DidChangeConfigurationParams) {
    when (val tokenSettings = params.settings.token) {
      is TokenSettings.Keychain -> TokenUtil.setToken(tokenSettings.getToken())
      is TokenSettings.OPSecretReference -> TokenUtil.setToken("")
    }

    runBackgroundableTask(title = "Refreshing GitLab personal access token status", cancellable = false) {
      patProvider.cacheToken(params.settings.token)

      runBlocking {
        userService.invalidateAndFetchCurrentUser()
      }

      if (ProjectContextService.isInitialized()) {
        gitlabStatusRefresh()
      }
    }
  }
}
