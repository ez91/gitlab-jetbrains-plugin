package com.gitlab.plugin.util.webview

import com.intellij.ui.jcef.JBCefBrowser
import org.cef.browser.CefBrowser
import org.cef.browser.CefFrame
import org.cef.handler.CefLoadHandlerAdapter

fun JBCefBrowser.addLoadEndHandler(block: () -> Unit) {
  jbCefClient.addLoadHandler(
    object : CefLoadHandlerAdapter() {
      override fun onLoadEnd(browser: CefBrowser, frame: CefFrame, httpStatusCode: Int) {
        block()
      }
    },
    cefBrowser
  )
}

fun JBCefBrowser.executeJavaScript(code: String, source: String? = null, line: Int = 0) =
  cefBrowser.executeJavaScript(code, source, line)
