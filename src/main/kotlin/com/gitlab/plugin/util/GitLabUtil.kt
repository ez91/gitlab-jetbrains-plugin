package com.gitlab.plugin.util

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.GitLabSettingsChangeEvent
import com.gitlab.plugin.GitLabSettingsListener
import com.gitlab.plugin.services.DuoContextService
import com.gitlab.plugin.services.ProjectContextService
import com.intellij.collaboration.api.httpclient.HttpClientUtil
import com.intellij.ide.BrowserUtil
import com.intellij.openapi.application.ApplicationManager

object GitLabUtil {
  const val SERVICE_NAME = "com.gitlab.plugin"
  const val GITLAB_DEFAULT_URL = "https://gitlab.com"
  const val API_PAT_SCOPE = "api"

  val userAgent by lazy {
    HttpClientUtil.getUserAgentValue("gitlab-jetbrains-plugin/${GitLabBundle.plugin()?.version ?: "DEV"}")
  }

  fun browseUrl(url: String) = BrowserUtil.browse(url)
}

enum class Status {
  ENABLED,
  DISABLED,
  ERROR,
  LOADING,
  NOT_LICENSED,
  GITLAB_VERSION_UNSUPPORTED
}

fun gitlabStatusChangedNotify(status: Status) {
  ApplicationManager.getApplication().messageBus.syncPublisher(GitLabSettingsListener.SETTINGS_CHANGED)
    .codeStyleSettingsChanged(GitLabSettingsChangeEvent(status))
}

fun gitlabStatusError() {
  gitlabStatusChangedNotify(Status.ERROR)
}

fun gitlabStatusLoading() {
  gitlabStatusChangedNotify(Status.LOADING)
}

fun gitlabStatusEnabled() {
  gitlabStatusChangedNotify(Status.ENABLED)
}

fun gitlabStatusDisabled() {
  gitlabStatusChangedNotify(Status.DISABLED)
}

fun gitlabStatusNotLicensed() {
  gitlabStatusChangedNotify(Status.NOT_LICENSED)
}

fun gitlabStatusVersionUnsupported() {
  gitlabStatusChangedNotify(Status.GITLAB_VERSION_UNSUPPORTED)
}

fun gitlabStatusRefresh() {
  val duoContext = DuoContextService.instance
  val projectContext = ProjectContextService.instance

  when {
    !projectContext.codeSuggestionsLicense.isLicensed -> {
      gitlabStatusNotLicensed()
    }

    duoContext.serverServiceWithoutCallbacks.get().isVersionUpgradeRequired -> {
      gitlabStatusVersionUnsupported()
    }

    duoContext.isDuoConfigured() && duoContext.isDuoEnabled() -> {
      gitlabStatusEnabled()
    }

    else -> {
      gitlabStatusDisabled()
    }
  }
}

object LINKS {
  const val CODE_SUGGESTIONS_SETUP_DOCS_URL =
    "https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-on-gitlab-saas"
  const val JETBRAINS_1PASSWORD_CLI_INTEGRATION_URL =
    "https://docs.gitlab.com/ee/editor_extensions/jetbrains_ide/index.html#integrate-with-1password-cli"

  const val CREATE_TOKEN_PATH =
    "/-/profile/personal_access_tokens?name=GitLab%20Duo%20For%20JetBrains&scopes=api"
}
