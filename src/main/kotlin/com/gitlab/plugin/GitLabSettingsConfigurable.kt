package com.gitlab.plugin

import com.gitlab.plugin.authentication.DEFAULT_GITLAB_1PASSWORD_SECRET_REFERENCE
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.services.health.HealthCheckRequest
import com.gitlab.plugin.settings.SettingsHealthChecker
import com.gitlab.plugin.util.LINKS
import com.gitlab.plugin.util.TokenUtil
import com.gitlab.plugin.util.removeTrailingSlash
import com.gitlab.plugin.workspace.*
import com.intellij.ide.BrowserUtil
import com.intellij.ide.DataManager
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.application.invokeLater
import com.intellij.openapi.options.BoundConfigurable
import com.intellij.openapi.options.Configurable
import com.intellij.openapi.options.ConfigurationException
import com.intellij.openapi.ui.DialogPanel
import com.intellij.ui.SimpleTextAttributes.GRAY_ATTRIBUTES
import com.intellij.ui.components.BrowserLink
import com.intellij.ui.components.JBCheckBox
import com.intellij.ui.components.JBPasswordField
import com.intellij.ui.dsl.builder.*
import com.intellij.ui.dsl.builder.Align.Companion.FILL
import com.intellij.ui.dsl.builder.Cell
import com.intellij.ui.dsl.builder.bindSelected
import com.intellij.ui.dsl.builder.bindText
import com.intellij.ui.dsl.builder.panel
import com.intellij.ui.layout.not
import com.intellij.util.application
import com.intellij.util.ui.StatusText
import java.util.*
import javax.swing.JLabel
import javax.swing.JTextField

class GitLabSettingsConfigurable : BoundConfigurable(GitLabBundle.message("settings.ui.group.name")) {

  private val settings = DuoPersistentSettings.getInstance()

  /**
   * Leaving this [tech debt](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/357)
   * here as a future improvement given this will require a larger refactor.
   */
  private var tokenText = TokenUtil.getToken() ?: ""
  private var urlText = settings.url
  private var codeSuggestionEnabled = settings.codeSuggestionsEnabled
  private var duoChatEnabled = settings.duoChatEnabled

  private val bundle: ResourceBundle = ResourceBundle.getBundle("messages.GitLabBundle")
  private val urlErrorMessage = bundle.getString("settings.ui.error.url.invalid")
  private val tokenLabel = bundle.getString("settings.ui.gitlab.token")
  private val createTokenLabel = bundle.getString("settings.ui.gitlab.create-token")
  private val hostLabel = bundle.getString("settings.ui.gitlab.url")
  private val docsLabel = bundle.getString("settings.ui.gitlab.docs-link-label")
  private val telemetryLabel = bundle.getString("settings.ui.gitlab.enable-telemetry")
  private val verifySetupLabel = bundle.getString("settings.ui.gitlab.verify-setup")
  private val ignoreCertificateErrorsLabel = bundle.getString("settings.ui.gitlab.ignore-certificate-errors-label")
  private val ignoreCertificateErrorsDescription = bundle.getString(
    "settings.ui.gitlab.ignore-certificate-errors-description"
  )
  private val codeSuggestionsEnabledLabel = bundle.getString("settings.ui.gitlab.enable-duo-code-suggestions")
  private val duoChatEnabledLabel = bundle.getString("settings.ui.gitlab.enable-duo-chat")

  private var settingsPanel: DialogPanel? = null
  private lateinit var errorReportingInstructions: Cell<BrowserLink>
  private lateinit var patSection: Row
  private lateinit var patField: Cell<JBPasswordField>
  private lateinit var patFieldEmptyText: StatusText
  private lateinit var integrate1PasswordCheckbox: Cell<JBCheckBox>
  private lateinit var integrate1PasswordSecretReferenceField: Cell<JTextField>

  private lateinit var healthCheck: CollapsibleRow
  private lateinit var tokenHealth: Cell<JLabel>
  private lateinit var serverVersionHealth: Cell<JLabel>
  private lateinit var currentUserHealth: Cell<JLabel>
  private lateinit var codeSuggestionHealth: Cell<JLabel>
  private lateinit var duoChatHealth: Cell<JLabel>
  private lateinit var verifyLabel: Cell<JLabel>

  private lateinit var settingsHealthChecker: SettingsHealthChecker

  private val didConfigurationChangeListener = application
    .messageBus
    .syncPublisher(DidChangeConfigurationListener.DID_CHANGE_CONFIGURATION_TOPIC)

  @Suppress("LongMethod")
  override fun createPanel(): DialogPanel {
    settingsPanel = panel {
      row {
        browserLink(docsLabel, LINKS.CODE_SUGGESTIONS_SETUP_DOCS_URL)
      }

      groupRowsRange("Connection") {
        row(hostLabel) {
          textField().bindText(settings::url)
            .addValidationRule(urlErrorMessage) { it.text.isBlank() }
            .onChanged { urlText = it.text.removeTrailingSlash() }
        }
        row {
          checkBox(ignoreCertificateErrorsLabel).bindSelected(settings::ignoreCertificateErrors).also { checkbox ->
            comment(ignoreCertificateErrorsDescription).also { warning ->
              warning.visible(settings.ignoreCertificateErrors)
              checkbox.onChanged {
                warning.visible(it.isSelected)
                settings.ignoreCertificateErrors = it.isSelected
              }
            }
          }
        }

        patSection = row {
          label(tokenLabel)

          patField = passwordField()
            .bindText(::tokenText)
            .focused()
            .errorOnApply(bundle.getString("settings.ui.gitlab.personal-access-token-required")) {
              !integrate1PasswordCheckbox.component.isSelected && String(it.password).isBlank()
            }
            .apply {
              patFieldEmptyText = component.emptyText.apply {
                if (settings.integrate1PasswordCLI) {
                  setText(GitLabBundle.message("settings.ui.personal-access-token.computed"), GRAY_ATTRIBUTES)
                }
              }
            }

          button(createTokenLabel) { BrowserUtil.open("${settings.url}${LINKS.CREATE_TOKEN_PATH}") }
        }
      }

      twoColumnsRow({
        button(verifySetupLabel) {
          verifyLabel.visible(true)

          invokeLater {
            performHealthCheck()
            healthCheck.expanded = true
          }
        }
      }) {
        verifyLabel = label("Verifying...").apply { visible(false) }
      }

      healthCheck = collapsibleGroup("Health Check") {
        row { tokenHealth = label("Validating token health...") }
        row { serverVersionHealth = label("Validating server version health...") }
        row { currentUserHealth = label("Validating current user health...") }
        row { codeSuggestionHealth = label("Validating Code Suggestions health...") }
        row { duoChatHealth = label("Validating GitLab Duo Chat health...") }

        row {
          errorReportingInstructions = browserLink(
            "How to report errors",
            "https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin#reporting-issues-or-providing-feedback"
          ).visible(false)
        }
      }.apply { addExpandedListener { expanded -> if (expanded) performHealthCheck() } }

      groupRowsRange("Features") {
        row {
          checkBox(codeSuggestionsEnabledLabel).bindSelected(settings::codeSuggestionsEnabled).onChanged {
            codeSuggestionEnabled = it.isSelected
            performHealthCheck()
          }
        }

        row {
          checkBox(duoChatEnabledLabel).bindSelected(settings::duoChatEnabled).onChanged {
            duoChatEnabled = it.isSelected
            performHealthCheck()
          }
        }
      }

      groupRowsRange("Advanced") {
        row {
          checkBox(telemetryLabel).bindSelected(settings::telemetryEnabled)
        }

        integrate1PasswordSettings()
      }
    }

    settingsHealthChecker = SettingsHealthChecker(
      errorReportingInstructions,
      tokenHealth.component,
      serverVersionHealth.component,
      currentUserHealth.component,
      codeSuggestionHealth.component,
      duoChatHealth.component,
      verifyLabel
    )

    return settingsPanel as DialogPanel
  }

  override fun apply() {
    val panel = settingsPanel ?: return
    val validationMessages = panel.validateAll()

    if (validationMessages.isEmpty()) {
      super.apply()

      didConfigurationChangeListener.onConfigurationChange(
        params = DidChangeConfigurationParams(
          settings = WorkspaceSettings(
            url = settings.url,
            codeSuggestionEnabled = settings.codeSuggestionsEnabled,
            duoChatEnabled = settings.duoChatEnabled,
            token = when {
              settings.integrate1PasswordCLI -> TokenSettings.OPSecretReference(
                settings.integrate1PasswordCLISecretReference
              )
              else -> TokenSettings.Keychain(String(patField.component.password))
            }
          )
        )
      )

      performHealthCheck()
    } else {
      throw ConfigurationException(validationMessages.joinToString(separator = "\n") { it.message })
    }
  }

  private fun Panel.integrate1PasswordSettings() {
    row {
      integrate1PasswordCheckbox = checkBox(bundle.getString("settings.ui.gitlab.1password-cli-integrate-label"))
        .bindSelected(settings::integrate1PasswordCLI)
        .onChanged {
          if (it.isSelected) {
            patFieldEmptyText.setText(GitLabBundle.message("settings.ui.personal-access-token.computed"), GRAY_ATTRIBUTES)
          } else {
            patFieldEmptyText.clear()
          }
        }

      browserLink(
        bundle.getString("settings.ui.gitlab.1password-cli-gitlab-docs-label"),
        LINKS.JETBRAINS_1PASSWORD_CLI_INTEGRATION_URL
      )
    }

    indent {
      row {
        enabledIf(integrate1PasswordCheckbox.selected)

        label(bundle.getString("settings.ui.gitlab.1password-cli-secret-reference-label"))

        integrate1PasswordSecretReferenceField = textField()
          .align(FILL)
          .bindText(settings::integrate1PasswordCLISecretReference)
          .errorOnApply(bundle.getString("settings.ui.gitlab.1password-secret-reference-invalid")) {
            integrate1PasswordCheckbox.component.isSelected && !OP_SECRET_REFERENCE_PATTERN.matches(it.text)
          }.apply {
            component.emptyText.setText(DEFAULT_GITLAB_1PASSWORD_SECRET_REFERENCE)
          }

        patSection.enabledIf(!integrate1PasswordCheckbox.selected)
      }
    }
  }

  private fun performHealthCheck() {
    // DataManager.getInstance().getDataContext must be run on EDT.
    // If no projects are open, it returns a default project.
    val project = this.settingsPanel
      ?.let { DataManager.getInstance().getDataContext(it).getData(CommonDataKeys.PROJECT) }
      ?.takeIf { !it.isDefault }

    application.executeOnPooledThread {
      val workspaceSettings = WorkspaceSettings(
        url = urlText,
        codeSuggestionEnabled = codeSuggestionEnabled,
        duoChatEnabled = duoChatEnabled,
        token = when {
          integrate1PasswordCheckbox.component.isSelected -> TokenSettings.OPSecretReference(
            integrate1PasswordSecretReferenceField.component.text
          )
          else -> TokenSettings.Keychain(String(patField.component.password))
        }
      )

      settingsHealthChecker.check(
        request = HealthCheckRequest(workspaceSettings, project)
      )
    }
  }
}

fun GitLabSettingsConfigurable.asBeta(): Configurable =
  object : Configurable by this, Configurable.Beta {}
