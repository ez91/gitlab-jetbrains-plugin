package com.gitlab.plugin

import com.intellij.DynamicBundle
import com.intellij.ide.plugins.PluginManagerCore
import com.intellij.openapi.extensions.PluginId
import com.intellij.openapi.options.ShowSettingsUtil
import com.intellij.openapi.project.Project
import org.jetbrains.annotations.NonNls
import org.jetbrains.annotations.PropertyKey

@NonNls
private const val BUNDLE = "messages.GitLabBundle"

object GitLabBundle : DynamicBundle(BUNDLE) {
  @Suppress("SpreadOperator")
  @JvmStatic
  fun message(@PropertyKey(resourceBundle = BUNDLE) key: String, vararg params: Any) =
    getMessage(key, *params)

  @Suppress("SpreadOperator", "unused")
  @JvmStatic
  fun messagePointer(@PropertyKey(resourceBundle = BUNDLE) key: String, vararg params: Any) =
    getLazyMessage(key, *params)

  fun plugin() = PluginManagerCore.getPlugin(PluginId.getId("com.gitlab.plugin"))

  fun openPluginSettings(project: Project?) {
    ShowSettingsUtil.getInstance().showSettingsDialog(project, message("settings.ui.group.name"))
  }
}
