package com.gitlab.plugin.services.chat

import com.gitlab.plugin.chat.api.abstraction.ChatApiClient
import com.gitlab.plugin.chat.api.model.AiActionResponse
import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.graphql.scalars.UserID
import com.gitlab.plugin.graphql.type.AiMessageRole
import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.services.ProjectContextService
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.util.application
import io.ktor.util.*
import kotlinx.coroutines.flow.takeWhile
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import java.util.*

internal class ChatApiClientService(
  private val userService: GitLabUserService = application.service<GitLabUserService>()
) : ChatApiClient {
  private val logger = logger<ChatApiClientService>()
  private val requestIdsBySubscriptionIds = HashMap<String, String>()

  override suspend fun processNewUserPrompt(
    subscriptionId: String,
    question: String,
    context: ChatRecordContext?
  ): AiActionResponse? {
    val action = ProjectContextService.instance.graphQLApi.chatMutation(question, subscriptionId, context)

    return action?.let {
      requestIdsBySubscriptionIds[subscriptionId] = action.requestId
      AiActionResponse(it)
    }
  }

  override suspend fun subscribeToUpdates(
    subscriptionId: String,
    onMessageReceived: suspend (message: AiMessage) -> Unit
  ) {
    val requestId = requestIdsBySubscriptionIds[subscriptionId]
      ?: throw RequestIdNotFoundException("Could not find requestId for subscriptionId: $subscriptionId")

    val userId = userService.getCurrentUserId()
      ?: throw CurrentUserNotFoundException("Could not find current user id in context")

    val userGid = "gid://gitlab/User/$userId"
    val subscription = ProjectContextService.instance.graphQLApi.chatSubscription(subscriptionId, UserID(userGid))

    var finished = false
    subscription?.takeWhile { !finished }?.collect {
      val message = it.data?.aiCompletionResponse

      // Ignore system notes for now as we don't have a way to display them and
      // this will result in an error as the caller of this code is not expecting
      // any responses for "system" role.
      if (message != null && message.role != AiMessageRole.SYSTEM) {
        if (message.chunkId == null) {
          finished = true
        }

        onMessageReceived(
          AiMessage(
            chunkId = message.chunkId,
            requestId = requestId,
            role = message.role.toString().toLowerCasePreservingASCIIRules(),
            content = message.content.orEmpty(),
            contentHtml = message.contentHtml.orEmpty(),
            timestamp = Clock.System.now().toLocalDateTime(TimeZone.UTC).toString()
          )
        )
      }
    }
  }

  override suspend fun clearChat(subscriptionId: String): AiActionResponse? {
    val action = ProjectContextService.instance.graphQLApi.chatMutation("/clear", subscriptionId)

    if (action?.errors?.isNotEmpty() == true) {
      logger.warn("GraphQL API returned an error: ${action.errors.joinToString(", ")}")
    }

    return action?.let { AiActionResponse(it) }
  }
}

class RequestIdNotFoundException(message: String) : Exception(message)
class CurrentUserNotFoundException(message: String) : Exception(message)
