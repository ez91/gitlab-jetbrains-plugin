package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.DuoClient
import com.gitlab.plugin.api.duo.PatProvider
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.api.proxiedEngine
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.IconStatusModifier
import com.gitlab.plugin.util.GitLabUtil
import com.gitlab.plugin.util.TokenUtil
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.util.application

/**
 * Duo context service
 *
 * Includes everything that should be globally available or has no dependency on the active project
 */
@Service(Service.Level.APP)
class DuoContextService {
  val duoSettings by lazy { DuoPersistentSettings.getInstance() }

  val duoHttpClient by lazy {
    DuoClient(
      tokenProvider = application.service<PatProvider>(),
      exceptionHandler = CreateNotificationExceptionHandler(GitLabNotificationManager()),
      userAgent = GitLabUtil.userAgent,
      onStatusChanged = IconStatusModifier(),
      httpClientEngine = proxiedEngine()
    )
  }

  val apolloClientFactory by lazy {
    ApolloClientFactory(tokenProvider = application.service<PatProvider>())
  }

  // We don't want notifications or icon modifications when we check the version during project startup
  // These warnings are confusing to new users who have not set up their host/token yet
  val serverServiceWithoutCallbacks by lazy {
    GitLabServerService(
      DuoApi(
        duoHttpClient.copy(
          exceptionHandler = {},
          onStatusChanged = object : DuoClient.DuoClientRequestListener {}
        ),
        duoSettings = duoSettings
      )
    )
  }

  /**
   * Return whether Duo is configured with required credentials
   *
   * @return whether duo is configured
   */
  fun isDuoConfigured(): Boolean = TokenUtil.getToken().let {
    !it.isNullOrBlank() || duoSettings.integrate1PasswordCLISecretReference.isNotBlank()
  }

  /**
   * Return whether Duo is enabled
   *
   * @return whether duo is enabled
   */
  fun isDuoEnabled(): Boolean = duoSettings.enabled

  companion object {
    @Deprecated("Use application-level service", ReplaceWith("application<DuoContextService>()"))
    val instance: DuoContextService by lazy {
      DuoContextService()
    }
  }
}
