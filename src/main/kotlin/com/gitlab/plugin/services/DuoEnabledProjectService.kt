package com.gitlab.plugin.services

import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import kotlinx.coroutines.runBlocking

@Service(Service.Level.PROJECT)
class DuoEnabledProjectService(private val project: Project) {
  private val logger = logger<DuoEnabledProjectService>()
  private val gitLabProjectService = project.service<GitLabProjectService>()

  private var isDuoEnabled: Boolean? = null

  fun isDuoEnabled(): Boolean {
    isDuoEnabled?.let { return it }

    val projectPath = gitLabProjectService.getCurrentProjectPath()
    if (projectPath != null) {
      val graphQLProject = runBlocking {
        project.service<ProjectContextService>().graphQLApi.getProject(projectPath)
      }

      if (graphQLProject?.duoFeaturesEnabled != null) {
        isDuoEnabled = graphQLProject.duoFeaturesEnabled
      } else {
        logger.info("The project retrieved from GraphQL is null.")
      }
    } else {
      isDuoEnabled = true
    }

    return isDuoEnabled ?: true
  }

  fun invalidateAndFetch() {
    isDuoEnabled = null

    isDuoEnabled()
  }
}
