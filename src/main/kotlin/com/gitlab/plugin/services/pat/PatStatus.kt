package com.gitlab.plugin.services.pat

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.pat.PatInfo
import com.gitlab.plugin.util.GitLabUtil

sealed interface PatStatus {
  val isValid: Boolean
  fun message(): String

  data class Accepted(private val patInfo: PatInfo, private val desiredScope: String = GitLabUtil.API_PAT_SCOPE) : PatStatus {
    val userId = patInfo.userId

    override val isValid: Boolean
      get() = patInfo.active && hasScope

    private val hasScope
      get() = patInfo.scopes.contains(desiredScope)

    override fun message(): String = when {
      !patInfo.active -> GitLabBundle.message("pat.accepted.not-active")
      !hasScope -> GitLabBundle.message("pat.accepted.missing-scope", desiredScope)
      else -> GitLabBundle.message("pat.accepted.valid")
    }
  }

  data class Refused(val host: String, val status: Int, val cause: Throwable) : PatStatus {
    override val isValid: Boolean
      get() = false

    override fun message(): String = GitLabBundle.message("pat.refused.message", host, status)
  }

  data class Unknown(val host: String, val cause: Throwable) : PatStatus {
    override val isValid: Boolean
      get() = false

    override fun message(): String = GitLabBundle.message("pat.unknown.message", host)
  }
}
