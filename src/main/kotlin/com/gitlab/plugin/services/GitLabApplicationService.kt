package com.gitlab.plugin.services

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.telemetry.SnowplowTracker
import com.intellij.openapi.Disposable
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service

@Service(Service.Level.APP)
class GitLabApplicationService : Disposable {
  companion object {
    @JvmStatic
    fun getInstance(): GitLabApplicationService = service()
  }

  val snowplowTracker by lazy {
    SnowplowTracker.create(BuildConfig.SNOWPLOW_COLLECTOR_URL, BuildConfig.SNOWPLOW_EMITTER_BATCH_SIZE)
  }

  override fun dispose() {
    snowplowTracker.close()
  }
}
