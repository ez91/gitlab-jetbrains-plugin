package com.gitlab.plugin.services.health

data class IntegrationHealthResult(
  private val checks: Map<HealthCheck.Name, HealthCheck>
) {
  fun hasError(): Boolean = checks
    .values
    .any { it.status == HealthCheck.Status.UNHEALTHY }

  operator fun get(name: HealthCheck.Name): HealthCheck {
    return checks[name]
      ?: error("No health check found for $name")
  }
}

data class HealthCheck(
  val name: Name,
  val message: String,
  val status: Status
) {
  enum class Status {
    HEALTHY,
    DEGRADED,
    UNHEALTHY
  }

  enum class Name {
    TOKEN,
    SERVER_VERSION,
    CURRENT_USER,
    CODE_SUGGESTIONS,
    DUO_CHAT
  }
}
