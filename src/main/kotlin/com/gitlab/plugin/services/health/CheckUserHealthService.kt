package com.gitlab.plugin.services.health

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.duo.NOOPExceptionHandler
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.graphql.CurrentUserQuery
import com.gitlab.plugin.services.DuoContextService
import com.gitlab.plugin.services.DuoEnabledProjectService
import com.gitlab.plugin.workspace.WorkspaceSettings
import com.intellij.openapi.components.service
import com.intellij.util.application

class CheckUserHealthService(
  private val apolloClientFactory: ApolloClientFactory = application.service<DuoContextService>().apolloClientFactory
) : HealthCheckService {
  override suspend fun check(request: HealthCheckRequest): List<HealthCheck> {
    val (workspaceSettings, project) = request

    val user = GraphQLApi(
      apolloClient = apolloClientFactory.create(workspaceSettings),
      exceptionHandler = NOOPExceptionHandler()
    ).getCurrentUser()

    val userCheck = checkCurrentUser(user)

    val duoEnabledForProject = project?.service<DuoEnabledProjectService>()?.isDuoEnabled() ?: true
    val codeSuggestionsCheck = checkCodeSuggestions(workspaceSettings, user, duoEnabledForProject)
    val duoChatCheck = checkDuoChat(workspaceSettings, user, duoEnabledForProject)

    return listOf(userCheck, codeSuggestionsCheck, duoChatCheck)
  }

  private fun checkCurrentUser(user: CurrentUserQuery.CurrentUser?) = if (user != null) {
    HealthCheck(
      name = HealthCheck.Name.CURRENT_USER,
      message = GitLabBundle.message("settings.ui.gitlab.verify-setup.current-user.authorized"),
      status = HealthCheck.Status.HEALTHY
    )
  } else {
    HealthCheck(
      name = HealthCheck.Name.CURRENT_USER,
      message = GitLabBundle.message("settings.ui.gitlab.verify-setup.current-user.unauthorized"),
      status = HealthCheck.Status.UNHEALTHY
    )
  }

  private fun checkCodeSuggestions(
    workspaceSettings: WorkspaceSettings,
    currentUser: CurrentUserQuery.CurrentUser?,
    duoEnabledForProject: Boolean
  ): HealthCheck {
    return if (!duoEnabledForProject) {
      HealthCheck(
        name = HealthCheck.Name.CODE_SUGGESTIONS,
        message = GitLabBundle.message("settings.ui.gitlab.verify-setup.code-suggestions.duo-disabled"),
        status = HealthCheck.Status.DEGRADED
      )
    } else if (currentUser == null || currentUser.duoCodeSuggestionsAvailable == false) {
      HealthCheck(
        name = HealthCheck.Name.CODE_SUGGESTIONS,
        message = GitLabBundle.message("settings.ui.gitlab.verify-setup.code-suggestions.disabled"),
        status = HealthCheck.Status.DEGRADED
      )
    } else if (!workspaceSettings.codeSuggestionEnabled) {
      HealthCheck(
        name = HealthCheck.Name.CODE_SUGGESTIONS,
        message = GitLabBundle.message("settings.ui.gitlab.verify-setup.code-suggestions.manually-disabled"),
        status = HealthCheck.Status.DEGRADED
      )
    } else {
      HealthCheck(
        name = HealthCheck.Name.CODE_SUGGESTIONS,
        message = GitLabBundle.message("settings.ui.gitlab.verify-setup.code-suggestions.enabled"),
        status = HealthCheck.Status.HEALTHY
      )
    }
  }

  private fun checkDuoChat(
    workspaceSettings: WorkspaceSettings,
    currentUser: CurrentUserQuery.CurrentUser?,
    duoEnabledForProject: Boolean
  ): HealthCheck {
    return if (!duoEnabledForProject) {
      HealthCheck(
        name = HealthCheck.Name.DUO_CHAT,
        message = GitLabBundle.message("settings.ui.gitlab.verify-setup.duo-chat.duo-disabled"),
        status = HealthCheck.Status.DEGRADED
      )
    } else if (currentUser == null || currentUser.duoChatAvailable == false) {
      HealthCheck(
        name = HealthCheck.Name.DUO_CHAT,
        message = GitLabBundle.message("settings.ui.gitlab.verify-setup.duo-chat.disabled"),
        status = HealthCheck.Status.DEGRADED
      )
    } else if (!workspaceSettings.duoChatEnabled) {
      HealthCheck(
        name = HealthCheck.Name.DUO_CHAT,
        message = GitLabBundle.message("settings.ui.gitlab.verify-setup.duo-chat.manually-disabled"),
        status = HealthCheck.Status.DEGRADED
      )
    } else {
      HealthCheck(
        name = HealthCheck.Name.DUO_CHAT,
        message = GitLabBundle.message("settings.ui.gitlab.verify-setup.duo-chat.enabled"),
        status = HealthCheck.Status.HEALTHY
      )
    }
  }
}
