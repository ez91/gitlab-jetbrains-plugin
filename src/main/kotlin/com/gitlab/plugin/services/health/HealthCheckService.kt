package com.gitlab.plugin.services.health

import com.gitlab.plugin.workspace.WorkspaceSettings
import com.intellij.openapi.project.Project

interface HealthCheckService {
  suspend fun check(request: HealthCheckRequest): List<HealthCheck>
}

data class HealthCheckRequest(
  val workspaceSettings: WorkspaceSettings,
  val project: Project? = null
)
