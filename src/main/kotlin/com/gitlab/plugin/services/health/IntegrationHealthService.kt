package com.gitlab.plugin.services.health

import kotlinx.coroutines.runBlocking

class IntegrationHealthService(
  private val healthcheckServices: List<HealthCheckService> = listOf(
    CheckTokenHealthService(),
    CheckUserHealthService(),
    CheckGitLabServerVersionHealthService()
  )
) {
  fun check(request: HealthCheckRequest): IntegrationHealthResult {
    return runBlocking {
      IntegrationHealthResult(
        checks = healthcheckServices.flatMap { it.check(request) }.associateBy { it.name }
      )
    }
  }
}
