package com.gitlab.plugin.services.health

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.GitLabForbiddenException
import com.gitlab.plugin.api.GitLabOfflineException
import com.gitlab.plugin.api.GitLabProxyException
import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.services.PatStatusService
import com.gitlab.plugin.services.pat.PatStatus
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.util.application

class CheckTokenHealthService(
  private val patStatusService: PatStatusService = application.service<PatStatusService>()
) : HealthCheckService {
  private val logger = logger<CheckTokenHealthService>()

  override suspend fun check(request: HealthCheckRequest): List<HealthCheck> {
    val (workspaceSettings) = request

    val token = workspaceSettings.token.getToken()
    val url = workspaceSettings.url

    if (token.isBlank()) {
      return HealthCheck(
        name = HealthCheck.Name.TOKEN,
        message = GitLabBundle.message("settings.ui.gitlab.verify-setup.empty-token"),
        status = HealthCheck.Status.UNHEALTHY
      ).let { listOf(it) }
    }

    val tokenStatus = patStatusService.currentStatus(url, token)
    val message = when (tokenStatus) {
      is PatStatus.Accepted -> tokenStatus.message()
      is PatStatus.Refused -> tokenStatus.cause.healthCheckMessage()
      is PatStatus.Unknown -> tokenStatus.cause.healthCheckMessage()
    }

    return HealthCheck(
      name = HealthCheck.Name.TOKEN,
      message = message,
      status = if (tokenStatus.isValid) HealthCheck.Status.HEALTHY else HealthCheck.Status.UNHEALTHY
    ).let { listOf(it) }
  }

  private fun Throwable.healthCheckMessage(): String {
    return when (this) {
      is GitLabOfflineException -> GitLabBundle.message("settings.ui.gitlab.verify-setup.unreachable")
      is GitLabForbiddenException -> GitLabBundle.message("settings.ui.gitlab.verify-setup.forbidden")
      is GitLabUnauthorizedException -> GitLabBundle.message("settings.ui.gitlab.verify-setup.unauthorized")
      is GitLabProxyException -> GitLabBundle.message("settings.ui.gitlab.verify-setup.proxy-exception")
      else -> {
        logger.warn(
          GitLabBundle.message("settings.ui.gitlab.verify-setup.unexpected-error"),
          this
        )

        GitLabBundle.message("settings.ui.gitlab.verify-setup.unexpected-error")
      }
    }
  }
}
