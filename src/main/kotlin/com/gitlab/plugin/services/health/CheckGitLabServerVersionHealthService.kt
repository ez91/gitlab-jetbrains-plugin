package com.gitlab.plugin.services.health

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.services.DuoContextService
import com.gitlab.plugin.services.GitLabServerService
import com.intellij.openapi.components.service
import com.intellij.util.application

class CheckGitLabServerVersionHealthService(
  private val gitLabServerService: GitLabServerService = application.service<DuoContextService>().serverServiceWithoutCallbacks
) : HealthCheckService {
  override suspend fun check(request: HealthCheckRequest): List<HealthCheck> {
    val (workspaceSettings) = request
    val server = gitLabServerService.get(workspaceSettings)

    val (message, status) = when {
      server == null -> GitLabBundle.message(
        "settings.ui.gitlab.verify-setup.server-version.unable-to-validate",
        workspaceSettings.url
      ) to HealthCheck.Status.DEGRADED
      server.isVersionUpgradeRequired -> GitLabBundle.message(
        "settings.ui.gitlab.verify-setup.server-version.upgrade-required",
        server.version,
        GitLabServerService.MINIMUM_GITLAB_VERSION
      ) to HealthCheck.Status.UNHEALTHY
      else -> GitLabBundle.message(
        "settings.ui.gitlab.verify-setup.server-version.supported",
        server.version
      ) to HealthCheck.Status.HEALTHY
    }

    return listOf(
      HealthCheck(
        name = HealthCheck.Name.SERVER_VERSION,
        message = message,
        status = status
      )
    )
  }
}
