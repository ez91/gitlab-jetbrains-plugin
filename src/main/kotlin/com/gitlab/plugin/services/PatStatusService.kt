package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.PatProvider
import com.gitlab.plugin.api.pat.PatApi
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.services.pat.PatStatus
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.util.application
import io.ktor.client.plugins.*
import io.ktor.utils.io.errors.*

@Service(Service.Level.APP)
class PatStatusService(private val patApi: PatApi = PatApi()) {
  private val logger = logger<PatStatusService>()

  suspend fun currentStatus(
    host: String = DuoPersistentSettings.getInstance().url,
    token: String = application.service<PatProvider>().token()
  ): PatStatus {
    return try {
      val patInfo = patApi.patInfo(host, token)

      PatStatus.Accepted(patInfo)
    } catch (e: ResponseException) {
      logger.debug("Failed to validate Personal Access Token. Try again with a different token.", e)

      PatStatus.Refused(host, e.response.status.value, e)
    } catch (e: IOException) {
      logger.debug("Failed to validate Personal Access Token.", e)

      PatStatus.Unknown(host, e)
    }
  }
}
