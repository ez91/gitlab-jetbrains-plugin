package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.listeners.GraphQLApiDidChangeConfigurationListener
import com.gitlab.plugin.codesuggestions.license.CodeSuggestionsLicenseStatus
import com.gitlab.plugin.codesuggestions.telemetry.LogDestination
import com.gitlab.plugin.codesuggestions.telemetry.SnowplowDestination
import com.gitlab.plugin.codesuggestions.telemetry.Telemetry
import com.gitlab.plugin.ui.CompletionStrategy
import com.gitlab.plugin.util.gitlabStatusRefresh
import com.intellij.openapi.components.Service
import com.intellij.openapi.project.Project

@Service(Service.Level.PROJECT)
class ProjectContextService(val project: Project) {

  init {
    if (!isInitialized()) {
      instance = this
    }
  }

  private val duoContext: DuoContextService by lazy { DuoContextService.instance }

  val duoApi by lazy {
    DuoApi(
      client = duoContext.duoHttpClient,
      telemetry = telemetry,
      duoSettings = duoContext.duoSettings,
      isLicensed = { codeSuggestionsLicense.isLicensed }
    )
  }

  private val serverService by lazy { GitLabServerService(duoApi) }

  val completionStrategy by lazy { CompletionStrategy(duoApi, serverService) }

  val telemetry by lazy {
    val snowplowTracker = GitLabApplicationService.getInstance().snowplowTracker
    Telemetry(
      listOf(LogDestination(), SnowplowDestination(snowplowTracker)),
      isEnabled = { duoContext.duoSettings.telemetryEnabled }
    )
  }

  val graphQLApi by lazy {
    GraphQLApi(
      apolloClient = duoContext.apolloClientFactory.create()
    ).also { GraphQLApiDidChangeConfigurationListener(duoContext.apolloClientFactory, it) }
  }

  val codeSuggestionsLicense by lazy {
    CodeSuggestionsLicenseStatus(
      graphQLApi,
      onLicenseChanged = ::gitlabStatusRefresh
    )
  }

  companion object {
    @Deprecated("Use project-level service", ReplaceWith("project.service<ProjectContextService>()"))
    lateinit var instance: ProjectContextService

    fun isInitialized() = this::instance.isInitialized
  }
}
