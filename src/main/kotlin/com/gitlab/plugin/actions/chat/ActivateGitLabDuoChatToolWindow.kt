package com.gitlab.plugin.actions.chat

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.intellij.ide.actions.ActivateToolWindowAction
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnActionEvent

const val DUO_CHAT_TOOL_WINDOW_ID = "GitLab Duo Chat"

/**
 * NOTE: The action ID of "ActivateGitLabDuoChatToolWindow" is important since it will cause the "Tool Window" action
 * to share this action's declaration after toolbar window creation.
 */
class ActivateGitLabDuoChatToolWindow : ActivateToolWindowAction(DUO_CHAT_TOOL_WINDOW_ID) {
  override fun update(event: AnActionEvent) {
    event.presentation.isVisible = DuoPersistentSettings.getInstance().duoChatEnabled
  }

  override fun getActionUpdateThread(): ActionUpdateThread {
    return ActionUpdateThread.EDT
  }
}
