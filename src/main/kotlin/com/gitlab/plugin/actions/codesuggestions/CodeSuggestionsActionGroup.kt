package com.gitlab.plugin.actions.codesuggestions

import com.intellij.openapi.actionSystem.DefaultActionGroup

class CodeSuggestionsActionGroup : DefaultActionGroup()
