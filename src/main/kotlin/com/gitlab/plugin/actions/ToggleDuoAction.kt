package com.gitlab.plugin.actions

import com.gitlab.plugin.services.DuoContextService
import com.gitlab.plugin.util.gitlabStatusRefresh
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.ToggleAction

class ToggleDuoAction : ToggleAction() {
  override fun isSelected(e: AnActionEvent): Boolean {
    val duoSettings = DuoContextService.instance.duoSettings

    return duoSettings.enabled
  }

  override fun setSelected(e: AnActionEvent, state: Boolean) {
    val duoSettings = DuoContextService.instance.duoSettings

    duoSettings.enabled = state

    gitlabStatusRefresh()
  }

  override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.BGT
}
