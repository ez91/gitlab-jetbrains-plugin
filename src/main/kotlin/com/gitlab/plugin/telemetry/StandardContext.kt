package com.gitlab.plugin.telemetry

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.snowplowanalytics.snowplow.tracker.payload.SelfDescribingJson

data class StandardContext(
  val source: String,
  val extra: Map<String, String>? = null,
  val environment: String
) {
  companion object {
    private object Environment {
      const val GITLAB_COM = "production"
      const val GITLAB_STAGING = "staging"
      const val GITLAB_ORG = "org"
      const val GITLAB_DEVELOPMENT = "development"
      const val GITLAB_SELF_MANAGED = "self-managed"

      const val GITLAB_COM_URL = "https://gitlab.com"
      const val GITLAB_STAGING_URL = "https://staging.gitlab.com"
      const val GITLAB_ORG_URL = "https://dev.gitlab.org"
      const val GITLAB_DEVELOPMENT_URL = "http://localhost"
    }

    const val SCHEMA = "iglu:com.gitlab/gitlab_standard/jsonschema/1-0-10"

    fun build(extra: Map<String, String>? = null) = StandardContext(
      source = requireNotNull(GitLabBundle.plugin()?.name),
      extra = extra,
      environment = environmentFromHost(DuoPersistentSettings.getInstance().url)
    ).toSelfDescribingJson()

    private fun environmentFromHost(url: String): String = when {
      url == Environment.GITLAB_COM_URL -> Environment.GITLAB_COM
      url == Environment.GITLAB_STAGING_URL -> Environment.GITLAB_STAGING
      url == Environment.GITLAB_ORG_URL -> Environment.GITLAB_ORG
      url.contains(Environment.GITLAB_DEVELOPMENT_URL) -> Environment.GITLAB_DEVELOPMENT
      else -> Environment.GITLAB_SELF_MANAGED
    }
  }

  private fun toSelfDescribingJson() = SelfDescribingJson(
    SCHEMA,
    mapOf(
      "source" to source,
      "extra" to extra,
      "environment" to environment
    )
  )
}
