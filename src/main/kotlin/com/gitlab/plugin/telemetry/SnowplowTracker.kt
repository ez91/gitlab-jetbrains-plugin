package com.gitlab.plugin.telemetry

import com.gitlab.plugin.api.ProxyManager
import com.gitlab.plugin.api.asProxyInfo
import com.gitlab.plugin.api.duo.configureSslSocketFactory
import com.gitlab.plugin.codesuggestions.telemetry.IdeExtensionVersionContext
import com.gitlab.plugin.util.GitLabUtil
import com.intellij.util.net.HttpConfigurable
import com.snowplowanalytics.snowplow.tracker.Snowplow
import com.snowplowanalytics.snowplow.tracker.Tracker
import com.snowplowanalytics.snowplow.tracker.configuration.EmitterConfiguration
import com.snowplowanalytics.snowplow.tracker.configuration.NetworkConfiguration
import com.snowplowanalytics.snowplow.tracker.configuration.SubjectConfiguration
import com.snowplowanalytics.snowplow.tracker.configuration.TrackerConfiguration
import com.snowplowanalytics.snowplow.tracker.http.OkHttpClientAdapter
import okhttp3.OkHttpClient

object SnowplowTracker {
  private const val NAMESPACE = "gl"
  private const val APP_ID = "gitlab_ide_extension"
  val IDE_EXTENSION_VERSION_CONTEXT by lazy { IdeExtensionVersionContext.build() }

  fun create(collectorUrl: String, batchSize: Int): Tracker {
    val client = OkHttpClient.Builder().buildWithProxySupport()
    val trackerConfiguration = TrackerConfiguration(NAMESPACE, APP_ID)
    val networkConfiguration = NetworkConfiguration(OkHttpClientAdapter(collectorUrl, client))
    val emitterConfiguration = EmitterConfiguration().batchSize(batchSize)
    val subjectConfiguration = SubjectConfiguration().useragent(GitLabUtil.userAgent)

    return Snowplow.createTracker(
      trackerConfiguration,
      networkConfiguration,
      emitterConfiguration,
      subjectConfiguration
    )
  }
}

private fun OkHttpClient.Builder.buildWithProxySupport(): OkHttpClient {
  configureSslSocketFactory()
  followRedirects(true)

  val proxyInfo = HttpConfigurable.getInstance().asProxyInfo()
  if (proxyInfo != null) {
    val proxyManager = ProxyManager(proxyInfo) { HttpConfigurable.getInstance().isHttpProxyEnabledForUrl(it) }

    proxySelector(proxyManager)
    proxyAuthenticator(proxyManager)
  }

  return build()
}
