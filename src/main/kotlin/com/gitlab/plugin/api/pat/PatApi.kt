package com.gitlab.plugin.api.pat

import com.gitlab.plugin.api.*
import com.gitlab.plugin.util.GitLabUtil
import com.intellij.collaboration.util.resolveRelative
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import java.net.URI

class PatApi(engine: HttpClientEngine? = null) {
  private val client = HttpClient(engine ?: proxiedEngine()) {
    expectSuccess = true

    defaultRequest {
      contentType(ContentType.Application.Json)
    }

    install(UserAgent) {
      agent = GitLabUtil.userAgent
    }

    install(HttpRequestRetry) {
      retryOnServerErrors(maxRetries = 3)
      exponentialDelay()
    }

    install(ContentNegotiation) {
      json(Json { isLenient = true })
    }

    HttpResponseValidator {
      handleResponseExceptionWithRequest { cause: Throwable, request: HttpRequest ->
        HttpExceptionHandler.handle(cause, request)
      }
    }
  }

  suspend fun patInfo(host: String, token: String): PatInfo {
    val url = URI(host).resolveRelative("api/v4/personal_access_tokens/self").toString()

    return client.get {
      url(url)
      headers { append("Authorization", "Bearer $token") }
    }.body()
  }
}
