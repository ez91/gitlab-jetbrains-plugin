package com.gitlab.plugin.api.pat

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PatInfo(
  val id: Int,
  val name: String,
  val revoked: Boolean,
  @SerialName("created_at")
  val createdAt: String,
  val scopes: List<String>,
  @SerialName("user_id")
  val userId: Int,
  @SerialName("last_used_at")
  val lastUsedAt: String,
  val active: Boolean,
  @SerialName("expires_at")
  val expiresAt: String
) {
  companion object {
    const val AI_FEATURES_SCOPE = "ai_features"
    const val API_SCOPE = "api"
  }
}
