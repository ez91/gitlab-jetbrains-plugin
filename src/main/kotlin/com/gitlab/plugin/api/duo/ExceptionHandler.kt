package com.gitlab.plugin.api.duo

fun interface ExceptionHandler {
  fun handleException(exception: Throwable)
}
