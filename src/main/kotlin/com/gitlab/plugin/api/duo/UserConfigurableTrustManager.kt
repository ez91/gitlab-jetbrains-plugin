package com.gitlab.plugin.api.duo

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.intellij.openapi.diagnostic.logger
import java.security.KeyStore
import java.security.cert.X509Certificate
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

/**
 * A trust manager which can be configured by the user when additional certificate settings were provided.
 */
class UserConfigurableTrustManager(private val duoPersistentSettings: DuoPersistentSettings) : X509TrustManager {
  private val defaultTrustManager: X509TrustManager by lazy {
    val trustManagerFactory = TrustManagerFactory.getInstance(
      TrustManagerFactory.getDefaultAlgorithm()
    ).apply { init(null as KeyStore?) }

    val trustManagers = trustManagerFactory.trustManagers
    check(trustManagers.size == 1 && trustManagers[0] is X509TrustManager) {
      "Unexpected default trust managers: ${trustManagers.contentToString()}"
    }

    trustManagers[0] as X509TrustManager
  }
  private val logger = logger<UserConfigurableTrustManager>()

  override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {
    defaultTrustManager.checkClientTrusted(chain, authType)
  }

  override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {
    if (duoPersistentSettings.ignoreCertificateErrors) {
      logger.debug("Not checking server certificate is trusted due to user configuration.")
    } else {
      defaultTrustManager.checkServerTrusted(chain, authType)
    }
  }

  override fun getAcceptedIssuers(): Array<X509Certificate> = defaultTrustManager.acceptedIssuers
}
