package com.gitlab.plugin.api.duo

import com.gitlab.plugin.services.DuoContextService
import com.gitlab.plugin.util.TokenUtil
import com.gitlab.plugin.workspace.TokenSettings
import com.intellij.openapi.components.Service

@Service(Service.Level.APP)
class PatProvider : DuoClient.TokenProvider {
  private var token = ""

  override fun token(): String {
    if (token.isNotBlank()) {
      return token
    }

    val settings = if (DuoContextService.instance.duoSettings.integrate1PasswordCLI) {
      TokenSettings.OPSecretReference(DuoContextService.instance.duoSettings.integrate1PasswordCLISecretReference)
    } else {
      TokenSettings.Keychain(TokenUtil.getToken().orEmpty())
    }

    token = settings.getToken()
    return token
  }

  fun cacheToken(settings: TokenSettings) {
    token = settings.getToken()
  }
}
