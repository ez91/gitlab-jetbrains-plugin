package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.api.duo.requests.Metadata
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.codesuggestions.telemetry.Telemetry
import com.gitlab.plugin.workspace.WorkspaceSettings
import com.intellij.openapi.diagnostic.logger
import io.ktor.client.call.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.utils.io.*

class DuoApi(
  private val client: DuoClient,
  private val telemetry: Telemetry = Telemetry.NOOP,
  private val duoSettings: DuoPersistentSettings,
  private val isLicensed: () -> Boolean = { true }
) {
  private val logger = logger<DuoApi>()
  private val basePath = "/api/v4/"

  suspend fun completions(payload: Completion.Payload): Completion.Response? {
    if (!isApiEnabled() || !client.isConfigured()) return null

    return withTelemetry(payload) { client.post(fullEndpoint("code_suggestions/completions")) { setBody(payload) } }
  }

  suspend fun metadata() = client.get(fullEndpoint("metadata"))?.body<Metadata.Response?>()

  @Suppress("TooGenericExceptionCaught")
  suspend fun metadata(settings: WorkspaceSettings): Metadata.Response? {
    return try {
      client.restClient.get("${settings.url}${basePath}metadata") {
        usingToken { settings.token.getToken() }
      }.body<Metadata.Response?>()
    } catch (e: Exception) {
      logger.warn("Unable to get metadata for server ${settings.url}.", e)
      null
    }
  }

  fun isApiEnabled(): Boolean = duoSettings.enabled && isLicensed()

  private fun fullEndpoint(endpoint: String) = "$basePath$endpoint"

  private suspend fun withTelemetry(
    payload: Completion.Payload,
    doRequest: suspend () -> HttpResponse?
  ): Completion.Response? {
    var eventContext = telemetry.newContext().copy(
      prefixLength = payload.currentFile.contentAboveCursor.length,
      suffixLength = payload.currentFile.contentBelowCursor.length
    )
    telemetry.requested(eventContext)

    try {
      val response = doRequest()
      eventContext = eventContext.copy(
        apiStatusCode = response?.status?.value,
        requestId = response?.headers?.get("x-request-id")
      )

      val body = response?.body<Completion.Response?>() ?: run {
        telemetry.error(eventContext)
        return null
      }

      eventContext = eventContext.copy(
        modelName = body.model.name,
        modelEngine = body.model.engine,
        language = body.model.lang
      )

      telemetry.loaded(eventContext)

      if (!body.hasSuggestions()) telemetry.notProvided(eventContext)

      return body
    } catch (e: CancellationException) {
      telemetry.cancelled(eventContext)
      return null
    } catch (e: Exception) {
      telemetry.error(eventContext)
      throw e
    }
  }
}
