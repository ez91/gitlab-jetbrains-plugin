package com.gitlab.plugin.api.duo

class NOOPExceptionHandler : ExceptionHandler {
  override fun handleException(exception: Throwable) = Unit
}
