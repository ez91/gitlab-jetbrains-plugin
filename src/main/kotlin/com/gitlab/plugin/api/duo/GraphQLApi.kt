package com.gitlab.plugin.api.duo

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.api.Optional
import com.apollographql.apollo3.exception.ApolloException
import com.gitlab.plugin.chat.api.model.AiAction
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.gitlab.plugin.graphql.*
import com.gitlab.plugin.graphql.scalars.UserID
import com.gitlab.plugin.graphql.type.AiChatInput
import com.gitlab.plugin.graphql.type.AiCurrentFileInput
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.diagnostic.logger

class GraphQLApi(
  var apolloClient: ApolloClient,
  private val exceptionHandler: ExceptionHandler = CreateNotificationExceptionHandler(
    GitLabNotificationManager()
  )
) {
  private val logger: Logger = logger<GraphQLApi>()

  suspend fun getCurrentUser() = try {
    apolloClient.query(CurrentUserQuery())
      .execute()
      .dataAssertNoErrors
      .currentUser
  } catch (cause: ApolloException) {
    exceptionHandler.handleException(cause)
    logger.info("GraphQL CurrentUserQuery returned an error: ${cause.message}")
    null
  }

  @Suppress("DEPRECATION")
  suspend fun chatMutation(
    content: String,
    clientSubscriptionId: String,
    context: ChatRecordContext? = null
  ): AiAction? = try {
    val currentFile = Optional.presentIfNotNull(
      context?.let {
        extractCurrentFile(it.currentFile)
      }
    )
    val request = ChatMutation(
      AiChatInput(
        content = content,
        currentFile = currentFile
      ),
      clientSubscriptionId
    )

    val response = apolloClient.mutation(request)
      .execute()
      .dataAssertNoErrors
      .action

    response?.requestId?.let { AiAction(requestId = it, errors = response.errors) }
  } catch (cause: ApolloException) {
    exceptionHandler.handleException(cause)
    logger.info("GraphQL ChatMutation request returned an error: ${cause.message}")
    null
  }

  private fun extractCurrentFile(currentFile: ChatRecordFileContext) = AiCurrentFileInput(
    fileName = currentFile.fileName,
    selectedText = currentFile.selectedText,
    contentAboveCursor = Optional.present(currentFile.contentAboveCursor),
    contentBelowCursor = Optional.present(currentFile.contentBelowCursor)
  )

  suspend fun getProject(projectPath: String) = try {
    apolloClient.query(ProjectQuery(projectPath))
      .execute()
      .dataAssertNoErrors
      .project
  } catch (cause: ApolloException) {
    exceptionHandler.handleException(cause)
    logger.info("GraphQL ProjectQuery returned an error: ${cause.message}")
    null
  }

  fun chatSubscription(clientSubscriptionId: String, userId: UserID) = try {
    apolloClient.subscription(
      ChatSubscription(
        clientSubscriptionId = clientSubscriptionId,
        userId = userId,
      )
    ).toFlow()
  } catch (cause: ApolloException) {
    exceptionHandler.handleException(cause)
    logger.info("GraphQL ChatSubscription returned an error: ${cause.message}")
    null
  }
}
