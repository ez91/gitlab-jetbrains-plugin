package com.gitlab.plugin.api.graphql

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.network.http.DefaultHttpEngine
import com.apollographql.apollo3.network.ws.WebSocketNetworkTransport
import com.gitlab.plugin.api.ProxyManager
import com.gitlab.plugin.api.asProxyInfo
import com.gitlab.plugin.api.duo.DuoClient
import com.gitlab.plugin.api.duo.configureSslSocketFactory
import com.gitlab.plugin.api.graphql.apollo.actioncable.ActionCableWebSocketEngine
import com.gitlab.plugin.api.graphql.apollo.actioncable.ActionCableWsProtocolFactory
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.util.removeTrailingSlash
import com.gitlab.plugin.workspace.WorkspaceSettings
import com.intellij.collaboration.util.resolveRelative
import com.intellij.util.net.HttpConfigurable
import io.ktor.http.HttpHeaders.Authorization
import io.ktor.http.HttpHeaders.Origin
import okhttp3.OkHttpClient
import java.net.URI

class ApolloClientFactory(
  private val tokenProvider: DuoClient.TokenProvider
) {

  private val okHttpClient: OkHttpClient by lazy {
    OkHttpClient.Builder().apply {
      configureSslSocketFactory()
      followRedirects(true)

      val proxyInfo = HttpConfigurable.getInstance().asProxyInfo()
      proxyInfo?.let {
        ProxyManager(it) { url -> HttpConfigurable.getInstance().isHttpProxyEnabledForUrl(url) }.let { proxyManager ->
          proxySelector(proxyManager)
          proxyAuthenticator(proxyManager)
        }
      }
    }.build()
  }

  fun create(settings: WorkspaceSettings? = null): ApolloClient {
    val url = (settings?.url ?: DuoPersistentSettings.getInstance().url).removeTrailingSlash()
    val instanceUri = URI(url)

    val serverUrl: String = instanceUri
      .resolveRelative("/api/graphql")
      .toString()

    val subscriptionUrl: String = instanceUri
      .resolveRelative("/-/cable")
      .toString()
      .replace("https://", "wss://")
      .replace("http://", "ws://")

    val bearerToken = computeBearerToken(settings)
    return ApolloClient.Builder()
      .addHttpHeader(Authorization, bearerToken)
      .httpEngine(DefaultHttpEngine(okHttpClient))
      .serverUrl(serverUrl)
      .subscriptionNetworkTransport(
        WebSocketNetworkTransport.Builder()
          .serverUrl(subscriptionUrl)
          .addHeader(Origin, instanceUri.toString())
          .addHeader(Authorization, bearerToken)
          .webSocketEngine(ActionCableWebSocketEngine(okHttpClient))
          .protocol(ActionCableWsProtocolFactory())
          .build()
      )
      .build()
  }

  private fun computeBearerToken(settings: WorkspaceSettings?): String {
    val token = settings?.token?.getToken() ?: tokenProvider.token()
    return "Bearer $token"
  }
}
