package com.gitlab.plugin.api.graphql.apollo.actioncable

import com.apollographql.apollo3.network.ws.WebSocketConnection
import com.apollographql.apollo3.network.ws.WsProtocol
import kotlinx.coroutines.CoroutineScope

class ActionCableWsProtocolFactory : WsProtocol.Factory {
  private val connectionAcknowledgeTimeoutMs: Long? = null

  override val name: String
    get() = "gitlab-actioncable-ws"

  override fun create(
    webSocketConnection: WebSocketConnection,
    listener: WsProtocol.Listener,
    scope: CoroutineScope
  ): WsProtocol {
    return ActionCableWsProtocol(
      connectionAcknowledgeTimeoutMs = connectionAcknowledgeTimeoutMs ?: 10_000,
      webSocketConnection = webSocketConnection,
      listener = listener
    )
  }
}
