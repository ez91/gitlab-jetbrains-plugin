package com.gitlab.plugin.api.graphql.actioncable

import com.apollographql.apollo3.network.ws.WsProtocol
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.diagnostic.logger

class ActionCableSubscription(
  val identifier: String,
  val listener: WsProtocol.Listener,
  val requestUuid: String,
) {
  private val logger: Logger = logger<ActionCableSubscription>()
  fun command(commandType: CommandType) = mapOf(
    "command" to commandType.command,
    "identifier" to identifier
  )

  fun handleGitLabGraphQLMessage(message: Map<String, Any?>?) {
    message ?: return

    val result = (message["result"] as? Map<String, Any?>).orEmpty()
    if (result.isNotEmpty()) {
      val payload = mapOf(
        "data" to result["data"],
        "errors" to result["errors"],
      )
      listener.operationResponse(requestUuid, payload)
    }

    val hasErrors = result["errors"]
      ?.let { it is Collection<*> && !it.isEmpty() }
      ?: false
    if (message["more"] == false || hasErrors) {
      val status = if (hasErrors) "error" else "complete"
      logger.warn("[requestUuid: $requestUuid] received $status response from server $message")
      listener.operationComplete(requestUuid)
    }
  }

  enum class CommandType(val command: String) {
    SUBSCRIBE("subscribe"),
    UNSUBSCRIBE("unsubscribe"),
  }
}
