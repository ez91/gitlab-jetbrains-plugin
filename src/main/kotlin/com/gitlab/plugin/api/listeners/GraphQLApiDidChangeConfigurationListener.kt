package com.gitlab.plugin.api.listeners

import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.workspace.DidChangeConfigurationListener
import com.gitlab.plugin.workspace.DidChangeConfigurationParams
import com.intellij.util.application

class GraphQLApiDidChangeConfigurationListener(
  private val apolloClientFactory: ApolloClientFactory,
  private val graphQLApi: GraphQLApi
) : DidChangeConfigurationListener {
  init {
    application.messageBus.connect().subscribe(DidChangeConfigurationListener.DID_CHANGE_CONFIGURATION_TOPIC, this)
  }

  override fun onConfigurationChange(params: DidChangeConfigurationParams) {
    graphQLApi.apolloClient = apolloClientFactory.create(params.settings)
  }
}
