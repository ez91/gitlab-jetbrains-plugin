package com.gitlab.plugin.api

import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.utils.io.errors.*

interface GitLabException

class GitLabResponseException(response: HttpResponse, val cachedResponseText: String) :
  ResponseException(response, cachedResponseText), GitLabException {
  override val message: String =
    "There was an error with the request: ${response.call.request.url}, " +
      "Method: ${response.call.request.method.value}, Status: ${response.status}."
}

class GitLabUnauthorizedException(response: HttpResponse, cachedResponseText: String) :
  ResponseException(response, cachedResponseText), GitLabException {
  override val message: String =
    "Invalid authentication for Request: ${response.call.request.url}, " +
      "Method: ${response.call.request.method.value}, Status: ${response.status}."
}

class GitLabForbiddenException(response: HttpResponse, cachedResponseText: String) :
  ResponseException(response, cachedResponseText), GitLabException {
  override val message: String =
    "User not allowed to perform request: ${response.call.request.url}, " +
      "Method: ${response.call.request.method.value}, Status: ${response.status}."
}

class GitLabOfflineException(request: HttpRequest, cause: Throwable) : IOException(), GitLabException {
  override val message: String =
    "Could not complete Request: ${request.url}, " +
      "Method: ${request.method.value}, Error: ${cause::class.qualifiedName}."
}

class GitLabProxyException(cause: Throwable) : IOException(cause), GitLabException {
  override val message: String =
    "Proxy is not configured properly, Cause: ${cause.message}, Error: ${cause::class.qualifiedName}." +
      "If it has been configured recently, you might need to restart the IDE."
}
