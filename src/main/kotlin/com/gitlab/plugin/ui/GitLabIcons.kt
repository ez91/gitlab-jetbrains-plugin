package com.gitlab.plugin.ui

import com.intellij.openapi.util.IconLoader

object GitLabIcons {
  @JvmField
  val NotificationIcon = IconLoader.getIcon("/icons/notificationIcon.svg", javaClass)
}
