package com.gitlab.plugin.ui

import com.intellij.notification.NotificationGroup
import com.intellij.notification.NotificationGroupManager
import com.intellij.notification.NotificationType
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.project.Project

const val GENERAL_NOTIFICATION_GROUP_ID = "com.gitlab.plugin.notification.general"
const val IMPORTANT_NOTIFICATION_GROUP_ID = "com.gitlab.plugin.notification.important"

enum class NotificationGroupType {
  GENERAL,
  IMPORTANT
}

class GitLabNotificationManager {
  fun sendNotification(
    notificationData: Notification,
    notificationGroupType: NotificationGroupType = NotificationGroupType.GENERAL,
    notificationType: NotificationType = NotificationType.INFORMATION,
    project: Project? = null
  ) {
    val manager = getNotificationGroup(notificationGroupType)

    val nativeNotification = manager.createNotification(
      notificationData.title,
      notificationData.message,
      notificationType
    )
    nativeNotification.setIcon(GitLabIcons.NotificationIcon)

    notificationData.actions.forEach { action ->
      nativeNotification.addAction(
        DumbAwareAction.create(action.title) {
          action.run {
            nativeNotification.expire()
          }
        }
      )
    }

    nativeNotification.notify(project)
  }

  private fun getNotificationGroup(notificationGroupType: NotificationGroupType): NotificationGroup =
    when (notificationGroupType) {
      NotificationGroupType.GENERAL -> NotificationGroupManager.getInstance()
        .getNotificationGroup(GENERAL_NOTIFICATION_GROUP_ID)

      NotificationGroupType.IMPORTANT -> NotificationGroupManager.getInstance()
        .getNotificationGroup(IMPORTANT_NOTIFICATION_GROUP_ID)
    }
}
