package com.gitlab.plugin.ui

import com.gitlab.plugin.GitLabSettingsChangeEvent
import com.gitlab.plugin.GitLabSettingsListener
import com.gitlab.plugin.codesuggestions.license.CodeSuggestionsLicenseStatus
import com.gitlab.plugin.services.DuoContextService
import com.gitlab.plugin.services.GitLabServerService
import com.gitlab.plugin.services.ProjectContextService
import com.gitlab.plugin.util.*
import com.intellij.notification.NotificationType
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.IconLoader
import com.intellij.openapi.util.NlsContexts
import com.intellij.openapi.wm.StatusBar
import com.intellij.openapi.wm.StatusBarWidget
import com.intellij.util.Consumer
import com.intellij.util.application
import com.intellij.util.messages.MessageBusConnection
import java.awt.event.MouseEvent
import java.util.*
import javax.swing.Icon

class GitLabStatusPanel(project: Project) : StatusBarWidget, StatusBarWidget.IconPresentation {
  private val bundle: ResourceBundle = ResourceBundle.getBundle("messages.GitLabBundle")
  private var myStatusBar: StatusBar? = null
  private val myConnection: MessageBusConnection

  var status: Status = Status.LOADING

  init {
    val duoContext = DuoContextService.instance

    status = if (duoContext.isDuoConfigured() && duoContext.isDuoEnabled()) {
      Status.ENABLED
    } else {
      Status.DISABLED
    }

    myConnection = project.messageBus.connect(this)
    registerListeners(myConnection)
  }

  override fun ID(): String = WIDGET_ID
  override fun getPresentation() = this
  override fun getTooltipText(): String? = getGitLabState().toolTip

  override fun getIcon(): Icon? = getGitLabState().icon

  override fun install(statusBar: StatusBar) {
    myStatusBar = statusBar
  }

  override fun getClickConsumer() = Consumer<MouseEvent> {
    val duoContext = DuoContextService.instance
    val duoSettings = duoContext.duoSettings
    val projectContext = ProjectContextService.instance

    application.executeOnPooledThread {
      when {
        !projectContext.codeSuggestionsLicense.isLicensed -> {
          GitLabNotificationManager().sendNotification(
            CodeSuggestionsLicenseStatus.licenseMissingNotification(),
            NotificationGroupType.IMPORTANT,
            NotificationType.ERROR
          )
          application.invokeLater(::refresh)
        }
        duoContext.serverServiceWithoutCallbacks.get().isVersionUpgradeRequired -> {
          GitLabNotificationManager().sendNotification(GitLabServerService.versionUnsupportedNotification())
          application.invokeLater(::refresh)
        }
        else -> {
          if (duoContext.isDuoConfigured()) {
            val enabled = duoSettings.toggleEnabled()
            val action = if (enabled) ::gitlabStatusEnabled else ::gitlabStatusDisabled

            application.invokeLater(action)
          } else {
            val notification = Notification(
              "GitLab Duo",
              "You need to configure your GitLab credentials first.",
              listOf(NotificationAction.settings(null))
            )

            gitlabStatusError()
            GitLabNotificationManager().sendNotification(
              notification,
              NotificationGroupType.IMPORTANT,
              NotificationType.ERROR
            )
          }

          application.invokeLater(::refresh)
        }
      }
    }
  }

  override fun dispose() {
    myStatusBar = null
  }

  fun refresh() {
    myStatusBar?.updateWidget(ID())
  }

  private fun registerListeners(connection: MessageBusConnection) {
    connection.subscribe(
      GitLabSettingsListener.SETTINGS_CHANGED,
      object : GitLabSettingsListener {
        override fun codeStyleSettingsChanged(event: GitLabSettingsChangeEvent) {
          status = event.status

          refresh()
        }
      }
    )
  }

  private fun getGitLabState(): WidgetState {
    val iconPath: String
    val tooltipId: String

    when (status) {
      Status.ENABLED -> {
        tooltipId = "status-icon.tooltip.enabled"
        iconPath = "/icons/gitlab-code-suggestions-enabled.svg"
      }

      Status.DISABLED -> {
        tooltipId = "status-icon.tooltip.disabled"
        iconPath = "/icons/gitlab-code-suggestions-disabled.svg"
      }

      Status.ERROR -> {
        tooltipId = "status-icon.tooltip.error"
        iconPath = "/icons/gitlab-code-suggestions-error.svg"
      }

      Status.LOADING -> {
        tooltipId = "status-icon.tooltip.loading"
        iconPath = "/icons/gitlab-code-suggestions-loading.svg"
      }

      Status.NOT_LICENSED -> {
        tooltipId = "code-suggestions.not-licensed"
        iconPath = "/icons/gitlab-code-suggestions-disabled.svg"
      }

      Status.GITLAB_VERSION_UNSUPPORTED -> {
        tooltipId = "code-suggestions.gitlab-version-unsupported"
        iconPath = "/icons/gitlab-code-suggestions-disabled.svg"
      }
    }

    return WidgetState(bundle.getString(tooltipId), IconLoader.getIcon(iconPath, javaClass))
  }

  companion object {
    @JvmField
    val WIDGET_ID: String = GitLabStatusPanel::class.java.name
  }

  class WidgetState(
    val toolTip: @NlsContexts.Tooltip String?,
    val icon: Icon?
  )
}
