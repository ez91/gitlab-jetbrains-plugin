package com.gitlab.plugin.ui

import com.apollographql.apollo3.exception.ApolloException
import com.apollographql.apollo3.exception.ApolloHttpException
import com.apollographql.apollo3.exception.ApolloNetworkException
import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.*
import com.gitlab.plugin.api.duo.ExceptionHandler
import com.intellij.notification.NotificationType
import io.ktor.http.*

class CreateNotificationExceptionHandler(
  private val notificationManager: GitLabNotificationManager = GitLabNotificationManager(),
  private val settingsAction: NotificationAction = NotificationAction.settings(null)
) : ExceptionHandler {

  inner class ExceptionNotification<T : Throwable>(private val message: (ex: T) -> String) {
    private var shown: Boolean = false
    private val titleMessage = GitLabBundle.message("notification.title.gitlab-duo")

    fun show(exception: T, actions: List<NotificationAction> = listOf()) {
      if (shown) return

      val notification = Notification(titleMessage, message(exception), actions)

      notificationManager.sendNotification(notification, NotificationGroupType.IMPORTANT, NotificationType.ERROR)
      shown = true
    }

    fun reset() {
      shown = false
    }
  }

  private val offlineNotification =
    ExceptionNotification<GitLabOfflineException> { GitLabBundle.message("notification.exception.offline.message") }
  private val unauthorizedNotification =
    ExceptionNotification<GitLabUnauthorizedException> {
      GitLabBundle.message(
        "notification.exception.unauthorized.message"
      )
    }
  private val responseNotification =
    ExceptionNotification<GitLabResponseException> {
      GitLabBundle.message(
        "notification.exception.response.message",
        it.cachedResponseText
      )
    }
  private val proxyNotification =
    ExceptionNotification<GitLabProxyException> { GitLabBundle.message("notification.exception.proxy-failure.message") }
  private val forbiddenNotification =
    ExceptionNotification<GitLabForbiddenException> {
      GitLabBundle.message(
        "notification.exception.insufficient-scope.message"
      )
    }
  private val apolloNetworkExceptionNotification =
    ExceptionNotification<ApolloNetworkException> {
      GitLabBundle.message(
        "notification.exception.gitlab-instance-misconfiguration.message",
        it.platformCause.toString()
      )
    }
  private val apolloExceptionNotification =
    ExceptionNotification<ApolloException> {
      GitLabBundle.message(
        "notification.exception.general-error.message"
      )
    }
  private val apolloHttpExceptionNotification =
    ExceptionNotification<ApolloHttpException> {
      if (it.statusCode == HttpStatusCode.Unauthorized.value) {
        GitLabBundle.message("notification.exception.unauthorized.message")
      } else {
        GitLabBundle.message("notification.exception.general-error.message")
      }
    }

  private val notifications = listOf(
    offlineNotification,
    unauthorizedNotification,
    responseNotification,
    forbiddenNotification,
    proxyNotification,
    apolloNetworkExceptionNotification,
    apolloExceptionNotification,
    apolloHttpExceptionNotification
  )

  override fun handleException(exception: Throwable) = when (exception) {
    is GitLabForbiddenException -> forbiddenNotification.show(exception, listOf(settingsAction))
    is GitLabUnauthorizedException -> unauthorizedNotification.show(exception, listOf(settingsAction))
    is GitLabOfflineException -> offlineNotification.show(exception)
    is GitLabResponseException -> responseNotification.show(exception)
    is GitLabProxyException -> proxyNotification.show(exception)
    is ApolloNetworkException -> apolloNetworkExceptionNotification.show(exception)
    is ApolloHttpException -> apolloHttpExceptionNotification.show(exception)
    is ApolloException -> apolloExceptionNotification.show(exception)
    else -> throw exception
  }

  fun resetNotifications() = notifications.map { it.reset() }
}
