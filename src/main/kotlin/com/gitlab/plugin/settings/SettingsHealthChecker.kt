package com.gitlab.plugin.settings

import com.gitlab.plugin.services.health.HealthCheck
import com.gitlab.plugin.services.health.HealthCheckRequest
import com.gitlab.plugin.services.health.IntegrationHealthService
import com.intellij.icons.AllIcons
import com.intellij.ui.components.BrowserLink
import com.intellij.ui.dsl.builder.Cell
import javax.swing.Icon
import javax.swing.JLabel

class SettingsHealthChecker(
  private val errorReportingInstructions: Cell<BrowserLink>,
  private val tokenHealth: JLabel,
  private val serverVersionHealth: JLabel,
  private val currentUserHealth: JLabel,
  private val codeSuggestionHealth: JLabel,
  private val duoChatHealth: JLabel,
  private val verifyLabel: Cell<JLabel>,
  private val integrationHealthService: IntegrationHealthService = IntegrationHealthService()
) {

  fun check(request: HealthCheckRequest) {
    val integrationHealthResult = integrationHealthService.check(request)

    tokenHealth.setTextAndIcon(integrationHealthResult[HealthCheck.Name.TOKEN])
    serverVersionHealth.setTextAndIcon(integrationHealthResult[HealthCheck.Name.SERVER_VERSION])
    currentUserHealth.setTextAndIcon(integrationHealthResult[HealthCheck.Name.CURRENT_USER])
    codeSuggestionHealth.setTextAndIcon(integrationHealthResult[HealthCheck.Name.CODE_SUGGESTIONS])
    duoChatHealth.setTextAndIcon(integrationHealthResult[HealthCheck.Name.DUO_CHAT])

    errorReportingInstructions.visible(integrationHealthResult.hasError())
    verifyLabel.visible(false)
  }

  private fun JLabel.setTextAndIcon(healthCheck: HealthCheck) {
    text = healthCheck.message
    icon = healthCheck.status.toIcon()
  }

  private fun HealthCheck.Status.toIcon(): Icon {
    return when (this) {
      HealthCheck.Status.HEALTHY -> AllIcons.RunConfigurations.TestPassed
      HealthCheck.Status.DEGRADED -> AllIcons.General.Warning
      HealthCheck.Status.UNHEALTHY -> AllIcons.General.Error
    }
  }
}
