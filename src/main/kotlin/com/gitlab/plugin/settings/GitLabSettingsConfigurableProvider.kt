package com.gitlab.plugin.settings

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.GitLabSettingsConfigurable
import com.gitlab.plugin.asBeta
import com.intellij.openapi.options.Configurable
import com.intellij.openapi.options.ConfigurableProvider

const val ALPHA_RELEASE_CHANNEL = "alpha"

internal class GitLabSettingsConfigurableProvider : ConfigurableProvider() {
  override fun createConfigurable(): Configurable? = when {
    canCreateConfigurable() -> GitLabSettingsConfigurable().let {
      when (BuildConfig.RELEASE_CHANNEL) {
        ALPHA_RELEASE_CHANNEL -> it.asBeta()
        else -> it
      }
    }
    else -> null
  }
}
