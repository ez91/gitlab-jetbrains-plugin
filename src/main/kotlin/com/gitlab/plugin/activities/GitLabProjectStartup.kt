package com.gitlab.plugin.activities

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.duo.PatProvider
import com.gitlab.plugin.codesuggestions.license.CodeSuggestionsLicenseStatus
import com.gitlab.plugin.services.*
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.Notification
import com.gitlab.plugin.ui.NotificationAction
import com.gitlab.plugin.ui.NotificationGroupType
import com.gitlab.plugin.util.gitlabStatusDisabled
import com.gitlab.plugin.util.gitlabStatusVersionUnsupported
import com.intellij.notification.NotificationType
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.startup.ProjectActivity
import com.intellij.util.application
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class GitLabProjectStartup : ProjectActivity {
  override suspend fun execute(project: Project) {
    // Switch to the default context to avoid computing on EDT.
    // Follows [JetBrains tips and tricks](https://plugins.jetbrains.com/docs/intellij/coroutine-tips-and-tricks.html)
    // rather than the [detekt/Android recommendation](https://detekt.dev/docs/rules/coroutines/#injectdispatcher).
    @Suppress("InjectDispatcher")
    val codeSuggestionsStatusNotification = withContext(Dispatchers.Default) {
      computeCodeSuggestionsStatusNotification(project)
    }

    GitLabNotificationManager().sendNotification(
      codeSuggestionsStatusNotification,
      NotificationGroupType.GENERAL,
      NotificationType.INFORMATION
    )
  }

  private suspend fun computeCodeSuggestionsStatusNotification(project: Project): Notification {
    val applicationContextService = application.service<DuoContextService>()
    val projectContextService = project.service<ProjectContextService>()

    val token = application.service<PatProvider>().token()
    if (token.isEmpty()) {
      return Notification(
        "Get started with GitLab Duo Code Suggestions.",
        "You need to configure your GitLab credentials first.",
        listOf(NotificationAction.settings(project))
      )
    }

    val patStatus = application.service<PatStatusService>().currentStatus(token = token).apply {
      if (isValid) {
        application.service<GitLabUserService>().invalidateAndFetchCurrentUser(this)
        projectContextService.codeSuggestionsLicense.validateLicense()
      }
    }

    return when {
      !patStatus.isValid -> {
        Notification(
          GitLabBundle.message("notification.title.gitlab-duo"),
          patStatus.message(),
          listOf(NotificationAction.settings(project))
        )
      }

      !project.service<DuoEnabledProjectService>().isDuoEnabled() -> {
        gitlabStatusDisabled()
        Notification(
          GitLabBundle.message("notification.title.gitlab-duo"),
          GitLabBundle.message("notification.duo-disabled")
        )
      }

      !projectContextService.codeSuggestionsLicense.isLicensed -> {
        CodeSuggestionsLicenseStatus.licenseMissingNotification()
      }

      applicationContextService.serverServiceWithoutCallbacks.get().isVersionUpgradeRequired -> {
        gitlabStatusVersionUnsupported()
        GitLabServerService.versionUnsupportedNotification()
      }

      else -> {
        Notification(
          "Get started with GitLab Duo Code Suggestions.",
          "GitLab Duo Code Suggestions is ready to use with this project."
        )
      }
    }
  }
}
