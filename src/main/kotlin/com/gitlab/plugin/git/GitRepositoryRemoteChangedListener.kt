package com.gitlab.plugin.git

import com.gitlab.plugin.services.DuoEnabledProjectService
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import git4idea.repo.GitConfigListener
import git4idea.repo.GitRepository

class GitRepositoryRemoteChangedListener(private val project: Project) : GitConfigListener {
  private val duoEnabledProjectService by lazy { project.service<DuoEnabledProjectService>() }

  private lateinit var lastRemotes: Array<String>

  override fun notifyConfigChanged(repository: GitRepository) {
    val currentRemotes = repository.remotes.mapNotNull { it.firstUrl }.toTypedArray()

    if (!::lastRemotes.isInitialized || !lastRemotes.contentEquals(currentRemotes)) {
      lastRemotes = currentRemotes

      duoEnabledProjectService.invalidateAndFetch()
    }
  }
}
