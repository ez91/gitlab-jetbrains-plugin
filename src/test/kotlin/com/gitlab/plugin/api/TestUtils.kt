package com.gitlab.plugin.api

import com.gitlab.plugin.api.duo.*
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.codesuggestions.telemetry.Telemetry
import com.gitlab.plugin.services.DuoContextService
import com.gitlab.plugin.util.GitLabUtil
import com.intellij.openapi.application.Application
import com.intellij.openapi.application.ApplicationManager
import com.intellij.util.net.HttpConfigurable
import io.ktor.client.engine.*
import io.ktor.client.engine.mock.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.utils.io.*
import io.mockk.*
import kotlinx.coroutines.runBlocking
import javax.net.ssl.SSLContext

fun captureRequest(block: suspend (httpEngine: HttpClientEngine) -> HttpResponse?):
  Pair<HttpRequestData, HttpResponse?> = runBlocking {
  var data: HttpRequestData? = null

  val mockEngine = MockEngine { request ->
    data = request
    respond(content = ByteReadChannel(""))
  }

  val httpResponse = block.invoke(mockEngine)

  val requestData = data ?: throw RuntimeException("Request data not found")

  return@runBlocking Pair(requestData, httpResponse)
}

fun buildMockEngine(
  statusCode: HttpStatusCode = HttpStatusCode.OK,
  content: String = """{"some_property": "abc"}""",
  block: (HttpRequestData) -> Unit = {}
): HttpClientEngine = MockEngine { request ->
  block.invoke(request)

  respond(
    content = ByteReadChannel(content),
    status = statusCode,
    headers = headersOf(HttpHeaders.ContentType, "application/json")
  )
}

@Suppress("detekt:LongParameterList")
fun buildClient(
  tokenProvider: DuoClient.TokenProvider = DuoClient.TokenProvider { "some_pat" },
  exceptionHandler: ExceptionHandler = ExceptionHandler { throw it },
  httpClientEngine: HttpClientEngine = buildMockEngine(),
  userAgent: String = "gitlabUserAgent",
  onStatusChanged: DuoClient.DuoClientRequestListener = object : DuoClient.DuoClientRequestListener {},
  shouldRetry: Boolean = false
) = DuoClient(
  tokenProvider,
  exceptionHandler,
  userAgent,
  shouldRetry,
  httpClientEngine,
  "https://gitlab.com",
  onStatusChanged
)

data class TestRequest<T>(val data: HttpRequestData, val response: T)

suspend fun <T> makeTestRequest(content: String, block: suspend (DuoApi) -> T): TestRequest<T> {
  var data: HttpRequestData? = null
  val mockEngine =
    buildMockEngine(content = content) { request -> data = request }

  val api = buildApi(
    buildClient(httpClientEngine = mockEngine, tokenProvider = { "pat" })
  )

  val response = block.invoke(api)
  val requestData = data ?: throw RuntimeException("Request data not found")

  return TestRequest(requestData, response)
}

fun buildApi(
  client: DuoClient = buildClient(),
  telemetry: Telemetry = Telemetry.NOOP,
  duoSettings: DuoPersistentSettings = DuoPersistentSettings(),
  isLicensed: () -> Boolean = { true }
) =
  DuoApi(client, telemetry, duoSettings, isLicensed)

fun mockClient(content: String = """{"choices":[],"model":{}}"""): DuoClient {
  val mockEngine = buildMockEngine(content = content)
  return buildClient(
    httpClientEngine = mockEngine,
    tokenProvider = { "pat" }
  )
}

fun mockDuoContextServicePersistentSettings() {
  val duoPersistentSettings: DuoPersistentSettings = mockk()
  mockkObject(DuoPersistentSettings.Companion)
  every { DuoPersistentSettings.getInstance() } returns duoPersistentSettings
  every { DuoPersistentSettings.getInstance().url } returns "https://localhost:8443"

  val duoContextService: DuoContextService = mockk()
  mockkObject(DuoContextService.Companion)
  every { DuoContextService.instance } returns duoContextService
  every { duoContextService.duoSettings } returns duoPersistentSettings

  mockkObject(GitLabUtil)
  every { GitLabUtil.userAgent } returns "gitlab-plugin-test-agent"

  mockkStatic(HttpConfigurable::class)
  every { HttpConfigurable.getInstance() } returns mockk<HttpConfigurable>()
}

fun OkHttpConfig.configureTestClient(ignoreCertificateErrors: Boolean = false) {
  config {
    val trustManager = UserConfigurableTrustManager(
      mockk<DuoPersistentSettings>().also {
        every { it.ignoreCertificateErrors } returns ignoreCertificateErrors
      }
    )
    val sslContext = SSLContext.getInstance("TLS")
    sslContext.init(null, arrayOf(trustManager), null)
    sslSocketFactory(sslContext.socketFactory, trustManager)
  }
}

fun mockApplication() {
  mockkStatic(ApplicationManager::getApplication)

  val application = mockk<Application>(relaxed = true)
  every { ApplicationManager.getApplication() } returns application
}
