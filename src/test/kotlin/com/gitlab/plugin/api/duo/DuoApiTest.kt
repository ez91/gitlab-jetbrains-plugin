package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.*
import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.api.duo.requests.Metadata
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.codesuggestions.telemetry.Event
import com.gitlab.plugin.codesuggestions.telemetry.Telemetry
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import io.ktor.http.*
import io.mockk.*
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.util.concurrent.CancellationException

class DuoApiTest {
  private val mockPayload = mockk<Completion.Payload> {
    every { currentFile.contentAboveCursor } returns "01"
    every { currentFile.contentBelowCursor } returns "234"
  }
  private val eventContext =
    Event.Context(
      uuid = "uuid",
      modelEngine = "test-engine",
      modelName = "test-name",
      language = "test-lang",
      prefixLength = 2,
      suffixLength = 3
    )

  @Nested
  inner class MetadataRequest {
    @Test
    fun `creates the request`() = runTest {
      val (requestData, response) = makeTestRequest("""{"version":"1.2.3"}""") { it.metadata() }

      assertEquals(response, Metadata.Response("1.2.3"))
      assertEquals("https://gitlab.com/api/v4/metadata", requestData.url.toString())
    }

    @Test
    fun `creates the request according to settings`() = runTest {
      val settings = WorkspaceSettingsBuilder {
        url("https://gitlab.myinstance.com")
        token("glpat-xxxxxxxx")
      }

      val (requestData, response) = makeTestRequest("""{"version":"1.2.3"}""") { it.metadata(settings) }

      assertEquals(response, Metadata.Response("1.2.3"))
      assertEquals("https://gitlab.myinstance.com/api/v4/metadata", requestData.url.toString())
      assertEquals("Bearer glpat-xxxxxxxx", requestData.headers["Authorization"])
    }

    @Test
    fun `it returns no metadata if the request fails`() = runTest {
      val settings = WorkspaceSettingsBuilder {}

      val (_, response) = makeTestRequest(
        """{"code":"401", "message": "Unauthorized}"""
      ) { it.metadata(settings) }

      assertNull(response)
    }
  }

  @Nested
  inner class CompletionsRequest {
    @Test
    fun `creates the request`() = runTest {
      val (requestData, response) = makeTestRequest("""{"choices":[{"text":"text1"},{"text":"text2"}],"model":{}}""") {
        it.completions(mockPayload)
      }

      val expectedResponse = Completion.Response(
        choices = listOf(
          Completion.Response.Choice("text1"),
          Completion.Response.Choice("text2"),
        ),
        model = Completion.Response.Model()
      )

      assertEquals(response, expectedResponse)
      assertEquals("https://gitlab.com/api/v4/code_suggestions/completions", requestData.url.toString())
    }

    @Nested
    inner class IsDisabled {
      private val client = mockk<DuoClient>()
      private val duoSettings = mockk<DuoPersistentSettings>()
      private val telemetry: Telemetry = mockk()
      private val duoApi = buildApi(client, telemetry = telemetry, duoSettings = duoSettings)

      @BeforeEach
      fun setupMocks() {
        every { duoSettings.enabled } returns false
        every { client.isConfigured() } returns true
      }

      @Test
      fun `does not generate suggestions`() = runTest {
        assertNull(duoApi.completions(mockPayload))

        coVerify(exactly = 0) { client.post(any()) }
      }

      @Test
      fun `does not send telemetry`() = runTest {
        duoApi.completions(mockPayload)

        verify(exactly = 0) {
          telemetry.loaded(any())
          telemetry.requested(any())
          telemetry.error(any())
          telemetry.cancelled(any())
        }
      }

      @Suppress("ClassName")
      @Nested
      inner class `with missing license` {
        private val duoApi = buildApi(client, telemetry = telemetry, duoSettings = duoSettings, isLicensed = { false })

        @BeforeEach
        fun setupMocks() {
          every { duoSettings.enabled } returns true
          every { client.isConfigured() } returns true
        }

        @Test
        fun `does not generate suggestions`() = runTest {
          assertNull(duoApi.completions(mockPayload))

          coVerify(exactly = 0) { client.post(any()) }
        }

        @Test
        fun `does not send telemetry`() = runTest {
          duoApi.completions(mockPayload)

          verify(exactly = 0) {
            telemetry.loaded(any())
            telemetry.requested(any())
            telemetry.error(any())
            telemetry.cancelled(any())
          }
        }
      }
    }

    @Nested
    inner class IsNotConfigured {
      private val client = mockk<DuoClient>()
      private val duoSettings = mockk<DuoPersistentSettings>()
      private val telemetry: Telemetry = mockk()
      private val duoApi = buildApi(client, telemetry = telemetry, duoSettings = duoSettings)

      @BeforeEach
      fun setupMocks() {
        every { duoSettings.enabled } returns true
        every { client.isConfigured() } returns false
      }

      @Test
      fun `does not generate suggestions`() = runTest {
        assertNull(duoApi.completions(mockPayload))

        coVerify(exactly = 0) { client.post(any()) }
      }

      @Test
      fun `does not send telemetry`() = runTest {
        duoApi.completions(mockPayload)

        verify(exactly = 0) {
          telemetry.loaded(any())
          telemetry.requested(any())
          telemetry.error(any())
          telemetry.cancelled(any())
          telemetry.notProvided(any())
        }
      }
    }

    @Nested
    inner class IsConfiguredAndEnabled {
      private val client = mockk<DuoClient>()
      private val duoSettings = mockk<DuoPersistentSettings>()

      @BeforeEach
      fun setupMocks() {
        every { duoSettings.enabled } returns true
        every { client.isConfigured() } returns true
      }

      @Test
      fun `sends a telemetry request event`() = runTest {
        val telemetry: Telemetry = mockk {
          every { requested(any()) } returns Unit
          every { loaded(any()) } returns Unit
          every { newContext() } returns eventContext
        }
        val duoApi = buildApi(mockClient("""{"choices":[{"text":"text1"}],"model":{}}"""), telemetry, duoSettings)

        duoApi.completions(mockPayload)

        verify(exactly = 1) { telemetry.requested(eventContext) }
      }

      @Test
      fun `sends telemetry error event if no response is received`() = runTest {
        val mockEngine = buildMockEngine(HttpStatusCode.RequestTimeout)
        val telemetry: Telemetry = mockk {
          every { requested(any()) } returns Unit
          every { error(any()) } returns Unit
          every { newContext() } returns eventContext
        }
        val duoApi = buildApi(buildClient(httpClientEngine = mockEngine), telemetry, duoSettings)

        try {
          duoApi.completions(mockPayload)
        } catch (ex: GitLabResponseException) {
          // expected exception here since it's RequestTimeout
        }

        verify(exactly = 1) { telemetry.error(eventContext) }
      }

      @Test
      fun `sends a telemetry load event`() = runTest {
        val telemetry: Telemetry = mockk {
          every { requested(any()) } returns Unit
          every { loaded(any()) } returns Unit
          every { newContext() } returns eventContext
        }

        val duoApi = buildApi(
          mockClient(
            """{"choices":[{"text":"text1"}],"model":{"engine": "response-engine", "name": "response-name", "lang": "response-lang"}}"""
          ),
          telemetry,
          duoSettings
        )

        duoApi.completions(mockPayload)

        verify(exactly = 1) {
          telemetry.loaded(
            eventContext.copy(
              modelEngine = "response-engine",
              modelName = "response-name",
              language = "response-lang",
              apiStatusCode = HttpStatusCode.OK.value
            )
          )
        }
      }

      @Test
      fun `sends telemetry cancel event if CancellationException is caught`() = runTest {
        val telemetry: Telemetry = mockk {
          every { requested(any()) } returns Unit
          every { cancelled(any()) } returns Unit
          every { newContext() } returns eventContext
        }
        val duoClient: DuoClient = mockk {
          coEvery { post(any(), any()) } throws CancellationException()
          every { isConfigured() } returns true
        }

        val duoApi = buildApi(duoClient, telemetry, duoSettings)

        duoApi.completions(mockPayload)

        verify(exactly = 1) { telemetry.cancelled(eventContext) }
      }

      @Test
      fun `sends a telemetry not_provided event if no suggestions are returned`() = runTest {
        val telemetry: Telemetry = mockk {
          every { requested(any()) } returns Unit
          every { loaded(any()) } returns Unit
          every { notProvided(any()) } returns Unit
          every { newContext() } returns eventContext
        }

        val duoApi = buildApi(
          mockClient(
            """{"choices":[],"model":{"engine": "response-engine", "name": "response-name", "lang": "response-lang"}}"""
          ),
          telemetry,
          duoSettings
        )

        duoApi.completions(mockPayload)

        verify(exactly = 1) {
          telemetry.notProvided(
            eventContext.copy(
              modelEngine = "response-engine",
              modelName = "response-name",
              language = "response-lang",
              apiStatusCode = HttpStatusCode.OK.value
            )
          )
        }
      }
    }
  }
}
