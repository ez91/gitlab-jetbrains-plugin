package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.services.DuoContextService
import com.gitlab.plugin.util.TokenUtil
import com.gitlab.plugin.workspace.TokenSettings
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import com.intellij.execution.configurations.GeneralCommandLine
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*

class PatProviderTest : DescribeSpec({
  describe("token") {
    beforeEach {
      mockkConstructor(GeneralCommandLine::class)
      mockkConstructor(TokenSettings.OPSecretReference::class)
      mockDuoContextServicePersistentSettings()
      mockkObject(TokenUtil)
    }

    afterSpec { unmockkAll() }

    context("with default token settings") {
      context("without a personal access token configured") {
        it("returns the previously persisted token") {
          every { DuoContextService.instance.duoSettings.integrate1PasswordCLI } returns false
          every { TokenUtil.getToken() } returns null

          PatProvider().token() shouldBe ""
        }
      }

      context("without the 1password CLI integration configured") {
        it("returns the previously persisted token") {
          every { DuoContextService.instance.duoSettings.integrate1PasswordCLI } returns false
          every { TokenUtil.getToken() } returns "glpat-xxxxxxxx"

          PatProvider().token() shouldBe "glpat-xxxxxxxx"
        }
      }

      context("with the 1password CLI integration configured") {
        it("returns the personal access token stored in 1Password") {
          every { DuoContextService.instance.duoSettings.integrate1PasswordCLI } returns true
          every {
            DuoContextService.instance.duoSettings.integrate1PasswordCLISecretReference
          } returns "op://Private/GitLab Personal Access Token/token"
          every { TokenUtil.getToken() } returns null
          every { anyConstructed<TokenSettings.OPSecretReference>().getToken() } returns "glpat-xxxxxxxx"

          PatProvider().token() shouldBe "glpat-xxxxxxxx"
        }
      }

      context("with both personal access token and 1password CLI secret reference configured") {
        beforeEach {
          every {
            DuoContextService.instance.duoSettings.integrate1PasswordCLISecretReference
          } returns "op://Private/GitLab Personal Access Token/token"
          every { TokenUtil.getToken() } returns "glpat-xxxxxxxx"
          every { anyConstructed<TokenSettings.OPSecretReference>().getToken() } returns "glpat-yyyyyyyy"
        }

        it("fetches the secret from keychain if disabled") {
          every { DuoContextService.instance.duoSettings.integrate1PasswordCLI } returns false

          PatProvider().token() shouldBe "glpat-xxxxxxxx"

          verify(exactly = 0) { anyConstructed<TokenSettings.OPSecretReference>().getToken() }
        }

        it("fetches the secret from 1password if enabled") {
          every { DuoContextService.instance.duoSettings.integrate1PasswordCLI } returns true

          PatProvider().token() shouldBe "glpat-yyyyyyyy"

          verify(exactly = 1) { anyConstructed<TokenSettings.OPSecretReference>().getToken() }
        }
      }
    }

    context("with cached personal access token") {
      it("should not get keychain token again") {
        every { DuoContextService.instance.duoSettings.integrate1PasswordCLI } returns false
        every { TokenUtil.getToken() } returns "glpat-xxxxxxxx"
        val patProvider = PatProvider()

        patProvider.token() shouldBe "glpat-xxxxxxxx"
        patProvider.token() shouldBe "glpat-xxxxxxxx"

        verify(exactly = 1) { TokenUtil.getToken() }
      }

      it("should not get 1password token again") {
        every { DuoContextService.instance.duoSettings.integrate1PasswordCLI } returns true
        every {
          DuoContextService.instance.duoSettings.integrate1PasswordCLISecretReference
        } returns "op://Private/GitLab Personal Access Token/token"
        every { anyConstructed<TokenSettings.OPSecretReference>().getToken() } returns "glpat-xxxxxxxx"
        val patProvider = PatProvider()

        patProvider.token() shouldBe "glpat-xxxxxxxx"
        patProvider.token() shouldBe "glpat-xxxxxxxx"

        verify(exactly = 1) { anyConstructed<TokenSettings.OPSecretReference>().getToken() }
      }

      it("should update cached token with updated token settings") {
        every { DuoContextService.instance.duoSettings.integrate1PasswordCLI } returns false
        every { TokenUtil.getToken() } returns "glpat-xxxxxxxx"
        val patProvider = PatProvider()

        patProvider.token() shouldBe "glpat-xxxxxxxx"

        val updatedSettings = WorkspaceSettingsBuilder { token("glpat-v2xxxxxx") }
        patProvider.cacheToken(updatedSettings.token)

        patProvider.token() shouldBe "glpat-v2xxxxxx"
        verify(exactly = 1) { TokenUtil.getToken() }
      }
    }
  }
})
