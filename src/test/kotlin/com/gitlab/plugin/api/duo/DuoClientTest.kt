package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.*
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler
import io.ktor.client.call.*
import io.ktor.client.engine.mock.*
import io.ktor.client.network.sockets.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.mockk.*
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.nio.channels.UnresolvedAddressException
import kotlin.test.assertFailsWith

class DuoClientTest {
  private val exceptionHandlerMock = mockk<CreateNotificationExceptionHandler>()
  val mockListener = mockk<DuoClient.DuoClientRequestListener>()

  @BeforeEach
  fun init() {
    clearMocks(exceptionHandlerMock, mockListener)

    every { exceptionHandlerMock.handleException(any()) } returns Unit
    every { mockListener.onRequestStart() } returns Unit
    every { mockListener.onRequestError() } returns Unit
    every { mockListener.onRequestSuccess() } returns Unit
  }

  @Nested
  inner class RestClient {
    @Test
    fun `builds request headers correctly`() = runTest {
      val pat = "VALID_PAT"

      val requestData = captureRequest { mockEngine ->
        buildClient(httpClientEngine = mockEngine, tokenProvider = { pat })
          .restClient
          .get("someEndpoint")
      }.first

      assertEquals("application/json", requestData.headers[HttpHeaders.ContentType])
      assertEquals("gitlabUserAgent", requestData.headers[HttpHeaders.UserAgent])
    }

    @Test
    fun `converts response object keys from underscore`() = runTest {
      val body = buildClient(httpClientEngine = buildMockEngine(content = """{"some_property": "abc"}"""))
        .restClient
        .get("someEndpoint")
        .body<Response>()

      assertEquals(Response(someProperty = "abc"), body)
    }

    @Test
    fun `throws GitLabOfflineException on UnresolvedAddressException`() {
      val mockEngine = MockEngine { _ -> throw UnresolvedAddressException() }

      assertFailsWith<GitLabOfflineException> {
        runBlocking {
          buildClient(httpClientEngine = mockEngine)
            .restClient
            .get("some_endpoint")
        }
      }
    }

    @Test
    fun `throws GitLabOfflineException on ConnectTimeoutException`() {
      val mockEngine = MockEngine { _ -> throw ConnectTimeoutException("timeout") }

      val client = buildClient(httpClientEngine = mockEngine).restClient

      assertFailsWith<GitLabOfflineException> {
        runBlocking { client.get("some_endpoint") }
      }
    }

    @Test
    fun `throws GitLabUnauthorizedException on 4xx responses`() = runTest {
      val mockEngine = buildMockEngine(HttpStatusCode.BadRequest)

      val client = buildClient(httpClientEngine = mockEngine)

      assertFailsWith<GitLabResponseException> {
        client.get("some_endpoint")
      }
    }

    @Test
    fun `throws GitLabForbiddenException on 403 responses`() = runTest {
      val mockEngine = buildMockEngine(HttpStatusCode.Forbidden, "Error")

      val client = buildClient(httpClientEngine = mockEngine)

      assertFailsWith<GitLabForbiddenException>("Error") {
        client.get("some_endpoint")
      }
    }

    @Test
    fun `throws GitLabResponseException on unauthorized`() = runTest {
      val mockEngine = buildMockEngine(HttpStatusCode.Unauthorized, "You are not logged in")

      val client = buildClient(httpClientEngine = mockEngine)

      assertFailsWith<GitLabUnauthorizedException>("You are not logged in") {
        client.get("some_endpoint")
      }
    }

    @Test
    fun `calls listener methods on request success`() = runTest {
      buildClient(onStatusChanged = mockListener)
        .restClient
        .get("someEndpoint")

      verifySequence {
        mockListener.onRequestStart()
        mockListener.onRequestSuccess()
      }
    }

    @Test
    fun `calls listener methods on request error`() = runTest {
      val mockEngine = buildMockEngine(HttpStatusCode.BadRequest)

      try {
        buildClient(httpClientEngine = mockEngine, onStatusChanged = mockListener).restClient.get("someEndpoint")
      } catch (ex: GitLabResponseException) {
        // expected exception here since it's BadRequest
      }

      verifySequence {
        mockListener.onRequestStart()
        mockListener.onRequestError()
      }
    }
  }

  @Nested
  inner class Get {
    @Test
    fun `creates the request passing additional options`() = runTest {
      val pat = "a_pat"

      val requestData = captureRequest { mockEngine ->
        buildClient(httpClientEngine = mockEngine, tokenProvider = { pat })
          .get("someEndpoint") {
            header("SOME_HEADER", "SOME_VALUE")
          }
      }.first

      assertEquals(HttpMethod.Get, requestData.method)
      assertEquals("https://gitlab.com/someEndpoint", requestData.url.toString())
      assertEquals("SOME_VALUE", requestData.headers["SOME_HEADER"])
      assertEquals("Bearer $pat", requestData.headers[HttpHeaders.Authorization])
    }

    @Test
    fun `handles exception using the exception handler`() = runTest {
      buildClient(
        httpClientEngine = buildMockEngine(HttpStatusCode.BadRequest),
        exceptionHandler = exceptionHandlerMock
      )
        .get("someEndpoint")

      verify(exactly = 1) { exceptionHandlerMock.handleException(ofType<GitLabResponseException>()) }
    }
  }

  @Nested
  inner class Post {
    @Test
    fun `creates the request passing additional options`() = runTest {
      val pat = "a_pat"

      val requestData = captureRequest { mockEngine ->
        buildClient(httpClientEngine = mockEngine, tokenProvider = { pat })
          .post("someEndpoint") {
            header("SOME_HEADER", "SOME_VALUE")
          }
      }.first

      assertEquals(HttpMethod.Post, requestData.method)
      assertEquals("https://gitlab.com/someEndpoint", requestData.url.toString())
      assertEquals("SOME_VALUE", requestData.headers["SOME_HEADER"])
      assertEquals("Bearer $pat", requestData.headers[HttpHeaders.Authorization])
    }

    @Test
    fun `handles exception using the exception handler`() = runTest {
      buildClient(
        httpClientEngine = buildMockEngine(HttpStatusCode.BadRequest),
        exceptionHandler = exceptionHandlerMock
      )
        .post("someEndpoint")

      verify(exactly = 1) { exceptionHandlerMock.handleException(ofType<GitLabResponseException>()) }
    }
  }

  private data class Response(val someProperty: String)
}
