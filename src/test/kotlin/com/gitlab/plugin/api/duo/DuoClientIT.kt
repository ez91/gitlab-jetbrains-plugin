package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.configureTestClient
import com.gitlab.plugin.api.duo.DuoClient.DuoClientRequestListener
import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.integrationtest.IntegrationTestEnvironment
import com.gitlab.plugin.integrationtest.IntegrationTestEnvironment.Configured
import com.gitlab.plugin.services.DuoContextService
import io.kotest.core.annotation.EnabledIf
import io.kotest.core.spec.style.DescribeSpec
import io.ktor.client.engine.okhttp.*
import io.mockk.*
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import javax.net.ssl.SSLHandshakeException

@EnabledIf(Configured::class)
class DuoClientIT : DescribeSpec({
  val env = IntegrationTestEnvironment.forSpec(
    DuoClientIT::class,
    IntegrationTestEnvironment(
      personalAccessToken = System.getenv("TEST_ACCESS_TOKEN"),
      gitlabHost = System.getenv("TEST_GITLAB_PROXY_HOST") ?: "https://localhost:8443",
      kclass = DuoClientIT::class,
    )
  )

  fun subject(ignoreCertificateErrors: Boolean) = DuoClient(
    tokenProvider = env.personalAccessTokenProvider,
    exceptionHandler = { throw it },
    userAgent = "gitlab-duo-plugin-integration-test",
    shouldRetry = false,
    httpClientEngine = OkHttp.create {
      configureTestClient(ignoreCertificateErrors = ignoreCertificateErrors)
    },
    host = env.gitlabHost,
    onStatusChanged = mockk<DuoClientRequestListener>(relaxUnitFun = true),
  )

  describe("a non-trusted certificate chain") {
    beforeEach {
      mockDuoContextServicePersistentSettings()
    }

    afterEach {
      clearAllMocks()
    }

    afterSpec {
      unmockkAll()
    }

    it("ignoring certificate errors").config(enabledIf = env.running()) {
      every { DuoContextService.instance.duoSettings.ignoreCertificateErrors } returns true
      subject(ignoreCertificateErrors = true).let {
        assertDoesNotThrow { it.get("/api/v4/metadata") }
      }
    }

    it("without ignoring certificate errors").config(enabledIf = env.running()) {
      every { DuoContextService.instance.duoSettings.ignoreCertificateErrors } returns false
      subject(ignoreCertificateErrors = false).let {
        assertThrows<SSLHandshakeException> { it.get("/api/v4/metadata") }
      }
    }
  }
})
