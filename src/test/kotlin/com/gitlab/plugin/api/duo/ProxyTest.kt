package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.ProxyManager
import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.api.setupProxy
import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.utils.io.errors.*
import io.mockk.clearAllMocks
import io.mockk.unmockkAll
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockserver.client.MockServerClient
import org.mockserver.junit.jupiter.MockServerExtension
import org.mockserver.junit.jupiter.MockServerSettings
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response
import java.net.Proxy
import kotlin.test.assertNotNull

@ExtendWith(MockServerExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@MockServerSettings(ports = [1234])
class ProxyTest {
  private lateinit var mockServerClient: MockServerClient
  private lateinit var mockServerUrl: String

  @BeforeAll
  fun beforeAll(mockServerClient: MockServerClient) {
    this.mockServerClient = mockServerClient
    this.mockServerUrl = "http://localhost:${mockServerClient.port}"
  }

  @BeforeEach
  fun beforeEach() {
    mockDuoContextServicePersistentSettings()
    mockServerClient.reset()
  }

  @AfterEach
  fun afterEach() {
    clearAllMocks()
  }

  @AfterAll
  fun afterAll() {
    unmockkAll()
  }

  @Test
  fun `when proxy is provided it sets up proxy`() = runTest {
    mockServerClient.`when`(
      request().withMethod("GET").withPath("/ping")
    ).respond(
      response().withStatusCode(200)
    )

    val proxy = ProxyManager.ProxyInfo(
      Proxy.Type.HTTP,
      "http://proxy-server",
      1234,
      "user",
      "12345",
      true
    )

    val httpClient = HttpClient(OkHttp) {
      engine {
        setupProxy(proxy)
      }
    }

    var exception: IOException? = null

    try {
      val response = httpClient.get("$mockServerUrl/ping")

      println(response)
    } catch (ex: IOException) {
      exception = ex
    }

    assertNotNull(exception)
    assert(exception.message!!.contains("http://proxy-server")) // This means it tried to redirect
  }

  @Test
  fun `when proxy is not provided it does not setup proxy`() = runTest {
    mockServerClient.`when`(
      request().withMethod("GET").withPath("/ping")
    ).respond(
      response().withStatusCode(200)
    )

    val httpClient = HttpClient(OkHttp) {
      engine {
        setupProxy(null)
      }
    }

    val response = httpClient.get("$mockServerUrl/ping")

    assert(response.status == HttpStatusCode.OK) // This means it tried to redirect
  }
}
