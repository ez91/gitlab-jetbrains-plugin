package com.gitlab.plugin.api

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.DescribeSpec
import io.ktor.client.call.*
import io.ktor.client.network.sockets.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.utils.io.errors.*
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkClass
import kotlinx.coroutines.runBlocking
import java.nio.channels.UnresolvedAddressException

class HttpExceptionHandlerTest : DescribeSpec({
  val httpRequest = mockk<HttpRequest>(relaxed = true)

  it("should throw GitLabUnauthorizedException when status code is 401") {
    val httpResponse = httpResponse(statusCode = HttpStatusCode.Unauthorized)
    val cause = mockk<ClientRequestException> {
      every { response } returns httpResponse
    }

    shouldThrow<GitLabUnauthorizedException> {
      runBlocking {
        HttpExceptionHandler.handle(cause, httpRequest)
      }
    }
  }

  it("should throw GitLabForbiddenException when status code is 403") {
    val httpResponse = httpResponse(statusCode = HttpStatusCode.Forbidden)
    val cause = mockk<ClientRequestException> {
      every { response } returns httpResponse
    }

    shouldThrow<GitLabForbiddenException> {
      runBlocking {
        HttpExceptionHandler.handle(cause, httpRequest)
      }
    }
  }

  describe("GitLabProxyException") {
    listOf(
      HttpStatusCode.ProxyAuthenticationRequired,
      HttpStatusCode.PayloadTooLarge,
    ).forEach { code ->
      it("should throw GitLabProxyException when status code is ${code.value}") {
        val httpResponse = httpResponse(statusCode = code)
        val cause = mockk<ClientRequestException> {
          every { response } returns httpResponse
          every { message } returns code.description
        }

        shouldThrow<GitLabProxyException> {
          runBlocking {
            HttpExceptionHandler.handle(cause, httpRequest)
          }
        }
      }
    }

    it("should throw GitLabProxyException when IOException message contains 431") {
      val cause = mockk<IOException> {
        every { message } returns "431 Request header fields too large."
      }

      shouldThrow<GitLabProxyException> {
        HttpExceptionHandler.handle(cause, httpRequest)
      }
    }
  }

  it("should throw GitLabResponseException unhandled status code") {
    val httpResponse = httpResponse(statusCode = HttpStatusCode.NotFound)
    val cause = mockk<ClientRequestException> {
      every { response } returns httpResponse
    }

    shouldThrow<GitLabResponseException> {
      runBlocking {
        HttpExceptionHandler.handle(cause, httpRequest)
      }
    }
  }

  describe("GitLabOfflineException") {
    listOf(
      UnresolvedAddressException::class,
      ConnectTimeoutException::class,
      HttpRequestTimeoutException::class
    ).forEach { exception ->
      it("should throw GitLabOfflineException when cause is ${exception.simpleName}") {
        val cause = mockkClass(exception, relaxed = true)

        shouldThrow<GitLabOfflineException> {
          HttpExceptionHandler.handle(cause, httpRequest)
        }
      }
    }
  }

  it("should re throw unhandled exception") {
    val cause = mockk<Exception>(relaxed = true)

    shouldThrow<Exception> {
      HttpExceptionHandler.handle(cause, httpRequest)
    }
  }
})

private fun httpResponse(statusCode: HttpStatusCode): HttpResponse {
  return mockk<HttpResponse>(relaxed = true) {
    every { status } returns statusCode
    coEvery { body<String>() } returns statusCode.description
  }
}
