package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.graphql.ProjectQuery
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.mockk.*

class DuoEnabledProjectServiceTest : DescribeSpec({
  beforeContainer {
    mockkConstructor(GraphQLApi::class)
    mockkConstructor(ApolloClientFactory::class)
  }

  afterContainer {
    unmockkAll()
  }

  describe("DuoEnabledProjectService") {
    val gitLabProjectService = mockk<GitLabProjectService>(relaxed = true)
    val projectContextService = mockk<ProjectContextService>(relaxed = true)

    val projectPath = "test/test-project"
    val project = mockk<Project>()

    lateinit var service: DuoEnabledProjectService

    beforeEach {
      every { project.service<GitLabProjectService>() } returns gitLabProjectService
      every { gitLabProjectService.getCurrentProjectPath() } returns projectPath

      every { project.name } returns "test-name"
      every { project.getService(ProjectContextService::class.java) } returns projectContextService

      service = DuoEnabledProjectService(project)
    }

    afterEach {
      clearAllMocks()
    }

    describe("isDuoEnabledForProject") {
      it("returns false for a project with duo disabled") {
        coEvery {
          projectContextService.graphQLApi.getProject(projectPath)
        } returns ProjectQuery.Project(false)

        service.isDuoEnabled().shouldBeFalse()
      }

      it("returns true for a project with duo enabled") {
        coEvery {
          projectContextService.graphQLApi.getProject(projectPath)
        } returns ProjectQuery.Project(true)

        service.isDuoEnabled().shouldBeTrue()
      }

      it("returns true when given an empty project path") {
        coEvery {
          projectContextService.graphQLApi.getProject("")
        } returns ProjectQuery.Project(true)
        every { gitLabProjectService.getCurrentProjectPath() } returns ""

        service.isDuoEnabled().shouldBeTrue()
      }

      context("Using GitLab 16.8 without feature flags") {
        it("returns and caches true") {
          coEvery {
            projectContextService.graphQLApi.getProject(projectPath)
          } returns null

          service.isDuoEnabled().shouldBeTrue()
        }
      }

      it("returns cached duo enabled value for given project") {
        coEvery {
          projectContextService.graphQLApi.getProject(projectPath)
        } returns ProjectQuery.Project(true)

        service.isDuoEnabled().shouldBeTrue()
        service.isDuoEnabled().shouldBeTrue()

        coVerify(exactly = 1) { projectContextService.graphQLApi.getProject(projectPath) }
      }

      it("invalidates and fetch the duo enabled value for given project") {
        coEvery {
          projectContextService.graphQLApi.getProject(projectPath)
        } returns ProjectQuery.Project(true)

        service.isDuoEnabled().shouldBeTrue()

        coEvery {
          projectContextService.graphQLApi.getProject(projectPath)
        } returns ProjectQuery.Project(false)
        service.invalidateAndFetch()

        service.isDuoEnabled().shouldBeFalse()
      }
    }
  }
})
