package com.gitlab.plugin.services.health

import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.runBlocking

class IntegrationHealthServiceTest : DescribeSpec({
  val request = HealthCheckRequest(WorkspaceSettingsBuilder {}, project = null)

  val healthCheckService1 = mockk<HealthCheckService>()
  val healthCheckService2 = mockk<HealthCheckService>()

  val integrationHealthService = IntegrationHealthService(
    listOf(healthCheckService1, healthCheckService2)
  )

  it("should collect health checks from all health check services") {
    val healthyCheck = HealthCheck(
      name = HealthCheck.Name.TOKEN,
      message = "In good health.",
      status = HealthCheck.Status.HEALTHY
    )

    val unhealthyCheck = HealthCheck(
      name = HealthCheck.Name.CURRENT_USER,
      message = "Unhealthy.",
      status = HealthCheck.Status.HEALTHY
    )

    val degraded = HealthCheck(
      name = HealthCheck.Name.CODE_SUGGESTIONS,
      message = "Degraded.",
      status = HealthCheck.Status.DEGRADED
    )

    coEvery { healthCheckService1.check(any()) } returns listOf(healthyCheck)
    coEvery { healthCheckService2.check(any()) } returns listOf(unhealthyCheck, degraded)

    val health = runBlocking {
      integrationHealthService.check(request)
    }

    health[HealthCheck.Name.TOKEN] shouldBe healthyCheck
    health[HealthCheck.Name.CURRENT_USER] shouldBe unhealthyCheck
    health[HealthCheck.Name.CODE_SUGGESTIONS] shouldBe degraded
  }
})
