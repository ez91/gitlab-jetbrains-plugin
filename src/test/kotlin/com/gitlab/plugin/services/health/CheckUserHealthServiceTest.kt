package com.gitlab.plugin.services.health

import com.apollographql.apollo3.ApolloClient
import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.graphql.CurrentUserQuery
import com.gitlab.plugin.services.DuoEnabledProjectService
import com.gitlab.plugin.workspace.WorkspaceSettings
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkConstructor
import kotlinx.coroutines.runBlocking

class CheckUserHealthServiceTest : DescribeSpec({
  val url = "https://gitlab.com"
  val token = "token"
  val project = mockk<Project>()

  val requestOutsideProject = HealthCheckRequest(
    workspaceSettings = WorkspaceSettingsBuilder {
      url(url)
      token(token)
    },
    project = null
  )

  val duoEnabledProjectService = mockk<DuoEnabledProjectService>()
  val apolloClientFactory = mockk<ApolloClientFactory>()
  val checkUserHealthService = CheckUserHealthService(apolloClientFactory)

  beforeEach {
    val apolloClient = mockk<ApolloClient>()
    every { apolloClientFactory.create(any<WorkspaceSettings>()) } returns apolloClient

    every { duoEnabledProjectService.isDuoEnabled() } returns true
    every { project.getService(DuoEnabledProjectService::class.java) } returns duoEnabledProjectService

    setupCurrentUser(null)
  }

  describe("Current User Health Check") {
    it("should not be healthy if current user is null") {
      setupCurrentUser(null)

      val checks = runBlocking {
        checkUserHealthService.check(requestOutsideProject)
      }

      checks[0].name shouldBe HealthCheck.Name.CURRENT_USER
      checks[0].status shouldBe HealthCheck.Status.UNHEALTHY
      checks[0].message shouldBe "Unauthorized to fetch the current user."
    }

    it("should be healthy if current user is not null") {
      setupCurrentUser(currentUser = mockk(relaxed = true))

      val checks = runBlocking {
        checkUserHealthService.check(requestOutsideProject)
      }

      checks[0].name shouldBe HealthCheck.Name.CURRENT_USER
      checks[0].status shouldBe HealthCheck.Status.HEALTHY
      checks[0].message shouldBe "Authorized to fetch the current user."
    }
  }

  describe("Code Suggestions Health Check") {
    it("should be degraded if current user is null") {
      setupCurrentUser(null)

      val checks = runBlocking {
        checkUserHealthService.check(requestOutsideProject)
      }

      checks[1].name shouldBe HealthCheck.Name.CODE_SUGGESTIONS
      checks[1].status shouldBe HealthCheck.Status.DEGRADED
      checks[1].message shouldBe "Code Suggestions are disabled."
    }

    it("should be degraded if project has duo features disabled") {
      val request = HealthCheckRequest(
        workspaceSettings = WorkspaceSettingsBuilder {
          url(url)
          token(token)
        },
        project = project,
      )
      every { duoEnabledProjectService.isDuoEnabled() } returns false
      val currentUser = mockk<CurrentUserQuery.CurrentUser>(relaxed = true) {
        every { duoCodeSuggestionsAvailable } returns true
      }
      setupCurrentUser(currentUser)

      val checks = runBlocking {
        checkUserHealthService.check(request)
      }

      checks[1].name shouldBe HealthCheck.Name.CODE_SUGGESTIONS
      checks[1].status shouldBe HealthCheck.Status.DEGRADED
      checks[1].message shouldBe "Code Suggestions are disabled because GitLab Duo features are disabled for this project."
    }

    it("should be degraded if current user has code suggestion disabled") {
      val currentUser = mockk<CurrentUserQuery.CurrentUser>(relaxed = true) {
        every { duoCodeSuggestionsAvailable } returns false
      }
      setupCurrentUser(currentUser)

      val checks = runBlocking {
        checkUserHealthService.check(requestOutsideProject)
      }

      checks[1].name shouldBe HealthCheck.Name.CODE_SUGGESTIONS
      checks[1].status shouldBe HealthCheck.Status.DEGRADED
      checks[1].message shouldBe "Code Suggestions are disabled."
    }

    it("should display disabled message if current user is null and code suggestions are disabled in settings") {
      val request = requestOutsideProject(
        WorkspaceSettingsBuilder {
          url(url)
          token(token)
          duoChatEnabled = false
        }
      )
      setupCurrentUser(null)

      val checks = runBlocking {
        checkUserHealthService.check(request)
      }

      checks[1].name shouldBe HealthCheck.Name.CODE_SUGGESTIONS
      checks[1].status shouldBe HealthCheck.Status.DEGRADED
      checks[1].message shouldBe "Code Suggestions are disabled."
    }

    it("should be degraded if code suggestions have been manually disabled in settings") {
      val request = requestOutsideProject(
        WorkspaceSettingsBuilder {
          url(url)
          token(token)
          codeSuggestionEnabled = false
        }
      )
      val currentUser = mockk<CurrentUserQuery.CurrentUser>(relaxed = true) {
        every { duoCodeSuggestionsAvailable } returns true
      }
      setupCurrentUser(currentUser)

      val checks = runBlocking {
        checkUserHealthService.check(request)
      }

      checks[1].name shouldBe HealthCheck.Name.CODE_SUGGESTIONS
      checks[1].status shouldBe HealthCheck.Status.DEGRADED
      checks[1].message shouldBe "Code Suggestions manually disabled in settings."
    }

    it("should be healthy if current user has code suggestion disabled") {
      val currentUser = mockk<CurrentUserQuery.CurrentUser>(relaxed = true) {
        every { duoCodeSuggestionsAvailable } returns true
      }
      setupCurrentUser(currentUser)

      val checks = runBlocking {
        checkUserHealthService.check(requestOutsideProject)
      }

      checks[1].name shouldBe HealthCheck.Name.CODE_SUGGESTIONS
      checks[1].status shouldBe HealthCheck.Status.HEALTHY
      checks[1].message shouldBe "Code Suggestions are enabled."
    }
  }

  describe("Duo Chat Health Check") {
    it("should be degraded if current user is null") {
      setupCurrentUser(null)

      val checks = runBlocking {
        checkUserHealthService.check(requestOutsideProject)
      }

      checks[2].name shouldBe HealthCheck.Name.DUO_CHAT
      checks[2].status shouldBe HealthCheck.Status.DEGRADED
      checks[2].message shouldBe "GitLab Duo Chat is disabled."
    }

    it("should be degraded if project has duo features disabled") {
      val request = HealthCheckRequest(
        workspaceSettings = WorkspaceSettingsBuilder {
          url(url)
          token(token)
        },
        project = project,
      )
      every { duoEnabledProjectService.isDuoEnabled() } returns false
      val currentUser = mockk<CurrentUserQuery.CurrentUser>(relaxed = true) {
        every { duoChatAvailable } returns true
      }
      setupCurrentUser(currentUser)

      val checks = runBlocking {
        checkUserHealthService.check(request)
      }

      checks[2].name shouldBe HealthCheck.Name.DUO_CHAT
      checks[2].status shouldBe HealthCheck.Status.DEGRADED
      checks[2].message shouldBe "GitLab Duo Chat is disabled because GitLab Duo features are disabled for this project."
    }

    it("should be degraded if current user has duo chat disabled") {
      val currentUser = mockk<CurrentUserQuery.CurrentUser>(relaxed = true) {
        every { duoChatAvailable } returns false
      }
      setupCurrentUser(currentUser)

      val checks = runBlocking {
        checkUserHealthService.check(requestOutsideProject)
      }

      checks[2].name shouldBe HealthCheck.Name.DUO_CHAT
      checks[2].status shouldBe HealthCheck.Status.DEGRADED
      checks[2].message shouldBe "GitLab Duo Chat is disabled."
    }

    it("should be display disabled message if current user is null and duo chat is disabled in settings") {
      val request = requestOutsideProject(
        WorkspaceSettingsBuilder {
          url(url)
          token(token)
          duoChatEnabled = false
        }
      )
      setupCurrentUser(null)

      val checks = runBlocking {
        checkUserHealthService.check(request)
      }

      checks[2].name shouldBe HealthCheck.Name.DUO_CHAT
      checks[2].status shouldBe HealthCheck.Status.DEGRADED
      checks[2].message shouldBe "GitLab Duo Chat is disabled."
    }

    it("should be degraded if duo chat has been manually disabled in settings") {
      val request = requestOutsideProject(
        WorkspaceSettingsBuilder {
          url(url)
          token(token)
          duoChatEnabled = false
        }
      )
      val currentUser = mockk<CurrentUserQuery.CurrentUser>(relaxed = true) {
        every { duoChatAvailable } returns true
      }
      setupCurrentUser(currentUser)

      val checks = runBlocking {
        checkUserHealthService.check(request)
      }

      checks[2].name shouldBe HealthCheck.Name.DUO_CHAT
      checks[2].status shouldBe HealthCheck.Status.DEGRADED
      checks[2].message shouldBe "GitLab Duo Chat manually disabled in settings."
    }

    it("should be healthy if current user has duo chat disabled") {
      val currentUser = mockk<CurrentUserQuery.CurrentUser>(relaxed = true) {
        every { duoChatAvailable } returns true
      }
      setupCurrentUser(currentUser)

      val checks = runBlocking {
        checkUserHealthService.check(requestOutsideProject)
      }

      checks[2].name shouldBe HealthCheck.Name.DUO_CHAT
      checks[2].status shouldBe HealthCheck.Status.HEALTHY
      checks[2].message shouldBe "GitLab Duo Chat is enabled."
    }
  }
})

private fun setupCurrentUser(currentUser: CurrentUserQuery.CurrentUser?) {
  mockkConstructor(GraphQLApi::class)
  coEvery { anyConstructed<GraphQLApi>().getCurrentUser() } returns currentUser
}

private fun requestOutsideProject(settings: WorkspaceSettings) = HealthCheckRequest(
  workspaceSettings = settings,
  project = null
)
