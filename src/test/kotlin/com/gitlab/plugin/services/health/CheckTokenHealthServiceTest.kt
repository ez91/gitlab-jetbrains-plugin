package com.gitlab.plugin.services.health

import com.gitlab.plugin.api.GitLabForbiddenException
import com.gitlab.plugin.api.GitLabOfflineException
import com.gitlab.plugin.api.GitLabProxyException
import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.services.PatStatusService
import com.gitlab.plugin.services.pat.PatStatus
import com.gitlab.plugin.workspace.WorkspaceSettings
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkClass
import kotlinx.coroutines.runBlocking

class CheckTokenHealthServiceTest : DescribeSpec({
  val url = "https://gitlab.com"
  val token = "token"
  val heathCheckRequest = request(
    WorkspaceSettingsBuilder {
      url(url)
      token(token)
    }
  )

  val patStatusService = mockk<PatStatusService>()
  val checkTokenHealthService = CheckTokenHealthService(patStatusService)

  it("should not be healthy if the token is empty") {
    val settings = request(settings = WorkspaceSettingsBuilder { token("") })

    val result = runBlocking {
      checkTokenHealthService.check(settings)
    }

    result[0].name shouldBe HealthCheck.Name.TOKEN
    result[0].status shouldBe HealthCheck.Status.UNHEALTHY
    result[0].message shouldBe "Personal access token is empty."
  }

  it("should use pat status message if token is accepted") {
    val patStatus = mockk<PatStatus.Accepted> {
      every { message() } returns "PatStatus accepted message."
      every { isValid } returns true
    }
    coEvery { patStatusService.currentStatus(url, token) } returns patStatus

    val result = runBlocking {
      checkTokenHealthService.check(heathCheckRequest)
    }

    result[0].message shouldBe "PatStatus accepted message."
  }

  it("should be healthy if pat status is valid") {
    val patStatus = mockk<PatStatus.Accepted> {
      every { message() } returns "PatStatus accepted message."
      every { isValid } returns true
    }
    coEvery { patStatusService.currentStatus(url, token) } returns patStatus

    val result = runBlocking {
      checkTokenHealthService.check(heathCheckRequest)
    }

    result[0].status shouldBe HealthCheck.Status.HEALTHY
  }

  it("should not be healthy if pat status is not valid") {
    val patStatus = mockk<PatStatus.Accepted> {
      every { message() } returns "PatStatus accepted message."
      every { isValid } returns false
    }
    coEvery { patStatusService.currentStatus(url, token) } returns patStatus

    val result = runBlocking {
      checkTokenHealthService.check(heathCheckRequest)
    }

    result[0].status shouldBe HealthCheck.Status.UNHEALTHY
  }

  listOf(
    GitLabForbiddenException::class to "Personal Access Token is insufficient.",
    GitLabUnauthorizedException::class to "Personal Access Token is invalid.",
  ).forEach { (cause, message) ->
    it("should compute health message from cause if token is refused because of ${cause.simpleName}") {
      val patStatus = PatStatus.Refused(url, 999, mockkClass(cause))
      coEvery { patStatusService.currentStatus(url, token) } returns patStatus

      val result = runBlocking {
        checkTokenHealthService.check(heathCheckRequest)
      }

      result[0].name shouldBe HealthCheck.Name.TOKEN
      result[0].message shouldBe message
      result[0].status shouldBe HealthCheck.Status.UNHEALTHY
    }
  }

  listOf(
    GitLabOfflineException::class to "Server cannot be reached.",
    GitLabProxyException::class to "Proxy is not set up correctly.",
  ).forEach { (cause, message) ->
    it("should compute health message from cause if token is unknown because of ${cause.simpleName}") {
      val patStatus = PatStatus.Unknown(url, mockkClass(cause))
      coEvery { patStatusService.currentStatus(url, token) } returns patStatus

      val result = runBlocking {
        checkTokenHealthService.check(heathCheckRequest)
      }

      result[0].name shouldBe HealthCheck.Name.TOKEN
      result[0].message shouldBe message
      result[0].status shouldBe HealthCheck.Status.UNHEALTHY
    }
  }
})

private fun request(settings: WorkspaceSettings) = HealthCheckRequest(
  settings,
  project = null
)
