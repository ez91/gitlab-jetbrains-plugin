package com.gitlab.plugin.services.health

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe

class IntegrationHealthTest : DescribeSpec({
  it("should have error if any check is in error") {
    val integrationHealthResult = IntegrationHealthResult(
      checks = mapOf(
        HealthCheck.Name.TOKEN to HealthCheck(HealthCheck.Name.TOKEN, "a", HealthCheck.Status.HEALTHY),
        HealthCheck.Name.CODE_SUGGESTIONS to HealthCheck(HealthCheck.Name.CODE_SUGGESTIONS, "b", HealthCheck.Status.HEALTHY),
        HealthCheck.Name.CURRENT_USER to HealthCheck(HealthCheck.Name.CURRENT_USER, "d", HealthCheck.Status.UNHEALTHY)
      )
    )

    integrationHealthResult.hasError() shouldBe true
  }

  it("should not have error if no checks are in error") {
    val integrationHealthResult = IntegrationHealthResult(
      checks = mapOf(
        HealthCheck.Name.TOKEN to HealthCheck(HealthCheck.Name.TOKEN, "a", HealthCheck.Status.HEALTHY),
        HealthCheck.Name.CODE_SUGGESTIONS to HealthCheck(HealthCheck.Name.CODE_SUGGESTIONS, "b", HealthCheck.Status.DEGRADED),
        HealthCheck.Name.CURRENT_USER to HealthCheck(HealthCheck.Name.CURRENT_USER, "d", HealthCheck.Status.HEALTHY)
      )
    )

    integrationHealthResult.hasError() shouldBe false
  }

  it("should get health check by name") {
    val integrationHealthResult = IntegrationHealthResult(
      checks = mapOf(
        HealthCheck.Name.TOKEN to HealthCheck(HealthCheck.Name.TOKEN, "a", HealthCheck.Status.HEALTHY),
        HealthCheck.Name.CODE_SUGGESTIONS to HealthCheck(HealthCheck.Name.CODE_SUGGESTIONS, "b", HealthCheck.Status.DEGRADED),
        HealthCheck.Name.CURRENT_USER to HealthCheck(HealthCheck.Name.CURRENT_USER, "d", HealthCheck.Status.HEALTHY)
      )
    )

    integrationHealthResult[HealthCheck.Name.TOKEN].name shouldBe HealthCheck.Name.TOKEN
    integrationHealthResult[HealthCheck.Name.TOKEN].message shouldBe "a"
    integrationHealthResult[HealthCheck.Name.TOKEN].status shouldBe HealthCheck.Status.HEALTHY
  }

  it("should throw if requested health check is missing") {
    val integrationHealthResult = IntegrationHealthResult(
      checks = mapOf(
        HealthCheck.Name.TOKEN to HealthCheck(HealthCheck.Name.TOKEN, "a", HealthCheck.Status.HEALTHY),
        HealthCheck.Name.CODE_SUGGESTIONS to HealthCheck(HealthCheck.Name.CODE_SUGGESTIONS, "b", HealthCheck.Status.DEGRADED),
        HealthCheck.Name.CURRENT_USER to HealthCheck(HealthCheck.Name.CURRENT_USER, "d", HealthCheck.Status.HEALTHY)
      )
    )

    shouldThrow<IllegalStateException> { integrationHealthResult[HealthCheck.Name.DUO_CHAT] }
  }
})
