package com.gitlab.plugin.services.health

import com.gitlab.plugin.services.GitLabServerService
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.mockk
import java.lang.module.ModuleDescriptor.Version

class CheckGitLabServerVersionHealthServiceTest : DescribeSpec({
  val url = "https://gitlab.com"
  val workspaceSettings = WorkspaceSettingsBuilder { url(url) }
  val request = HealthCheckRequest(workspaceSettings)

  val serverService = mockk<GitLabServerService>()
  val serverVersionHealthService = CheckGitLabServerVersionHealthService(serverService)

  afterEach {
    clearAllMocks()
  }

  it("should be unable to validate version if no server is returned") {
    coEvery { serverService.get(workspaceSettings) } returns null

    val check = serverVersionHealthService.check(request)[0]

    check.name shouldBe HealthCheck.Name.SERVER_VERSION
    check.message shouldBe "Unable to determine the GitLab version of https://gitlab.com."
    check.status shouldBe HealthCheck.Status.DEGRADED
  }

  it("should be unhealthy if the server version is bellow minimum supported version") {
    coEvery { serverService.get(workspaceSettings) } returns GitLabServerService.GitLabServer(Version.parse("16.7.1"))

    val check = serverVersionHealthService.check(request)[0]

    check.name shouldBe HealthCheck.Name.SERVER_VERSION
    check.message shouldBe "GitLab 16.7.1 is not supported. Upgrade to GitLab 16.8 or later."
    check.status shouldBe HealthCheck.Status.UNHEALTHY
  }

  it("should be healthy if the server version is supported") {
    coEvery { serverService.get(workspaceSettings) } returns GitLabServerService.GitLabServer(Version.parse("16.8"))

    val check = serverVersionHealthService.check(request)[0]

    check.name shouldBe HealthCheck.Name.SERVER_VERSION
    check.message shouldBe "GitLab 16.8 is supported."
    check.status shouldBe HealthCheck.Status.HEALTHY
  }
})
