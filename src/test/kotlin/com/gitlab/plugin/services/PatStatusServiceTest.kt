package com.gitlab.plugin.services

import com.gitlab.plugin.api.GitLabOfflineException
import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.api.pat.PatApi
import com.gitlab.plugin.api.pat.PatInfo
import com.gitlab.plugin.services.pat.PatStatus
import com.gitlab.plugin.util.GitLabUtil
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.mockk.*

class PatStatusServiceTest : DescribeSpec({
  val httpRequest = mockk<HttpRequest>(relaxed = true)
  val unauthorizedError = mockk<HttpResponse>(relaxed = true)

  val host = "https://my-gitlab.com"
  val token = "gl_token"

  val patApi = mockk<PatApi>()
  val patStatusService = PatStatusService(patApi)

  beforeEach {
    every { unauthorizedError.status } returns HttpStatusCode.Unauthorized
  }

  afterEach { clearAllMocks() }

  it("returns Accepted pat status if the request succeeds") {
    coEvery { patApi.patInfo(host, token) } returns patInfo

    val status = patStatusService.currentStatus(host, token)

    status shouldBe PatStatus.Accepted(patInfo)
  }

  it("returns Refused pat status if the request fails") {
    val cause = GitLabUnauthorizedException(unauthorizedError, "Error occurred.")
    coEvery { patApi.patInfo(host, token) } throws cause

    val status = patStatusService.currentStatus(host, token)

    status shouldBe PatStatus.Refused(host, 401, cause)
  }

  it("returns Unknown pat status if the server is offline") {
    val cause = GitLabOfflineException(httpRequest, cause = Exception())
    coEvery { patApi.patInfo(host, token) } throws cause

    val status = patStatusService.currentStatus(host, token)

    status shouldBe PatStatus.Unknown(host, cause)
  }
})

private val patInfo = PatInfo(
  id = 1,
  name = "name",
  revoked = false,
  createdAt = "2022-01-01T00:00:00Z",
  expiresAt = "2022-01-01T00:00:00Z",
  scopes = listOf(GitLabUtil.API_PAT_SCOPE),
  userId = 123,
  lastUsedAt = "2022-01-01T00:00:00Z",
  active = true
)
