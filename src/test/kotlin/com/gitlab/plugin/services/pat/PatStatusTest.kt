package com.gitlab.plugin.services.pat

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.pat.PatInfo
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe

class PatStatusTest : DescribeSpec({
  val host = "https://gitlab.com"

  describe("PatStatus.Active") {
    context("with active PAT that has desired scope") {
      val patStatus = PatStatus.Accepted(buildPatInfo(true, listOf("test")), "test")

      it("is valid") {
        patStatus.isValid shouldBe true
      }

      it("returns valid message") {
        patStatus.message() shouldBe GitLabBundle.message("pat.accepted.valid")
      }
    }

    context("with inactive PAT") {
      val patStatus = PatStatus.Accepted(buildPatInfo(false, listOf("test")), "test")

      it("is not valid") {
        patStatus.isValid shouldBe false
      }

      it("returns not active message") {
        patStatus.message() shouldBe GitLabBundle.message("pat.accepted.not-active")
      }
    }

    context("without desired scope") {
      val patStatus = PatStatus.Accepted(buildPatInfo(true, listOf("other")), "test")

      it("is not valid") {
        patStatus.isValid shouldBe false
      }

      it("returns not missing scope message") {
        patStatus.message() shouldBe GitLabBundle.message("pat.accepted.missing-scope", "test")
      }
    }
  }

  describe("PatStatus.Refused") {
    val patStatus = PatStatus.Refused(host, 401, Exception())

    it("should not be valid") {
      patStatus.isValid shouldBe false
    }

    it("should return refused message") {
      patStatus.message() shouldBe GitLabBundle.message("pat.refused.message", host, 401)
    }
  }

  describe("PatStatus.Unknown") {
    val patStatus = PatStatus.Unknown(host, Exception())

    it("should not be valid") {
      patStatus.isValid shouldBe false
    }

    it("should return unknown status message") {
      patStatus.message() shouldBe GitLabBundle.message("pat.unknown.message", host)
    }
  }
})

private fun buildPatInfo(active: Boolean, scopes: List<String>) = PatInfo(
  id = 1,
  name = "name",
  revoked = false,
  createdAt = "createdAt",
  scopes = scopes,
  userId = 1,
  lastUsedAt = "lastUsedAt",
  active = active,
  expiresAt = "expiresAt"
)
