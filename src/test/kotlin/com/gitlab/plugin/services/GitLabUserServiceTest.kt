package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.PatProvider
import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.services.pat.PatStatus
import com.intellij.openapi.application.ApplicationManager
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*

class GitLabUserServiceTest : DescribeSpec({
  val url = "https://gitlab.com"
  val token = "gl_pat"

  val patProvider = mockk<PatProvider>()

  val patStatusService = mockk<PatStatusService>()
  val userService = GitLabUserService(patStatusService)

  beforeEach {
    mockDuoContextServicePersistentSettings()
    every { DuoPersistentSettings.getInstance().url } returns url

    mockApplication()
    every { patProvider.token() } returns token
    every { ApplicationManager.getApplication().getService(PatProvider::class.java) } returns patProvider
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("user id should initially be null") {
    userService.getCurrentUserId() shouldBe null
  }

  it("refreshes user id") {
    coEvery { patStatusService.currentStatus(url, token) } returns patForUser(1)
    userService.invalidateAndFetchCurrentUser()
    userService.getCurrentUserId() shouldBe 1

    coEvery { patStatusService.currentStatus(url, token) } returns patForUser(2)
    userService.invalidateAndFetchCurrentUser()
    userService.getCurrentUserId() shouldBe 2
  }
})

private fun patForUser(id: Int): PatStatus.Accepted {
  return mockk {
    every { userId } returns id
    every { isValid } returns true
  }
}
