package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.requests.Metadata
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import java.lang.module.ModuleDescriptor.Version
import kotlin.test.assertEquals

class GitLabServerServiceTest : DescribeSpec({
  val duoApi: DuoApi = mockk<DuoApi>()

  afterEach {
    clearAllMocks()
  }

  describe("get current server data") {
    beforeEach {
      coEvery { duoApi.metadata() } returns Metadata.Response(version = "1.0")
    }

    it("fetches new data, when server data is stale") {
      val gitLabServerService = GitLabServerService(duoApi)
      val metadata = gitLabServerService.get()

      Version.parse("1.0") shouldBe metadata.version
    }

    it("returns existing data when server data is fresh") {
      val gitLabServerService = GitLabServerService(duoApi)
      val metadata = gitLabServerService.get()

      coEvery { duoApi.metadata() } returns Metadata.Response(version = "1.1")

      gitLabServerService.get()

      coVerify(exactly = 1) { duoApi.metadata() }
      assertEquals(Version.parse("1.0"), metadata.version)
    }
  }

  describe("get server version based on settings") {
    val settings = WorkspaceSettingsBuilder {}
    val gitLabServerService = GitLabServerService(duoApi)

    it("returns server version if the server responds with information") {
      coEvery { duoApi.metadata(settings) } returns Metadata.Response(version = "1.0")

      val result = gitLabServerService.get(settings)

      result?.version shouldBe Version.parse("1.0")
    }

    it("return null if the server does not respond") {
      coEvery { duoApi.metadata(settings) } returns null

      val result = gitLabServerService.get(settings)

      result shouldBe null
    }
  }
})
