package com.gitlab.plugin.settings

import com.gitlab.plugin.services.health.HealthCheck
import com.gitlab.plugin.services.health.HealthCheckRequest
import com.gitlab.plugin.services.health.IntegrationHealthResult
import com.gitlab.plugin.services.health.IntegrationHealthService
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import com.intellij.icons.AllIcons
import com.intellij.ui.components.BrowserLink
import com.intellij.ui.dsl.builder.Cell
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import javax.swing.JLabel

class SettingsHealthCheckerTest : DescribeSpec({
  val healthyTokenCheck = HealthCheck(
    name = HealthCheck.Name.TOKEN,
    message = "healthy",
    status = HealthCheck.Status.HEALTHY
  )

  val healthyServerVersionCheck = HealthCheck(
    name = HealthCheck.Name.SERVER_VERSION,
    message = "healthy",
    status = HealthCheck.Status.HEALTHY
  )

  val healthyUserCheck = HealthCheck(
    name = HealthCheck.Name.CURRENT_USER,
    message = "healthy",
    status = HealthCheck.Status.HEALTHY
  )

  val unhealthyUserCheck = HealthCheck(
    name = HealthCheck.Name.CURRENT_USER,
    message = "unhealthy",
    status = HealthCheck.Status.UNHEALTHY
  )

  val healthyCodeSuggestionsCheck = HealthCheck(
    name = HealthCheck.Name.CODE_SUGGESTIONS,
    message = "healthy",
    status = HealthCheck.Status.HEALTHY
  )

  val degradedCodeSuggestionCheck = HealthCheck(
    name = HealthCheck.Name.CODE_SUGGESTIONS,
    message = "degraded",
    status = HealthCheck.Status.DEGRADED
  )

  val healthyDuoChatCheck = HealthCheck(
    name = HealthCheck.Name.DUO_CHAT,
    message = "healthy",
    status = HealthCheck.Status.HEALTHY
  )

  val workspaceSettings = HealthCheckRequest(WorkspaceSettingsBuilder {}, project = null)

  val errorReportingInstructions: Cell<BrowserLink> = mockk(relaxed = true)
  val tokenHealth: JLabel = mockk(relaxed = true)
  val serverVersionHealth: JLabel = mockk(relaxed = true)
  val currentUserHealth: JLabel = mockk(relaxed = true)
  val codeSuggestionHealth: JLabel = mockk(relaxed = true)
  val duoChatHealth: JLabel = mockk(relaxed = true)
  val verifyLabel: Cell<JLabel> = mockk(relaxed = true)

  val integrationHealthResult = mockk<IntegrationHealthResult>(relaxed = true)
  val integrationHealthService = mockk<IntegrationHealthService>(relaxUnitFun = true)

  val settingsHealthChecker = SettingsHealthChecker(
    errorReportingInstructions,
    tokenHealth,
    serverVersionHealth,
    currentUserHealth,
    codeSuggestionHealth,
    duoChatHealth,
    verifyLabel,
    integrationHealthService
  )

  beforeEach {
    every { integrationHealthResult[HealthCheck.Name.TOKEN] } returns healthyTokenCheck
    every { integrationHealthResult[HealthCheck.Name.SERVER_VERSION] } returns healthyServerVersionCheck
    every { integrationHealthResult[HealthCheck.Name.CURRENT_USER] } returns healthyUserCheck
    every { integrationHealthResult[HealthCheck.Name.CODE_SUGGESTIONS] } returns healthyCodeSuggestionsCheck
    every { integrationHealthResult[HealthCheck.Name.DUO_CHAT] } returns healthyDuoChatCheck

    every { integrationHealthService.check(workspaceSettings) } returns integrationHealthResult
  }

  afterEach {
    clearAllMocks()
  }

  it("should make error reporting instructions visible if is not healthy") {
    every { integrationHealthResult.hasError() } returns true

    settingsHealthChecker.check(workspaceSettings)

    verify { errorReportingInstructions.visible(true) }
  }

  it("should make hide error reporting instructions if is healthy") {
    every { integrationHealthResult.hasError() } returns false

    settingsHealthChecker.check(workspaceSettings)

    verify { errorReportingInstructions.visible(false) }
  }

  it("should hide verify label after authentication health check") {
    settingsHealthChecker.check(workspaceSettings)

    verify {
      integrationHealthService.check(workspaceSettings)
      verifyLabel.visible(false)
    }
  }

  it("should update labels with authentication check result") {
    every { integrationHealthResult[HealthCheck.Name.TOKEN] } returns healthyTokenCheck
    every { integrationHealthResult[HealthCheck.Name.SERVER_VERSION] } returns healthyServerVersionCheck
    every { integrationHealthResult[HealthCheck.Name.CURRENT_USER] } returns unhealthyUserCheck
    every { integrationHealthResult[HealthCheck.Name.CODE_SUGGESTIONS] } returns degradedCodeSuggestionCheck
    every { integrationHealthResult[HealthCheck.Name.DUO_CHAT] } returns healthyDuoChatCheck

    settingsHealthChecker.check(workspaceSettings)

    verify {
      tokenHealth.text = healthyTokenCheck.message
      tokenHealth.icon = AllIcons.RunConfigurations.TestPassed

      serverVersionHealth.text = healthyServerVersionCheck.message
      serverVersionHealth.icon = AllIcons.RunConfigurations.TestPassed

      currentUserHealth.text = unhealthyUserCheck.message
      currentUserHealth.icon = AllIcons.General.Error

      codeSuggestionHealth.text = degradedCodeSuggestionCheck.message
      codeSuggestionHealth.icon = AllIcons.General.Warning

      duoChatHealth.text = healthyDuoChatCheck.message
      duoChatHealth.icon = AllIcons.RunConfigurations.TestPassed
    }
  }
})
