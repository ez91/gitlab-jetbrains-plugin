package com.gitlab.plugin.codesuggestions.rules

import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.editor.Document
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import io.mockk.*

class CursorOutOfBoundsTest : DescribeSpec({
  val request: InlineCompletionRequest = mockk()
  val document: Document = mockk()
  val logger: Logger = mockk()
  val cursorOutOfBounds = CursorOutOfBounds(logger)
  val documentLength = 10

  beforeEach {
    every { request.document } returns document
    every { document.textLength } returns documentLength
    every { document.charsSequence } returns "0123456789"
    every { logger.info(any<String>()) } just runs
  }

  afterEach {
    clearMocks(document, request, logger)
  }

  context("when cursor is within the document") {
    beforeEach {
      every { request.endOffset } returns documentLength / 2
    }

    it("returns false") {
      cursorOutOfBounds.shouldSkipSuggestion(request) shouldBe false
    }

    it("does not log a message") {
      cursorOutOfBounds.shouldSkipSuggestion(request)

      verify(exactly = 0) { logger.info(any<String>()) }
    }
  }

  context("when cursor position is negative") {
    beforeEach {
      every { request.endOffset } returns -1
    }

    it("returns true") {
      cursorOutOfBounds.shouldSkipSuggestion(request) shouldBe true
    }

    it("logs a message") {
      val logMessageSlot = slot<String>()
      every { logger.info(capture(logMessageSlot)) } just runs

      cursorOutOfBounds.shouldSkipSuggestion(request)

      verify(exactly = 1) { logger.info(any<String>()) }
      logMessageSlot.captured shouldContain "cursor is out of bounds: cursor position (-1) is negative"
    }
  }

  context("when cursor position is greater than the size of the document") {
    beforeEach {
      every { request.endOffset } returns documentLength + 1
    }

    it("returns true") {
      cursorOutOfBounds.shouldSkipSuggestion(request) shouldBe true
    }

    it("logs a message") {
      val logMessageSlot = slot<String>()
      every { logger.info(capture(logMessageSlot)) } just runs

      cursorOutOfBounds.shouldSkipSuggestion(request)

      verify(exactly = 1) { logger.info(any<String>()) }
      logMessageSlot.captured shouldContain "cursor is out of bounds: cursor position (11) is greater than the size of the document (10)"
    }
  }
})
