package com.gitlab.plugin.codesuggestions.telemetry

import com.gitlab.plugin.codesuggestions.gutter.GutterIconInlineCompletionEventListener
import com.intellij.codeInsight.inline.completion.InlineCompletion
import com.intellij.codeInsight.inline.completion.InlineCompletionHandler
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.event.EditorFactoryEvent
import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldNotBeSameInstanceAs
import io.mockk.*

class EditorListenerTest : DescribeSpec({
  val editorListener = EditorListener()
  val event: EditorFactoryEvent = mockk()
  val editor: Editor = mockk()

  mockkObject(InlineCompletion)

  beforeEach {
    every { event.editor } returns editor
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("editorCreated") {
    context("when an InlineCompletionHandler exists for the editor") {
      val inlineCompletionHandler: InlineCompletionHandler = mockk(relaxUnitFun = true)

      beforeEach {
        every { InlineCompletion.getHandlerOrNull(editor) } returns inlineCompletionHandler
      }

      it("registers the InlineCompletionEventListener") {
        editorListener.editorCreated(event)

        verify {
          inlineCompletionHandler.addEventListener(any<InlineCompletionEventListener>())
        }
      }

      it("register a new GutterIconInlineCompletionEventListener for each editor") {
        editorListener.editorCreated(event)
        verify { inlineCompletionHandler.addEventListener(any<GutterIconInlineCompletionEventListener>()) }

        editorListener.editorCreated(event)

        val listeners = mutableListOf<GutterIconInlineCompletionEventListener>()
        verify { inlineCompletionHandler.addEventListener(capture(listeners)) }
        listeners.size shouldBe 2
        listeners[0] shouldNotBeSameInstanceAs listeners[1]
      }
    }

    context("when no InlineCompletionHandler exists for the editor") {
      beforeEach {
        every { InlineCompletion.getHandlerOrNull(editor) } returns null
      }

      it("does nothing") {
        shouldNotThrowAny {
          editorListener.editorCreated(event)
        }
      }
    }
  }
})
