package com.gitlab.plugin.codesuggestions.telemetry

import com.gitlab.plugin.GitLabBundle
import com.intellij.openapi.application.ApplicationInfo
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.maps.shouldContainAll
import io.mockk.*

class IdeExtensionVersionContextTest : DescribeSpec({
  mockkStatic(ApplicationInfo::getInstance)
  mockkObject(GitLabBundle)

  beforeEach {
    every { ApplicationInfo.getInstance().build.asString() } returns "IU-232.9921.47"
    every { ApplicationInfo.getInstance().shortCompanyName } returns "JetBrains"

    every { GitLabBundle.plugin()?.name } returns "GitLab Duo"
    every { GitLabBundle.plugin()?.version } returns "0.4.1"
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  it("returns a self-describing JSON with the correct keys and values") {
    IdeExtensionVersionContext.build().let {
      it.map shouldContainAll mapOf(
        "schema" to IdeExtensionVersionContext.SCHEMA,
        "data" to mapOf(
          "ide_name" to "IntelliJ IDEA",
          "ide_version" to "IU-232.9921.47",
          "ide_vendor" to "JetBrains",
          "extension_name" to "GitLab Duo",
          "extension_version" to "0.4.1"
        )
      )
    }
  }
})
