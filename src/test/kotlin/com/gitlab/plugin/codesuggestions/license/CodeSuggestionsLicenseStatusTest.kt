package com.gitlab.plugin.codesuggestions.license

import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.graphql.CurrentUserQuery
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*

@Suppress("DEPRECATION")
class CodeSuggestionsLicenseStatusTest : DescribeSpec({
  val graphQLApi: GraphQLApi = mockk()
  val onLicenseChanged: () -> Unit = mockk()
  val currentUser: CurrentUserQuery.CurrentUser = mockk()
  lateinit var codeSuggestionsLicenseStatus: CodeSuggestionsLicenseStatus

  beforeEach {
    codeSuggestionsLicenseStatus = CodeSuggestionsLicenseStatus(graphQLApi, onLicenseChanged)
    coEvery { graphQLApi.getCurrentUser() } returns currentUser
    every { onLicenseChanged() } just runs
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("licensed") {
    beforeEach {
      every { currentUser.duoCodeSuggestionsAvailable } returns true
    }

    it("defaults to true") {
      codeSuggestionsLicenseStatus.isLicensed shouldBe true
    }
  }

  describe("validateLicense") {
    context("when current user has Duo Code Suggestion enabled") {
      beforeEach {
        codeSuggestionsLicenseStatus.isLicensed = false
        every { currentUser.duoCodeSuggestionsAvailable } returns true
      }

      it("sets licensed to true calls onLicenseChanged") {
        codeSuggestionsLicenseStatus.validateLicense()

        codeSuggestionsLicenseStatus.isLicensed shouldBe true
        verify(exactly = 1) { onLicenseChanged() }
      }
    }

    context("when current user has Duo Code Suggestion disabled") {
      beforeEach {
        codeSuggestionsLicenseStatus.isLicensed = true
        every { currentUser.duoCodeSuggestionsAvailable } returns false
      }

      it("sets licensed to false and calls onLicenseChanged") {
        codeSuggestionsLicenseStatus.validateLicense()

        codeSuggestionsLicenseStatus.isLicensed shouldBe false
        verify(exactly = 1) { onLicenseChanged() }
      }
    }

    context("when GitLab GraqhQL api returns null") {
      beforeEach {
        codeSuggestionsLicenseStatus.isLicensed = true
        coEvery { graphQLApi.getCurrentUser() } returns null
      }

      it("sets licensed to false and calls onLicenseChanged") {
        codeSuggestionsLicenseStatus.validateLicense()

        codeSuggestionsLicenseStatus.isLicensed shouldBe false
        verify(exactly = 1) { onLicenseChanged() }
      }
    }
  }
})
