package com.gitlab.plugin.integrationtest

import app.cash.turbine.test
import com.apollographql.apollo3.api.Optional
import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.graphql.ChatMutation
import com.gitlab.plugin.graphql.ChatSubscription
import com.gitlab.plugin.graphql.scalars.AiModelID
import com.gitlab.plugin.graphql.scalars.UserID
import com.gitlab.plugin.graphql.type.AiChatInput
import com.gitlab.plugin.services.DuoContextService
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler
import com.intellij.util.net.HttpConfigurable
import io.kotest.common.runBlocking
import io.kotest.core.annotation.EnabledIf
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldNotBeEmpty
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.string.shouldNotBeEmpty
import io.mockk.*
import java.util.UUID
import kotlin.test.assertNotNull
import kotlin.time.Duration.Companion.seconds

@EnabledIf(IntegrationTestEnvironment.Configured::class)
class GraphQLChatIT : DescribeSpec({
  fun buildGraphQLApi(
    env: IntegrationTestEnvironment = IntegrationTestEnvironment.forSpec(GraphQLChatIT::class),
    ignoreCertificateErrors: Boolean = false,
    exceptionHandler: CreateNotificationExceptionHandler = mockk<CreateNotificationExceptionHandler>()
  ): GraphQLApi {
    every { DuoContextService.instance.duoSettings.ignoreCertificateErrors } returns ignoreCertificateErrors

    every { DuoPersistentSettings.getInstance().url } returns env.gitlabHost

    every { exceptionHandler.handleException(any()) } returns Unit

    val apolloClientFactory = ApolloClientFactory(env.personalAccessTokenProvider)
    return GraphQLApi(apolloClientFactory.create(), exceptionHandler)
  }

  beforeSpec {
    mockDuoContextServicePersistentSettings()
  }

  beforeEach {
    mockkStatic(HttpConfigurable::class)
    every { HttpConfigurable.getInstance() } returns mockk<HttpConfigurable>()
  }

  afterSpec {
    unmockkAll()
  }

  describe("[behind proxy]") {
    val proxiedGitLab = IntegrationTestEnvironment(
      personalAccessToken = System.getenv("TEST_ACCESS_TOKEN"),
      gitlabHost = System.getenv("TEST_GITLAB_PROXY_HOST") ?: "https://localhost:8443",
      kclass = GraphQLChatIT::class,
    )

    beforeEach {
      mockDuoContextServicePersistentSettings()
    }

    afterEach {
      clearAllMocks()
    }

    afterSpec {
      unmockkAll()
    }

    it("returns the current user if ignoring certificate errors").config(enabledIf = proxiedGitLab.running()) {
      val subject = buildGraphQLApi(env = proxiedGitLab, ignoreCertificateErrors = true)

      runBlocking {
        assertNotNull(subject.getCurrentUser())
      }
    }

    it("throws an error that is handled by the exceptionHandler when not ignoring certificate errors")
      .config(enabledIf = proxiedGitLab.running()) {
        val exceptionHandler = mockk<CreateNotificationExceptionHandler>()
        val subject = buildGraphQLApi(env = proxiedGitLab, ignoreCertificateErrors = false, exceptionHandler)

        runBlocking {
          val actual = subject.getCurrentUser()
          actual.shouldBeNull()
          verify(exactly = 1) { exceptionHandler.handleException(any()) }
        }
      }
  }

  describe("aiCompletionResponses") {
    lateinit var graphqlApi: GraphQLApi

    beforeEach {
      graphqlApi = buildGraphQLApi(ignoreCertificateErrors = false)
    }

    it("receives AI messages from the subscription") {
      val currentUser = runBlocking { assertNotNull(graphqlApi.getCurrentUser(), "no current user found") }
      val sid = UUID.randomUUID().toString()
      val sendChatResponse = runBlocking {
        val mutation = ChatMutation(
          chatInput = AiChatInput(
            content = "Hi Duo from local testing. What can I ask?",
            resourceId = Optional.present(AiModelID(currentUser.id)),
          ),
          clientSubscriptionId = sid,
        )
        graphqlApi.apolloClient.mutation(mutation)
          .execute()
          .dataAssertNoErrors
          .action
      }
      val subscriptionFlow = graphqlApi.chatSubscription(sid, UserID(currentUser.id))
      sendChatResponse.shouldNotBeNull()
      sendChatResponse.errors.shouldBeEmpty()

      val aiCompletionResponses = mutableListOf<ChatSubscription.AiCompletionResponse>()
      subscriptionFlow?.test(60.seconds) {
        // First item seems to always be null
        skipItems(1)

        while (true) {
          val response = awaitItem().dataAssertNoErrors.aiCompletionResponse
          response.shouldNotBeNull()
          response.content.shouldNotBeEmpty()

          aiCompletionResponses.add(response)

          // Final result should have chunkId: null so we know we're done here
          if (response.chunkId == null) break
        }

        // There may still be chunks coming out of order
        cancelAndIgnoreRemainingEvents()
      }

      aiCompletionResponses.shouldNotBeEmpty()
    }
  }
})
