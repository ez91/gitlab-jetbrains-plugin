package com.gitlab.plugin.integrationtest

import com.gitlab.plugin.api.configureTestClient
import io.kotest.core.annotation.EnabledCondition
import io.kotest.core.spec.Spec
import io.kotest.core.test.TestCase
import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.*
import java.net.ConnectException
import kotlin.reflect.KClass
import kotlin.test.assertNotNull

class IntegrationTestEnvironment(
  private val kclass: KClass<out Spec>,
  private val personalAccessToken: String? = System.getenv("TEST_ACCESS_TOKEN"),
  val gitlabHost: String = System.getenv("TEST_GITLAB_HOST") ?: "https://gitlab.com",
) {
  fun running(): (TestCase) -> Boolean = {
    Running().enabled(kclass)
  }

  val personalAccessTokenProvider = {
    assertNotNull(personalAccessToken, "personalAccessToken must not be null")
  }

  companion object {
    private val instances: MutableMap<KClass<out Spec>, IntegrationTestEnvironment> = mutableMapOf()

    fun forSpec(kclass: KClass<out Spec>, env: IntegrationTestEnvironment? = null): IntegrationTestEnvironment {
      if (env != null) {
        return env.also { instances[kclass] = it }
      }

      return instances.getOrPut(kclass) { IntegrationTestEnvironment(kclass = kclass) }
    }
  }

  open class Configured : EnabledCondition {
    override fun enabled(
      kclass: KClass<out Spec>
    ) = forSpec(kclass).let {
      it.gitlabHost.isNotEmpty() && it.personalAccessToken.orEmpty().isNotEmpty()
    }
  }

  class Running : Configured() {
    override fun enabled(kclass: KClass<out Spec>) = if (super.enabled(kclass)) {
      canRequestCurrentAccessToken(forSpec(kclass))
    } else {
      false
    }

    private fun canRequestCurrentAccessToken(env: IntegrationTestEnvironment): Boolean {
      val client = HttpClient(
        OkHttp.create {
          configureTestClient(ignoreCertificateErrors = true)
        }
      )

      return client.use {
        try {
          val response = runBlocking {
            it.request {
              method = HttpMethod.Get
              url("${env.gitlabHost}/api/v4/personal_access_tokens/self")
            }
          }

          response.status >= HttpStatusCode.OK
        } catch (ex: ConnectException) {
          System.err.println("Failed to validate personal access token: ${ex.message}")
          false
        }
      }
    }
  }
}
