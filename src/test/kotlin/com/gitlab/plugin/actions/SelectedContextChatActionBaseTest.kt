package com.gitlab.plugin.actions

import com.gitlab.plugin.actions.chat.SelectedContextChatActionBase
import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.chat.mockDuoChatToolWindowManager
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.actionSystem.Presentation
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Caret
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.SelectionModel
import com.intellij.openapi.wm.ToolWindow
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class SelectedContextChatActionBaseTest : DescribeSpec({
  val duoChatToolWindow: ToolWindow = mockk<ToolWindow>()

  describe("SelectedContextChatActionBase") {
    beforeEach {
      mockDuoContextServicePersistentSettings()
      every { DuoPersistentSettings.getInstance().duoChatEnabled } returns true
    }

    afterEach { clearAllMocks() }
    afterSpec { unmockkAll() }

    describe("actionPerformed") {
      val mockEditor = mockk<Editor>(relaxed = true)
      val mockSelectionModel = mockk<SelectionModel>(relaxed = true)
      val mockChatService = mockk<ChatService>(relaxed = true)
      val mockActionEvent = mockk<AnActionEvent>(relaxed = true)

      beforeEach {
        mockDuoChatToolWindowManager(duoChatToolWindow, mockActionEvent.project)
        every { mockEditor.selectionModel } returns mockSelectionModel
        every { mockActionEvent.getData(CommonDataKeys.EDITOR) } returns mockEditor
        every { mockActionEvent.project?.service<ChatService>() } returns mockChatService
      }

      describe("with no selection") {
        beforeEach {
          every { mockSelectionModel.hasSelection() } returns false
        }

        it("does not process prompt").config(blockingTest = true) {
          val content = "content"
          val type = ChatRecord.Type.GENERAL

          val action = SelectedContextChatActionBase(content, type)
          action.actionPerformed(mockActionEvent)

          coVerify(exactly = 0) { mockChatService.processNewUserPrompt(any()) }
          verify(exactly = 1) { duoChatToolWindow.activate(null) }
        }
      }

      describe("with selection") {
        beforeEach {
          every { mockSelectionModel.hasSelection() } returns true
        }

        it("does process prompt") {
          val content = "content"
          val type = ChatRecord.Type.GENERAL

          val action = SelectedContextChatActionBase(content, type)
          action.actionPerformed(mockActionEvent)

          coVerify(exactly = 1) { mockChatService.processNewUserPrompt(any()) }
          verify(exactly = 1) { duoChatToolWindow.activate(null) }
        }
      }
    }

    describe("update") {
      val mockCaret = mockk<Caret>(relaxed = true)
      val mockActionEvent = mockk<AnActionEvent>(relaxed = true)
      val mockPresentation = mockk<Presentation>(relaxUnitFun = true)

      beforeEach {
        every { mockActionEvent.getData(CommonDataKeys.CARET) } returns mockCaret
        every { mockActionEvent.presentation } returns mockPresentation
      }

      describe("with selection") {
        beforeEach {
          every { mockCaret.hasSelection() } returns true
        }

        it("sets enabled to true") {
          val action = SelectedContextChatActionBase("content", ChatRecord.Type.GENERAL)
          action.update(mockActionEvent)

          verify(exactly = 1) { mockPresentation.setEnabled(true) }
        }
      }

      describe("with no selection") {
        beforeEach {
          every { mockCaret.hasSelection() } returns false
        }

        it("sets enabled to false") {
          val action = SelectedContextChatActionBase("content", ChatRecord.Type.GENERAL)
          action.update(mockActionEvent)

          verify(exactly = 1) { mockPresentation.setEnabled(false) }
        }
      }

      it("sets visible to true if duo features are enabled") {
        every { DuoPersistentSettings.getInstance().duoChatEnabled } returns true
        val action = SelectedContextChatActionBase("content", ChatRecord.Type.GENERAL)

        action.update(mockActionEvent)

        verify { mockPresentation.setVisible(true) }
      }

      it("sets visible to false if duo features are disabled") {
        every { DuoPersistentSettings.getInstance().duoChatEnabled } returns false
        val action = SelectedContextChatActionBase("content", ChatRecord.Type.GENERAL)

        action.update(mockActionEvent)

        verify { mockPresentation.setVisible(false) }
      }
    }
  }
})
