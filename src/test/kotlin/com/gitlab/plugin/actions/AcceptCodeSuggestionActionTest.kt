package com.gitlab.plugin.actions

import com.intellij.codeInsight.inline.completion.session.InlineCompletionContext
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.actionSystem.IdeActions
import com.intellij.openapi.editor.Editor
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class AcceptCodeSuggestionActionTest : DescribeSpec({
  mockkStatic(InlineCompletionContext::getOrNull)

  val acceptCodeSuggestionAction = AcceptCodeSuggestionAction()
  val event = mockk<AnActionEvent>(relaxed = true)

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("should perform insert inline completion action") {
    val insertInlineCompletionAction = mockk<AnAction>(relaxUnitFun = true)
    every { event.actionManager.getAction(IdeActions.ACTION_INSERT_INLINE_COMPLETION) } returns insertInlineCompletionAction

    acceptCodeSuggestionAction.actionPerformed(event)

    verify { insertInlineCompletionAction.actionPerformed(event) }
  }

  it("should disable and hide action if editor is not present in event data") {
    every { event.getData(CommonDataKeys.EDITOR) } returns null

    acceptCodeSuggestionAction.update(event)

    verify { event.presentation.setVisible(false) }
    verify { event.presentation.setEnabled(false) }
  }

  it("should disable and hide action if editor is not displaying inline completion") {
    val editor = mockk<Editor>()
    every { event.getData(CommonDataKeys.EDITOR) } returns editor
    every { InlineCompletionContext.getOrNull(editor) } returns mockk<InlineCompletionContext> {
      every { isCurrentlyDisplaying() } returns false
    }

    acceptCodeSuggestionAction.update(event)

    verify { event.presentation.setVisible(false) }
    verify { event.presentation.setEnabled(false) }
  }

  it("should enable and display action if editor is displaying inline completion") {
    val editor = mockk<Editor>()
    every { event.getData(CommonDataKeys.EDITOR) } returns editor
    every { InlineCompletionContext.getOrNull(editor) } returns mockk<InlineCompletionContext> {
      every { isCurrentlyDisplaying() } returns true
    }

    acceptCodeSuggestionAction.update(event)

    verify { event.presentation.setVisible(true) }
    verify { event.presentation.setEnabled(true) }
  }
})
