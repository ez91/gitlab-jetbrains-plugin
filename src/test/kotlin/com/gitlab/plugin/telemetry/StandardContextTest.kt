package com.gitlab.plugin.telemetry

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.snowplowanalytics.snowplow.tracker.payload.SelfDescribingJson
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.maps.shouldContainAll
import io.kotest.matchers.shouldBe
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockkObject
import io.mockk.unmockkAll

class StandardContextTest : DescribeSpec({
  mockkObject(GitLabBundle, DuoPersistentSettings)

  beforeEach {
    every { GitLabBundle.plugin()?.name } returns "test plugin name"
    every { DuoPersistentSettings.getInstance().url } returns "http://instance.url"
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("build") {
    context("when extra is not provided") {
      it("returns a self-describing JSON with source and environment") {
        val selfDescribingJson = StandardContext.build()
        selfDescribingJson.map shouldContainAll mapOf(
          "schema" to "iglu:com.gitlab/gitlab_standard/jsonschema/1-0-10",
          "data" to mapOf(
            "source" to "test plugin name",
            "environment" to "self-managed",
            "extra" to null
          )
        )
      }
    }

    context("when extra is provided") {
      val extraMap = mapOf("a" to "b", "c" to "d")

      it("returns a self-describing JSON with source and environment") {
        val selfDescribingJson = StandardContext.build(extraMap)
        selfDescribingJson.map shouldContainAll mapOf(
          "schema" to "iglu:com.gitlab/gitlab_standard/jsonschema/1-0-10",
          "data" to mapOf(
            "source" to "test plugin name",
            "environment" to "self-managed",
            "extra" to mapOf("a" to "b", "c" to "d")
          )
        )
      }
    }

    describe("environment") {
      @Suppress("UNCHECKED_CAST")
      fun SelfDescribingJson.data() = map["data"] as Map<String, String>

      context("when host is gitlab.com") {
        beforeEach {
          every { DuoPersistentSettings.getInstance().url } returns "https://gitlab.com"
        }

        it("sets environment to production") {
          StandardContext.build().data()["environment"] shouldBe "production"
        }
      }

      context("when host is https://staging.gitlab.com") {
        beforeEach {
          every { DuoPersistentSettings.getInstance().url } returns "https://staging.gitlab.com"
        }

        it("sets environment to production") {
          StandardContext.build().data()["environment"] shouldBe "staging"
        }
      }

      context("when host is https://dev.gitlab.org") {
        beforeEach {
          every { DuoPersistentSettings.getInstance().url } returns "https://dev.gitlab.org"
        }

        it("sets environment to production") {
          StandardContext.build().data()["environment"] shouldBe "org"
        }
      }

      context("when host contains http://localhost") {
        beforeEach {
          every { DuoPersistentSettings.getInstance().url } returns "http://localhost"
        }

        it("sets environment to production") {
          StandardContext.build().data()["environment"] shouldBe "development"
        }
      }

      context("when host is none of the pre-defined host URLs") {
        beforeEach {
          every { DuoPersistentSettings.getInstance().url } returns "https://gitlab.example.com"
        }

        it("sets environment to production") {
          StandardContext.build().data()["environment"] shouldBe "self-managed"
        }
      }
    }
  }
})
