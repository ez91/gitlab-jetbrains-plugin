package com.gitlab.plugin.e2eTest.steps

import com.gitlab.plugin.e2eTest.pages.idea
import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.stepsProcessing.step
import com.intellij.remoterobot.utils.keyboard
import java.awt.event.KeyEvent

class EditorSteps(private val remoteRobot: RemoteRobot) {

  fun addContentToEditor(content: String) = with(remoteRobot) {
    step("Add content to current editor") {
      idea {
        with(textEditor()) {
          editor.click()
          keyboard { enterText(content) }
        }
      }
    }
  }

  fun clearContent() = with(remoteRobot) {
    step("Clear content in editor") {
      idea {
        keyboard {
          selectAll()
          key(KeyEvent.VK_DELETE)
        }
      }
    }
  }
}
