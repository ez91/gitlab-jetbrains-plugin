package com.gitlab.plugin.e2eTest.tests

import com.automation.remarks.junit5.Video
import com.gitlab.plugin.e2eTest.pages.duoChatFrame
import com.gitlab.plugin.e2eTest.pages.idea
import com.gitlab.plugin.e2eTest.steps.ProjectSteps
import com.gitlab.plugin.e2eTest.utils.RemoteRobotExtension
import com.gitlab.plugin.e2eTest.utils.StepsLogger
import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.stepsProcessing.step
import com.intellij.remoterobot.utils.waitFor
import com.intellij.remoterobot.utils.waitForIgnoringError
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import java.time.Duration
import java.time.Duration.ofMinutes

@Suppress("ClassName")
@ExtendWith(RemoteRobotExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DuoChatTest {

  init {
    StepsLogger.init()
  }

  @BeforeAll
  fun waitForIde(remoteRobot: RemoteRobot) {
    waitForIgnoringError(ofMinutes(3)) { remoteRobot.callJs("true") }
    ProjectSteps(remoteRobot).createNewProject()
    remoteRobot.idea {
      step("Ensure GitLab Duo is enabled") {
        assert(isDuoEnabled())
      }
    }
  }

  @AfterAll
  fun closeProject(remoteRobot: RemoteRobot) = ProjectSteps(remoteRobot).closeProject()

  @Nested
  inner class `with valid token set` {

    @Test
    @Video
    fun `GitLab Duo Chat simple request`(remoteRobot: RemoteRobot) = with(remoteRobot) {
      idea {
        openDuoChat()

        duoChatFrame {
          sendChat("hi")
          val expectedResponse = "GitLab Duo Chat"

          // We check for "GitLab Duo Chat" here because it is the only part of that response that is deterministic
          // Agent name is defined in https://gitlab.com/gitlab-org/gitlab/-/blob/35aa4271a2b621be73e555af03ac21d51b4f80a2/ee/lib/gitlab/llm/chain/agents/zero_shot/executor.rb#L16
          waitFor(
            Duration.ofSeconds(60),
            errorMessage = "Expected '$expectedResponse' to be present in the response from Duo Chat."
          ) {
            getLastChatMessage().contains(expectedResponse)
          }
        }
      }
    }
  }
}
