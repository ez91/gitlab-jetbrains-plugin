package com.gitlab.plugin.e2eTest.suites

import com.gitlab.plugin.e2eTest.tests.AuthenticateTest
import com.gitlab.plugin.e2eTest.tests.CodeSuggestionTest
import com.gitlab.plugin.e2eTest.tests.DuoChatTest
import com.gitlab.plugin.e2eTest.tests.PluginDefaultsTest
import org.junit.platform.suite.api.SelectClasses
import org.junit.platform.suite.api.Suite

@Suite
@SelectClasses(
  PluginDefaultsTest::class,
  AuthenticateTest::class,
  CodeSuggestionTest::class,
  DuoChatTest::class,
)
class E2ETestSuite
