package com.gitlab.plugin.e2eTest.pages

import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.data.RemoteComponent
import com.intellij.remoterobot.fixtures.CommonContainerFixture
import com.intellij.remoterobot.fixtures.DefaultXpath
import com.intellij.remoterobot.fixtures.FixtureName
import com.intellij.remoterobot.fixtures.JCefBrowserFixture
import com.intellij.remoterobot.search.locators.byXpath
import com.intellij.remoterobot.stepsProcessing.step
import com.intellij.remoterobot.utils.keyboard
import com.intellij.remoterobot.utils.waitFor
import com.intellij.remoterobot.utils.waitForIgnoringError
import java.awt.event.KeyEvent
import java.time.Duration

fun RemoteRobot.duoChatFrame(function: DuoChatFrame.() -> Unit) {
  find<DuoChatFrame>(timeout = Duration.ofSeconds(120)).apply(function)
}

private const val DUO_CHAT_MESSAGES_XPATH = "//*[@id='chat-component']//*[contains(@class, 'duo-chat-message')]"
private const val DUO_CHAT_INPUT_XPATH = "//*[@id='chat-component']//*[contains(@class, 'duo-chat-input')]"

@FixtureName("Duo Chat Frame")
@DefaultXpath("type", "//div[@class='JBCefOsrComponent']")
class DuoChatFrame(remoteRobot: RemoteRobot, remoteComponent: RemoteComponent) :
  CommonContainerFixture(remoteRobot, remoteComponent) {

  fun sendChat(request: String) {
    step("Send Duo Chat request") {
      print("Sending Duo Chat request: \"$request\"")

      waitFor(errorMessage = "No Duo Chat input found.") {
        duoChatBrowser()!!.exist(DUO_CHAT_INPUT_XPATH)
      }

      keyboard { enterText(request) }
      keyboard { key(KeyEvent.VK_ENTER) }
    }
  }

  fun getLastChatMessage(): String {
    return duoChatBrowser()!!.findElements(DUO_CHAT_MESSAGES_XPATH).last().html
  }

  private fun duoChatBrowser(): JCefBrowserFixture? {
    waitForIgnoringError(Duration.ofSeconds(60), errorMessage = "No Duo Chat browser found.") {
      browsers().isNotEmpty() && browsers().any { it.getDom().contains("GitLab Duo Chat") }
    }

    return browsers().find { it.getDom().contains("GitLab Duo Chat") }
  }

  private fun browsers(): List<JCefBrowserFixture> {
    return remoteRobot.findAll<JCefBrowserFixture>(byXpath("//div[@class='JBCefOsrComponent']"))
  }
}
