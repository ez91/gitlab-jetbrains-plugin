package com.gitlab.plugin.util

import com.gitlab.plugin.api.duo.PatProvider
import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.services.ProjectContextService
import com.gitlab.plugin.workspace.DidChangeConfigurationParams
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.progress.Task
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class TokenUpdatedDidChangeConfigurationListenerTest : DescribeSpec({
  val userService = mockk<GitLabUserService>(relaxUnitFun = true)
  val patProvider = mockk<PatProvider>(relaxUnitFun = true)
  val listener = TokenUpdatedDidChangeConfigurationListener(userService, patProvider)

  beforeEach {
    mockkObject(ProjectContextService)

    mockkStatic(::gitlabStatusRefresh)
    every { gitlabStatusRefresh() } answers {}

    mockkObject(TokenUtil)
    every { TokenUtil.setToken(any()) } returns Unit

    mockProgressManager()
  }

  afterEach {
    clearAllMocks()
    unmockkAll()
  }

  it("updates TokenUtil token when keychain token is configured") {
    val settings = WorkspaceSettingsBuilder { token("keychain_token") }

    listener.onConfigurationChange(DidChangeConfigurationParams(settings))

    verify { TokenUtil.setToken("keychain_token") }
  }

  it("clears TokenUtil when other token type is configured") {
    val settings = WorkspaceSettingsBuilder { onePassword("keychain_token") }

    listener.onConfigurationChange(DidChangeConfigurationParams(settings))

    verify { TokenUtil.setToken("") }
  }

  it("updates patProvider cache when token is updated") {
    val settings = WorkspaceSettingsBuilder { onePassword("keychain_token") }

    listener.onConfigurationChange(DidChangeConfigurationParams(settings))

    coVerify { patProvider.cacheToken(settings.token) }
  }

  it("invalidates current GitLab user when token is updated") {
    val settings = WorkspaceSettingsBuilder { onePassword("keychain_token") }

    listener.onConfigurationChange(DidChangeConfigurationParams(settings))

    coVerify { userService.invalidateAndFetchCurrentUser(null) }
  }

  it("updates gitlab status when project is initialized") {
    every { ProjectContextService.isInitialized() } returns true
    val settings = WorkspaceSettingsBuilder {}

    listener.onConfigurationChange(DidChangeConfigurationParams(settings))

    verify { gitlabStatusRefresh() }
  }

  it("doesn't update gitlab status when project is not initialized") {
    every { ProjectContextService.isInitialized() } returns false
    val settings = WorkspaceSettingsBuilder {}

    listener.onConfigurationChange(DidChangeConfigurationParams(settings))

    verify(exactly = 0) { gitlabStatusRefresh() }
  }
})

/**
 * Mocks the singleton ProgressManager to runs tasks synchronously.
 */
private fun mockProgressManager() {
  mockkStatic(ProgressManager::getInstance)

  val progressManager = mockk<ProgressManager>()
  every { ProgressManager.getInstance() } returns progressManager

  val taskSlot = slot<Task>()
  every { progressManager.run(capture(taskSlot)) } answers {
    val progressIndicator = mockk<ProgressIndicator>(relaxed = true)
    taskSlot.captured.run(progressIndicator)
  }
}
