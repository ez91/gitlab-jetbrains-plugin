package com.gitlab.plugin.git

import com.gitlab.plugin.services.DuoEnabledProjectService
import com.intellij.openapi.project.Project
import git4idea.repo.GitRemote
import git4idea.repo.GitRepository
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify

class GitRepositoryRemoteChangedListenerTest : DescribeSpec({
  val anyRemotes = listOf<GitRemote>()

  val project = mockk<Project>()
  val duoEnabledProjectService = mockk<DuoEnabledProjectService>(relaxUnitFun = true)

  val listener = GitRepositoryRemoteChangedListener(project)

  beforeEach {
    every { project.getService(DuoEnabledProjectService::class.java) } returns duoEnabledProjectService
  }

  afterEach {
    clearAllMocks()
  }

  it("should invalidate and fetch duo enabled on first config update") {
    val repository = mockk<GitRepository> {
      every { remotes } returns anyRemotes
    }

    listener.notifyConfigChanged(repository)

    verify { duoEnabledProjectService.invalidateAndFetch() }
  }

  it("should invalidate and fetch duo enabled if remote urls have changed") {
    val repository = mockk<GitRepository> {
      every { remotes } returns listOf(remote("https://gitlab.com/test/test.git"))
    }
    listener.notifyConfigChanged(repository)

    val updatedRepository = mockk<GitRepository> {
      every { remotes } returns listOf(remote("https://gitlab.my-instance.com/test/test.git"))
    }
    listener.notifyConfigChanged(updatedRepository)

    verify(exactly = 2) { duoEnabledProjectService.invalidateAndFetch() }
  }

  it("should not invalidate and fetch duo enabled if remote urls have not changed") {
    val repository = mockk<GitRepository> {
      every { remotes } returns listOf(remote("https://gitlab.com/test/test.git"))
    }

    listener.notifyConfigChanged(repository)
    listener.notifyConfigChanged(repository)

    verify(exactly = 1) { duoEnabledProjectService.invalidateAndFetch() }
  }
})

private fun remote(url: String = "https://gitlab.com/test/test.git") = mockk<GitRemote> {
  every { firstUrl } returns url
}
