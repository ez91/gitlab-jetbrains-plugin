package com.gitlab.plugin.ui

import com.intellij.notification.NotificationType
import com.intellij.notification.Notifications
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class GitLabNotificationManagerTest : BasePlatformTestCase() {
  val receivedNotifications: MutableList<com.intellij.notification.Notification> = mutableListOf()

  @BeforeEach
  public override fun setUp() {
    super.setUp()

    project.messageBus.connect(testRootDisposable).subscribe(
      Notifications.TOPIC,
      object : Notifications {
        override fun notify(notification: com.intellij.notification.Notification) {
          receivedNotifications.add(notification)
        }
      }
    )
  }

  @Test
  fun sendNotification() {
    // Action
    val notification = Notification(
      "Title",
      "Message"
    )
    GitLabNotificationManager().sendNotification(
      notification,
      NotificationGroupType.GENERAL,
      NotificationType.INFORMATION,
      project
    )

    // Case
    val receivedNotification = receivedNotifications.last()
    assertEquals(receivedNotification.title, "Title")
    assertEquals(receivedNotification.content, "Message")
    assertEquals(receivedNotification.groupId, GENERAL_NOTIFICATION_GROUP_ID)
    assertEquals(receivedNotification.type, NotificationType.INFORMATION)
  }
}
