package com.gitlab.plugin.ui

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.services.GitLabServerService
import io.kotest.assertions.withClue
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.datatest.withData
import io.mockk.*
import java.lang.module.ModuleDescriptor

class CompletionStrategyTest : DescribeSpec({
  val duoApi: DuoApi = mockk()
  val serverService: GitLabServerService = mockk()

  describe("generateCompletions") {
    context("when instances is at least 16.8") {
      withData(
        listOf(
          "16.8",
          "16.8.0",
          "16.8.0-ee",
          "16.8.1",
          "16.8.1-ee",
          "16.9.0",
          "16.9.0-ee",
          "16.10.0-pre",
          "17.0.0",
          "17.0.0-ee"
        )
      ) { version ->
        coEvery { duoApi.completions(any()) } returns null
        every { serverService.get() } returns GitLabServerService.GitLabServer(ModuleDescriptor.Version.parse(version), 0)

        val payload = Completion.Payload(
          currentFile = Completion.Payload.CodeSuggestionRequestFile(contentAboveCursor = "content above")
        )

        CompletionStrategy(duoApi, serverService).generateCompletions(payload)

        withClue("calls API for completions on $version") {
          coVerify(exactly = 1) { duoApi.completions(any()) }
        }

        clearMocks(duoApi, serverService)
      }
    }

    context("when instances is below 16.8") {
      withData(
        listOf(
          "16.6",
          "16.6.0",
          "16.6.0-ee",
          "16.7.0",
          "16.7.0-ee",
          "16.7.1",
          "16.7.1-ee",
          "16.7.10",
          "16.7.10-ee",
        )
      ) { version ->
        coEvery { duoApi.completions(any()) } returns null
        every { serverService.get() } returns GitLabServerService.GitLabServer(ModuleDescriptor.Version.parse(version), 0)

        val payload = Completion.Payload(
          currentFile = Completion.Payload.CodeSuggestionRequestFile(contentAboveCursor = "content below")
        )

        CompletionStrategy(duoApi, serverService).generateCompletions(payload)

        withClue("does not call API for completions on $version") {
          coVerify(exactly = 0) { duoApi.completions(any()) }
        }

        clearMocks(duoApi, serverService)
      }
    }
  }
})
