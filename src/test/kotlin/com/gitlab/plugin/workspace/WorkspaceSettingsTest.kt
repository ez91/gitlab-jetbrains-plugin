package com.gitlab.plugin.workspace

import com.gitlab.plugin.api.mockDuoContextServicePersistentSettings
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.NotificationGroupType
import com.gitlab.plugin.util.TokenUtil
import com.intellij.execution.ExecutionException
import com.intellij.execution.configurations.GeneralCommandLine
import com.intellij.execution.process.ProcessOutput
import com.intellij.execution.util.ExecUtil
import com.intellij.notification.NotificationType
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.core.spec.style.scopes.DescribeSpecContainerScope
import io.kotest.matchers.shouldBe
import io.mockk.*

class WorkspaceSettingsTest : DescribeSpec({
  describe("TokenSettings") {
    describe("getToken") {
      beforeEach {
        mockkConstructor(GeneralCommandLine::class)
        mockkConstructor(GitLabNotificationManager::class)
        every {
          anyConstructed<GitLabNotificationManager>().sendNotification(
            any(),
            NotificationGroupType.IMPORTANT,
            NotificationType.ERROR
          )
        } returns Unit
        mockDuoContextServicePersistentSettings()
        mockkObject(TokenUtil)
      }

      afterEach {
        clearAllMocks()
      }

      afterSpec { unmockkAll() }

      describe("Keychain") {
        it("returns the previously persisted token") {
          val expected = "glpat-xxxxxxxx"
          val subject = TokenSettings.Keychain(expected)
          subject.getToken() shouldBe expected
        }
      }

      describe("OPSecretReference") {
        it("returns empty string for an invalid secret reference") {
          val subject = TokenSettings.OPSecretReference("xxx")
          subject.getToken() shouldBe ""
        }

        context("given a quoted secret reference") {
          `it behaves like a valid secret reference`(
            subject = TokenSettings.OPSecretReference("\"op://Private/GitLab Pretend Access Token/token\"")
          )
        }

        context("given an unquoted secret reference") {
          `it behaves like a valid secret reference`(
            subject = TokenSettings.OPSecretReference("op://Private/GitLab Pretend Access Token/token")
          )
        }
      }
    }
  }
})

suspend fun DescribeSpecContainerScope.`it behaves like a valid secret reference`(subject: TokenSettings.OPSecretReference) {
  context("given a zero exit status") {
    it("returns a Personal Access Token") {
      mockkStatic(ExecUtil::class) {
        every { ExecUtil.execAndGetOutput(any()) } returns mockk<ProcessOutput>().also {
          every { it.exitCode } returns 0
          every { it.stderrLines } returns emptyList()
          every { it.stdoutLines } returns listOf("glpat-yyyyyyyy")
        }

        subject.getToken() shouldBe "glpat-yyyyyyyy"
        verify(exactly = 0) { anyConstructed<GitLabNotificationManager>().sendNotification(any(), any(), any()) }
      }
    }
  }

  context("given a non-zero exit status") {
    it("returns an empty string") {
      mockkStatic(ExecUtil::class) {
        every { ExecUtil.execAndGetOutput(any()) } returns mockk<ProcessOutput>().also {
          every { it.exitCode } returns 1
          every { it.stderrLines } returns emptyList()
          every { it.stdoutLines } returns emptyList()
        }

        subject.getToken() shouldBe ""
        verify(exactly = 1) {
          anyConstructed<GitLabNotificationManager>().sendNotification(
            any(),
            NotificationGroupType.IMPORTANT,
            NotificationType.ERROR
          )
        }
      }
    }
  }

  context("given an execution exception") {
    it("catches execution exceptions") {
      mockkStatic(ExecUtil::class) {
        every { ExecUtil.execAndGetOutput(any()) } throws ExecutionException("Expected exception")

        subject.getToken() shouldBe ""
        verify(exactly = 1) {
          anyConstructed<GitLabNotificationManager>().sendNotification(
            any(),
            NotificationGroupType.IMPORTANT,
            NotificationType.ERROR
          )
        }
      }
    }
  }
}
