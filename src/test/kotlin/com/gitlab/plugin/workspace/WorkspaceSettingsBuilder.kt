package com.gitlab.plugin.workspace

import io.kotest.common.runBlocking

class WorkspaceSettingsBuilder(
  var url: String = "https://gitlab.com",
  var codeSuggestionEnabled: Boolean = true,
  var duoChatEnabled: Boolean = true,
  var token: TokenSettings = TokenSettings.Keychain("my-awesome-token")
) {
  companion object {
    operator fun invoke(body: WorkspaceSettingsBuilder.() -> Unit): WorkspaceSettings {
      return WorkspaceSettingsBuilder().apply(body).build()
    }

    fun coCreate(body: suspend WorkspaceSettingsBuilder.() -> Unit): WorkspaceSettings {
      return WorkspaceSettingsBuilder()
        .apply { runBlocking { body() } }
        .build()
    }
  }

  fun url(value: String) = apply {
    this.url = value
  }

  fun token(value: String) = apply {
    this.token = TokenSettings.Keychain(value)
  }

  fun onePassword(secret: String) = apply {
    this.token = TokenSettings.OPSecretReference(secret)
  }

  fun build(): WorkspaceSettings {
    return WorkspaceSettings(url, codeSuggestionEnabled, duoChatEnabled, token)
  }
}
