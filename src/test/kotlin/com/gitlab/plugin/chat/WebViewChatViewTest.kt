package com.gitlab.plugin.chat

import com.gitlab.plugin.chat.view.CefChatBrowser
import com.gitlab.plugin.chat.view.WebViewChatView
import com.gitlab.plugin.chat.view.model.ChatViewMessage
import com.gitlab.plugin.chat.view.model.NewPromptMessage
import com.gitlab.plugin.chat.view.model.NewRecordMessage
import com.gitlab.plugin.chat.view.model.UpdateRecordMessage
import com.intellij.openapi.wm.ToolWindowManager
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.string.shouldContain
import io.mockk.*

class WebViewChatViewTest : DescribeSpec({
  val toolWindowManager: ToolWindowManager = mockk()
  val chatBrowser: CefChatBrowser = mockk(relaxed = true)

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("init") {
    describe("adding local resources") {
      it("adds all asset files") {
        WebViewChatView(chatBrowser, toolWindowManager)

        verify(exactly = 5) { chatBrowser.addLocalResource(any(), any(), any()) }
      }

      it("uses the correct resource path prefix") {
        WebViewChatView(chatBrowser, toolWindowManager)

        verify { chatBrowser.addLocalResource(any(), WebViewChatView.RESOURCE_PATH_PREFIX, any()) }
      }
    }
  }

  describe("addRecord") {
    val chatRecord = buildChatRecord()

    it("posts a newRecord message to the browser") {
      val payloadSlot = slot<String>()
      every { chatBrowser.postMessage(capture(payloadSlot)) } just runs

      WebViewChatView(chatBrowser, toolWindowManager).addRecord(NewRecordMessage(chatRecord))

      verify(exactly = 1) { chatBrowser.postMessage(any()) }
      payloadSlot.captured shouldContain ChatViewMessage.NEW_RECORD
    }
  }

  describe("updateRecord") {
    val chatRecord = buildChatRecord()

    it("posts an updateRecord message to the browser") {
      val payloadSlot = slot<String>()
      every { chatBrowser.postMessage(capture(payloadSlot)) } just runs

      WebViewChatView(chatBrowser, toolWindowManager).updateRecord(UpdateRecordMessage(chatRecord))

      verify(exactly = 1) { chatBrowser.postMessage(any()) }
      payloadSlot.captured shouldContain ChatViewMessage.UPDATE_RECORD
    }
  }

  describe("onMessage") {
    it("registers a handler with ChatBrowser") {
      WebViewChatView(chatBrowser, toolWindowManager).onMessage(mockk())

      verify(exactly = 1) { chatBrowser.onPostMessageReceived(any()) }
    }

    it("calls the given block with the deserialized ChatViewMessage") {
      val onPostMessageReceivedSlot = slot<(String) -> Unit>()
      every { chatBrowser.onPostMessageReceived(capture(onPostMessageReceivedSlot)) } just runs

      val blockParam: (ChatViewMessage) -> Unit = mockk(relaxed = true)
      WebViewChatView(chatBrowser, toolWindowManager).onMessage(blockParam)

      NewPromptMessage("test content").let {
        onPostMessageReceivedSlot.captured(it.toJson())

        verify { blockParam(it) }
      }
    }
  }

  describe("clear") {
    it("sends a clear message to the browser") {
      val payloadSlot = slot<String>()
      every { chatBrowser.postMessage(capture(payloadSlot)) } just runs

      WebViewChatView(chatBrowser, toolWindowManager).clear()

      verify(exactly = 1) { chatBrowser.postMessage(any()) }
      payloadSlot.captured shouldContain ChatViewMessage.CLEAR
    }
  }
})
