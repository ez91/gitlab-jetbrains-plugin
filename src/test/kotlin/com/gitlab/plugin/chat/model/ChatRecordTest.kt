package com.gitlab.plugin.chat.model

import io.kotest.core.spec.style.DescribeSpec
import io.kotest.datatest.withData
import io.kotest.matchers.shouldBe

class ChatRecordTest : DescribeSpec({
  describe("ChatRecord.Type") {
    describe("fromContent") {
      describe("returns the type for a known command") {
        withData(
          listOf(
            Pair("/explain", ChatRecord.Type.EXPLAIN_CODE),
            Pair("/tests", ChatRecord.Type.GENERATE_TESTS),
            Pair("/refactor", ChatRecord.Type.REFACTOR_CODE),
            Pair("/clear", ChatRecord.Type.CLEAR_CHAT),
            Pair("/reset", ChatRecord.Type.NEW_CONVERSATION),
          )
        ) {
          ChatRecord.Type.fromContent(it.first) shouldBe it.second
        }
      }

      describe("recognizes commands that include whitespace") {
        withData(
          listOf(
            Pair("/explain ", ChatRecord.Type.EXPLAIN_CODE),
            Pair(" /explain ", ChatRecord.Type.EXPLAIN_CODE),
            Pair(" /explain", ChatRecord.Type.EXPLAIN_CODE),
          )
        ) {
          ChatRecord.Type.fromContent(it.first) shouldBe it.second
        }
      }

      it("returns general for an unknown command") {
        ChatRecord.Type.fromContent("/unknown") shouldBe ChatRecord.Type.GENERAL
      }
    }
  }
})
