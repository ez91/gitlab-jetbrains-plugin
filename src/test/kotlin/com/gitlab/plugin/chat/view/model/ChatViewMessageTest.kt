package com.gitlab.plugin.chat.view.model

import com.gitlab.plugin.chat.buildChatRecord
import com.gitlab.plugin.chat.buildLocalDateTime
import io.kotest.assertions.withClue
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.datatest.withData
import io.kotest.matchers.shouldBe
import java.util.*

class ChatViewMessageTest : DescribeSpec({
  describe("fromJson") {
    withData(
      listOf(
        Pair("""{"eventType":"appReady"}""", AppReadyMessage),
        Pair(
          """{"eventType":"newPrompt", "content": "test content"}""",
          NewPromptMessage("test content")
        ),
        Pair(
          """{"eventType":"trackFeedback", "content": "feedback", "data": {"extendedTextFeedback": "choices", "feedbackChoices": []}}
          """.trimMargin().removeNewlines(),
          TrackFeedbackMessage("feedback", TrackFeedbackMessage.Data("choices", emptyList()))
        )
      )
    ) {
      withClue("returns the payload for ${it.second.javaClass} from the json") {
        ChatViewMessage.fromJson(it.first) shouldBe it.second
      }
    }
  }

  describe("toJson") {
    val timestamp = buildLocalDateTime()
    val id = UUID.randomUUID().toString()
    val chunkId = 1
    val requestId = UUID.randomUUID().toString()
    val chatRecord = buildChatRecord(id = id, requestId = requestId, timestamp = timestamp, chunkId = chunkId)

    withData(
      listOf(
        Pair(
          NewRecordMessage(chatRecord),
          """{"eventType":"newRecord","record":{"id":"$id","role":"user","type":"general",
            |"requestId":"$requestId","state":"ready","timestamp":"$timestamp","chunkId":$chunkId,
            |"errors":[]}}
          """.trimMargin().removeNewlines()
        ),
        Pair(
          UpdateRecordMessage(chatRecord),
          """{"eventType":"updateRecord","record":{"id":"$id","role":"user","type":"general",
            |"requestId":"$requestId","state":"ready","timestamp":"$timestamp","chunkId":$chunkId,
            |"errors":[]}}
          """.trimMargin().removeNewlines()
        ),
      )
    ) {
      withClue("returns the json of the payload for ${it.first.javaClass}") {
        it.first.toJson() shouldBe it.second
      }
    }
  }
})

private fun String.removeNewlines() = replace("\n", "")
