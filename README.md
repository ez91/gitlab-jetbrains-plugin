# [GitLab Duo Plugin for JetBrains IDEs](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin)

This is the GitLab Duo plugin for JetBrains IDEs (IntelliJ, PyCharm, GoLand, Webstorm, Rubymine, etc).

If you have feedback or bug reports,
[create an issue](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/new).

## Version compatibility

The GitLab Duo plugin for JetBrains IDEs supports Code Suggestions for both
[GitLab SaaS](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/saas.html)
and [GitLab self-managed](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/self_managed.html).

Requirements:

- JetBrains IDEs: **2023.2.X** and later.
- The GitLab Duo Code Suggestions feature requires GitLab version 16.8 or later.

For version compatibility between versions of the plugin, IDE, and GitLab, see this table:

| Plugin version | IDE version                                    | GitLab version | Note |
|----------------|------------------------------------------------|----------------|------|
| 2.0.0 - 2.0.x  | 2023.3 (`233.11799`) - 2024.1 (`241.*`)     | 16.8 - 17.x    | Not compatible with GitLab instances below 16.8. |
| 1.0.0 - 1.0.x  | 2023.3 (`233.11799`) - 2024.1 (`241.*`)     | 16.8 - 17.x    | Not compatible with GitLab instances below 16.8. |
| 0.5.0 - 0.5.x  | 2023.3 (`233.11799`) - 2024.1 (`241.*`)     | 16.1 - 17.x    |      |
| 0.4.0 - 0.4.x  | 2023.2.2 (`232.9921`) - 2023.2.x (`232.*`)     | 16.1 - 17.x    | Not compatible with IDEs 2023.3 or later due to changes in the IDE's inline completion APIs. |
| 0.2.0 - 0.3.x  | 2023.2 (`232.7754.73`) - 2023.2.1 (`232.9559.62`) | 16.1 - 17.x    | Not compatible with IDEs 2023.2.2 or later due to a [change in IDE APIs](https://github.com/JetBrains/intellij-community/commit/c9468002a855d6ae91bc59304912e4202a03ac70#diff-eb3ee17c6a258543cd7789ffae05eb37409a5278668e2ceb16980ebde7b05258). |
| 0.1.1 - 0.1.3  | 2023.2 (`232.7754.73`) - 2023.2.1 (`232.9559.62`) | 16.1 - 16.2    | Not compatible with GitLab 16.3 or later due to a change in the model endpoint ([#51](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/51), [#82](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/82)). |

## Setup

This extension requires you to create a GitLab personal access token, and assign it to the extension:

Prerequisites:

- You have created a project in JetBrains.
- Code Suggestions is enabled for your instance or top-level group. See instructions
  [for SaaS](https://docs.gitlab.com/ee/user/group/manage.html#enable-code-suggestions-for-a-group)
  and for
  [self-managed](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/self_managed.html) installations.
- You have created a [GitLab personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) with the `api` scope.

To do this:

1. Install [the GitLab Duo extension](https://plugins.jetbrains.com/plugin/22325-gitlab-duo)
   from the JetBrains Plugin Marketplace. (`Settings` -> `Plugins`) and enable it.
1. To enable the extension in your IDE:
   1. Go to your IDE's top menu bar and select **Settings**.
   1. On the left sidebar, select **Plugins**. Find the **GitLab Duo** plugin, and select **Install**.
   1. Select **OK**.
1. To configure the extension in your IDE:
   1. Go to your IDE's top menu bar and select **Settings**.
   1. On the left sidebar, select **Tools > GitLab Duo**.
   1. For **GitLab URL**, provide the URL of your GitLab instance. For GitLab SaaS, use `https://gitlab.com`.
   1. For **Access Token**, paste in the personal access token you created. The token is not displayed, nor is it accessible to others.
   1. Select **Verify setup**.
   1. Select **OK**.

### Install a pre-release build

Pre-release builds of the plugin are published to the
[`Alpha` release channel](https://plugins.jetbrains.com/plugin/22325-gitlab-duo/edit/versions/alpha)
in JetBrains Marketplace. To install a pre-release build, either:

- Download the build from JetBrains Marketplace and
  [install it from disk](https://www.jetbrains.com/help/idea/managing-plugins.html#install_plugin_from_disk).
- [Add the `alpha` plugin repository](https://www.jetbrains.com/help/idea/managing-plugins.html#add_plugin_repos)
  to your IDE. For the repository URL, use `https://plugins.jetbrains.com/plugins/alpha/list`.

For a video tutorial of this process, see
[Install alpha releases of the GitLab Duo plugin for JetBrains](https://www.youtube.com/watch?v=Z9AuKybmeRU)
on the GitLab Unfiltered YouTube channel.
<!-- Video published on 2024-04-04 -->

## Features

### Code Suggestions

Write code more efficiently by using generative AI to suggest code while you're developing. To learn more about this feature, see the
[Code Suggestions documentation](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html). With Code Suggestions, you get:

- Code Completion, which suggests completions for the current line you are typing. These suggestions usually have low latency.
- Code Generation, which generates code based on a natural-language code comment block. Responses for code generation are slower than completion and returned in a single block.

Code Suggestions is a generative artificial intelligence (AI) model. GitLab selects the best-in-class large-language models for specific tasks. We use [Google Vertex AI Code Models](https://cloud.google.com/vertex-ai/docs/generative-ai/code/code-models-overview) and [Anthropic Claude](https://www.anthropic.com/product) for Code Suggestions.

No new additional data is collected to enable this feature. Private non-public GitLab customer data is not used as training data.
Learn more about [Code Suggestion data usage](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/#code-suggestions-data-usage).

Users should read about the [known limitations](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#known-limitations).

#### Supported Languages

Languages supported by GitLab Duo Code Suggestions can be found in the [documentation](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#supported-languages).

#### Usage

- `Tab` accepts suggestion
- `Escape` dismisses suggestion

#### Status Bar

A status icon is displayed in the status bar. It provides the following:

1. When token is not entered, disabled status icon will be shown.
1. When token is entered, enabled status icon will be shown.
1. When code suggestions tries to fetch suggestions, loading status icon will be shown.
1. When there is any exception in background, error status icon will be shown.

#### Reporting issues or providing feedback

If you encounter an error while using GitLab Duo, you can report it with your IDE's
built-in error reporting tool:

1. To access the tool, either:
   - When an error occurs, in the error message, select **See details and submit report**.
   - In the status bar, on the bottom right, select the exclamation mark.
1. In the **IDE Internal Errors** dialog, describe the error.
1. Select **Report and clear all**.
1. Your browser opens a GitLab issue form, pre-populated with debug information.
1. Follow the prompts in the issue template to fill out the description, providing
   as much context as you can.
1. Select **Create issue** to file the bug report.

For other issues or general
feedback, [open an issue](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/new)
with the `Bug` or `Feature Proposal` template.

#### Enabling experiment or BETA features

1. Go to your IDE's top menu bar and select **Settings**. You can also press:
   - <kbd>⌘</kbd>+<kbd>,</kbd> on macOS
   - <kbd>Control</kbd>+<kbd>Alt</kbd>+<kbd>S</kbd> on Windows or Linux
1. Select **Tools > GitLab Duo**.
1. Select **Enable Experiment or BETA features**.
1. To apply the changes, restart your IDE.

## Roadmap

To learn more about this project's team, processes, and plans, see
the [Create:Editor Extensions Group](https://handbook.gitlab.com/handbook/engineering/development/dev/create/editor-extensions/)
page in the GitLab handbook.

For a list of all open issues in this project, see the
[issues page](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/)
for this project.

## Troubleshooting

### Enable debug mode

To enable debug logs:

1. On the top bar, go to **Help > Diagnostic Tools > Debug Log Settings**, or
   search for the action by going to **Help > Find Action > Debug log settings**.
1. Add the following line: `com.gitlab.plugin`
1. Select **OK**.

The debug logs are available in the `idea.log` log file.

### Ignore certificate errors

To troubleshoot connecting to a GitLab instance through a Deep Packet Inspection/TLS intercepting proxy:

1. Refer to JetBrains documentation on [SSL certificates](https://www.jetbrains.com/help/idea/ssl-certificates.html).
1. Go to your IDE's top menu bar and select **Settings**.
1. On the left sidebar, select **Tools > GitLab Duo**.
1. Confirm your default browser trusts the **URL to GitLab instance** you're using.
1. Enable the **Ignore certificate errors** option.
1. Select **Verify setup**.
1. Select **Apply**.
1. Select **OK**.

---

## Contributing

This extension is open source and [hosted on GitLab](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin). Contributions are more than welcome and subject to the terms set forth in [CONTRIBUTING](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/blob/main/CONTRIBUTING.md) -- feel free to fork and add new features or submit bug reports. See [CONTRIBUTING](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/blob/main/CONTRIBUTING.md) for more information.
