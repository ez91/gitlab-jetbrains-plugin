# Changelog

## [Unreleased]

### Added

- Add health check section in the plugin settings ([!429](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/429))

### Changed

### Removed

### Fixed

- Remove trailing slashes after configured host URL to avoid 404 Not Found while executing ChatSubscription. ([440!](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/440))

## [2.1.1] - 2024-05-22

### Added

- Shift the focus on the DuoChat window when invoking DuoChat actions ([!420](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/420))
- Display user notifications where errors are occurring ([!427](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/427))
- Indent and format code suggestions based on the code style settings ([!410](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/410))

## [2.1.0] - 2024-05-15

### Added

- Add GitLab Duo: Accept Code Suggestion shortcut under the GitLab Duo group ([!415](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/415))
- Integrate with 1Password CLI to provide personal access tokens ([!357](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/357), [Learn more](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/149736))
- Display indicator in the Gutter for code suggestions ([!423](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/423))

## [2.0.3] - 2024-05-08

### Added

- Show/hide the Duo Chat tool window with the new <kbd>alt</kbd>+<kbd>D</kbd> keyboard shortcut ([!398](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/398))
- Add Beta label to GitLab Duo settings when running pre-release builds ([!390](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/390))
- Display balloon notifications to alert users when a personal access token validation fails ([!391](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/391))

### Changed

- Group Duo Chat and Code Suggestions settings under **Settings > GitLab Duo > Features** ([!403](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/403))

### Fixed

- Token updates no longer require the user to reopen the project ([!394](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/394))
- Under **Settings > GitLab Duo > Advanced**, selecting **Enable Duo Chat** now displays or hides the chat tool window ([!380](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/380))

## [2.0.2] - 2024-04-18

### Fixed

- Toggling Duo Chat features in the settings panel now displays or hides context menu actions ([!380](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/380))

## [2.0.1] - 2024-04-18

### Fixed

- Update GitLab Duo actions prefix from `DuoChat` to `GitLab Duo Chat` ([!377](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/377))

## [2.0.0] - 2024-04-17

### Added

- Add the new GitLab Duo Chat tool window ([!237](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/237))
- Add right-click actions for the /refactor, /explain, and /tests Chat commands  ([!260](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/260))
- Add configurable key maps for the /refactor, /explain, and /tests Chat commands ([!260](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/260))
- Disable the status in the GitLab Duo icon tooltip when the opened project has GitLab Duo disabled ([!334](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/334))
- Display a notification when the opened project has GitLab Duo disabled ([!323](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/323))
- Set up a JetBrains Marketplace release channel for preview builds ([#224](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/224))
- Allow for switching between light and dark themes in the Chat view ([!328](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/328))
- Add support for ignoring certificate errors which are _expected_ by user with Deep Packet Inspection/TLS intercepting
  proxies. ([!341](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/341))

  Users should verify their specific GitLab SSL configuration, and attempt to use the JetBrains and system certificate
  settings before using this option.
- Check PAT scope when opening a project or verifying setup, and show a notification if scopes are missing. ([#203](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/203))

### Changed

- Update plugin setup instructions to use the `api` PAT scope ([!313](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/313)). This scope is required for the upcoming Duo Chat feature.

### Fixed

- Specify the action update thread to be EDT for ChatActionGroup and OpenChatAction. ([!353](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/353))
- Allow token verification to be done without opening a project. ([!362](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/362))
- Support Personal Access Tokens with only `api` scope ([!349](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/349))

  Previously including the `ai_features` scope was required for Duo Code Suggestions by the plugin.
- Refresh API clients base URL when GitLab Duo settings are updated. ([!369](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/369))

## [1.0.1] - 2024-02-23

### Fixed

- Handle 404 response during setup verification for GitLab.com ([!300](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/300))

## [1.0.0] - 2024-02-14

### Added

- Code Suggestions now requires a [Duo Pro](https://about.gitlab.com/gitlab-duo/) license. Display Code Suggestions [license](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/#required-subscription) provisioning status in Duo icon tooltip ([#191](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/191))
- Code Suggestions now requires GitLab version 16.8 or later. Display warning in GitLab Duo icon tooltip for unsupported instances. ([#189](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/189))

### Changed

- Update setup instructions to include `read_user` PAT scope ([!275](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/275))

## [0.5.6] - 2024-01-23

### Added

- Support for Code Suggestions in `sh`, `html`, and `css` files ([#136](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/136))
- Support for 2024.1 EAP IDEs ([#182](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/182))

## [0.5.5] - 2024-01-10

### Fixed

- Improve error message shown when user verifies settings ([#134](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/134))
- Verify Setup in settings fails when switching GitLab instance URLs ([#133](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/133))

## [0.5.4] - 2023-12-14

### Fixed

- Fix bug where reporting an error sometimes led to a 400 Bad Request error page ([#138](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/138))

## [0.5.3] - 2023-12-07

### Changed

- Require a slightly newer IDE build to prevent installation in incompatible IDEs such as
  RustRover ([#149](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/149))

## [0.5.2] - 2023-12-04

### Changed

- Open GitLab issue form when error reports are submitted in the
  IDE ([!184](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/184))
- Reduce debounce delay for code
  suggestions ([!207](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/207))

## [0.5.1] - 2023-11-13

### Changed

- Increase request and socket timeouts to accommodate longer
  requests ([!181](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/181))

## [0.5.0] - 2023-11-03

### Changed

- Update plugin to be compatible with 2023.3 EAP (2023.2 and earlier IDEs are not supported by this
  version) ([!152](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/152))

## [0.4.3] - 2023-10-30

### Changed

- Update GitLab Duo logo ([#107](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/107))

## [0.4.2] - 2023-09-28

### Changed

- Improve exception notification
  messages ([!121](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/121))
- Expand supported file extensions for code
  suggestions ([!136](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/136))

## [0.4.1] - 2023-09-20

### Changed

- Added debounce delay of 1 second in suggestions request to avoid suggestions during
  typing ([#77](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/77))
- Only request suggestion if at end of line or after the cursor are only special
  characters ([#96](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/96))

## [0.4.0] - 2023-09-15

### Added

- Add settings to enabled/disable auto-completion
  ([#48](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/48))

### Changed

- Improves error message when server instance is not configured correctly
  ([#88](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/88))
- Improve the Settings UI ([#92](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/92))
- Update plugin to be compatible with 2023.2.2 IDEs (previous versions are not
  supported) ([!111](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/111))

## [0.3.0] - 2023-09-07

### Added

- Uses the IDE proxy settings if they are configured
  ([#74](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/74))

### Fixed

- Plugin should not require a restart when installing or toggling its
  visibility ([#85](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/85))

## [0.2.0] - 2023-09-04

### Added

- Adds button to verify setup of GitLab Code Suggestions
  ([#63](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/63))

### Changed

- Avoids making a request for suggestion if there's not enough content
  ([#83](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/83))

### Fixed

- Suggestions can be requested from GitLab servers at version 16.3.0-ee
  ([#82](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/82))

## [0.1.3] - 2023-08-28

### Added

- Add setting for users to control code suggestions
  telemetry ([#67](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/67))

### Changed

- Improves UX on setting up Duo Plugin
  ([#63](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/63))
- Uses GitLab API endpoint if GitLab server version is 16.3 or above
  ([#51](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/51))
- Give priority to code suggestion over built-in intellisense while pressing tab key
  ([#71](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/71))
- Update supported file
  extensions ([#58](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/58))

### Fixed

- Ensure plugin restart when installing or
  updating ([#70](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/70))
- Partially fixes suggestions crashing when triggered from within parenthesis and brackets
  ([#75](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/75))

## [0.1.2] - 2023-08-15

### Changed

- Rename plugin to GitLab
  Duo ([#55](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/55))

### Fixed

- Status indicator is not getting refreshed while adding/removing
  token ([#36](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/36))
- GitLab Instance URL with dash(es) not accepted by the
  plugin ([#59](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/59))
  by [@Prezu](https://gitlab.com/Prezu)
- Handle offline exceptions and show them in the
  UI ([!41](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/41))
- Plugin breaks in some IDEs due to wrong
  imports ([#61](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/61))

## [0.1.1] - 2023-07-21

### Added

- Settings screen to configure a GitLab instance and credentials for the Plugin
- Status bar icon to track configuration and busy/ready state
- GitLab Duo Code Suggestions initial integration

[Unreleased]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.1.1...HEAD
[2.1.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.1.0...v2.1.1
[2.1.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.0.3...v2.1.0
[2.0.3]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.0.2...v2.0.3
[2.0.2]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.0.1...v2.0.2
[2.0.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.0.0...v2.0.1
[2.0.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v1.0.1...v2.0.0
[1.0.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.5.6...v1.0.0
[0.5.6]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.5.5...v0.5.6
[0.5.5]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.5.4...v0.5.5
[0.5.4]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.5.3...v0.5.4
[0.5.3]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.5.2...v0.5.3
[0.5.2]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.5.1...v0.5.2
[0.5.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.4.3...v0.5.0
[0.4.3]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.4.2...v0.4.3
[0.4.2]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.1.3...v0.2.0
[0.1.3]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/commits/v0.1.1
