# Debugging setup guide

When debugging, you should follow these steps:

1. [Set a port value](#set-a-port-value).
1. [Set up internal mode](#set-up-internal-mode).
1. [Create `idea.properties` in the plugin sandbox](#create-ideaproperties-in-the-plugin-sandbox).

## Set a port value

Chrome DevTools are included in JCEF browsers in JetBrains IDEs. The DevTools are
active by default, but the port must be configured to attach to the Chrome DevTools client.
To change the default port:

1. Go to **Help > Find Action** and enter `Registry`.
1. Search for `ide.browser.jcef.debug.port` and set the port accordingly. You can use the
   default option (`-1`) if you don't need a specific port.

For further information on setting port values, see the official JetBrains documentation
for [JCEF Debugging](https://plugins.jetbrains.com/docs/intellij/jcef.html#debugging).

## Set up internal mode

After the port is set, configure internal mode:

1. In the official JetBrains documentation, follow the instructions in
   [Enabling Internal Mode](https://plugins.jetbrains.com/docs/intellij/enabling-internal.html).

1. After restarting, run the plugin to test if internal mode has been set while in the plugin sandbox:

   1. In most cases, the `idea.properties` file settings do not persist across the local and sandbox instances.

   1. To test this, open **Help > Find Action**, and search for `Edit Custom Properties`.
      If the file exists and is correct, these properties are set:

      ```kotlin
        idea.is.internal=true
        ide.browser.jcef.contextMenu.devTools.enabled=true
        ide.browser.jcef.debug.port=-1
      ```

1. Restart the IDE.
1. To confirm you can access DevTools, right-click on the webview, and select **Open DevTools**.

### Create `idea.properties` in the plugin sandbox

An error like this displays if the `idea.properties` file does not exist:

```plaintext
  Cannot write to file ~/Library/Application Support/JetBrains/IdeaIC2023.3/idea.properties:
  File does not exist: ~/Library/Application Support/JetBrains/IdeaIC2023.3/idea.properties
```

To fix the error:

1. In your terminal window, go to the `JetBrains` subdirectory under `Application Support`:

   ```shell
   cd ~/Library/Application\ Support/JetBrains/
   ```

1. In the error message, find the subdirectory inside the `JetBrains` directory. In the
   example, it was `IdeaIC2023.3`. Use that value to create the subfolder, and go to it:

   ```shell
   mkdir IdeaIC2023.3
   cd IdeaIC2023
   ```

1. In the folder, create a file called `idea.properties`:

   ```shell
   touch ./idea.properties
   ```

1. To access the file, go to **Help > Find Action** and enter `Edit Custom Properties`.
1. Add the following properties to the `idea.properties` file:

   ```kotlin
   idea.is.internal=true
   ide.browser.jcef.contextMenu.devTools.enabled=true
   ide.browser.jcef.debug.port=-1
   ```

1. Restart the IDE.
1. To confirm you can access DevTools, right-click on the webview, and select **Open DevTools**.
