# Development environment setup

Use these instructions to run the GitLab plugin locally.

1. [Prerequisites](#prerequisites).
1. [Fork and clone the project](#fork-and-clone-the-project).
1. [Run the plugin](#run-the-plugin).
1. [Run tests](#run-tests).
1. [Add documentation](#add-documentation).
1. [Update the project](#update-the-project).
1. [Use Snowplow Micro](#use-snowplow-micro).

## Prerequisites

- You have [created a GitLab account](https://gitlab.com/users/sign_up). It is free and it is awesome!
- You have [IntelliJ IDEA CE](https://www.jetbrains.com/idea/download)
  or [IntelliJ IDEA Ultimate](https://www.jetbrains.com/idea/download) installed.
- You have the [Gradle](https://www.jetbrains.com/help/idea/gradle.html) plugin for JetBrains installed
  and configured.
- You have Node.js installed (version specified in [`.tool-versions`](.tool-versions)).
- You have JDK 17 (Eclipse Temurin) installed in your IntelliJ
- You have the following plugins installed in IntelliJ:
  - [detekt](https://plugins.jetbrains.com/plugin/10761-detekt)
  - [GraphQL](https://plugins.jetbrains.com/plugin/8097-graphql)
  - [Kotest](https://plugins.jetbrains.com/plugin/14080-kotest)
  - [Plugin DevKit](https://plugins.jetbrains.com/plugin/22851-plugin-devkit)

## Fork and clone the project

1. Use your GitLab account to
   [fork this project](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/forks/new).
   Need help? See how to [fork a project](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html).
1. Go to your forked project. The URL should be similar to
   `https://gitlab.com/<username>/gitlab-jetbrains-plugin`.
1. On the right-hand side of the page, select **Clone**. Copy the SSH or HTTPS URL to clone the project to your local machine.
   Need help? See this [guide to cloning a project](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository).
1. In IntelliJ IDEA,
   [open your cloned project](https://www.jetbrains.com/help/idea/import-project-or-module-wizard.html#open-project).

## Run the plugin

To run the plugin in IntelliJ IDEA:

1. In IntelliJ IDEA, on the navigation bar, select **Run plugin**.
1. In the dropdown list, under **Run Configuration**, select **Run Plugin**.

## Run tests

To run tests in IntelliJ IDEA:

1. In IntelliJ IDEA, on the navigation bar, select **Run plugin**.
1. In the dropdown list, under **Run Configuration**, select **Run Tests**.

To run the end-to-end tests, see [GitLab Duo JetBrains plugin end-to-end tests](end_to_end_testing.md).

## Add documentation

If you add a new feature or change an existing feature, document it in the README.

To add documentation that includes a new image:

1. Add images into the `docs/assets` folder, and commit the changes.
1. Edit the README file, and insert full permalinks to your new images.
   The permalinks contain the commit SHA from your first commit, and are
   in the form of:

   ```plaintext
   https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/raw/<COMMIT_SHA>/docs/assets/imagename.png
   ```

1. Commit your text changes.

## Update the project

1. In IntelliJ IDEA, go to the main menu.
1. Select [**Git > Update Project**](https://www.jetbrains.com/help/idea/sync-with-a-remote-repository.html#update).
1. In the **Update Project** dialog, select the update strategy:

    - To merge during the update, select **Merge the incoming changes into the current branch**.
      This is equivalent to running `git fetch` and then `git merge`, or `git pull --no-rebase`.
    - To rebase during the update, select **Rebase the current branch on top of the incoming changes**.
      This is equivalent to running `git fetch` and then `git rebase`, or `git pull --rebase`. All local commits
      are placed on top of the updated upstream `HEAD`.

1. Optional. Some updates modify sensitive build scripts, and you must synchronize
   your IDE with the latest changes. Your IDE should prompt you to synchronize, or do it for you.
   To ensure the project is rebuilt correctly, consider running `./gradlew clean`.

   For more information, see
   [IDEA - Reload a linked Gradle project](https://www.jetbrains.com/help/idea/work-with-gradle-projects.html#gradle_refresh_project).

## Use Snowplow Micro

Before following the instructions below, please ensure that Docker is installed and working. This walkthrough has
been tested using [Rancher Desktop](https://rancherdesktop.io/). For other Docker alternatives, please visit
this [handbook page](https://handbook.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop).

If you are confirming any changes to the Snowplow metrics or Iglu schemas, you will likely need to have [Snowplow micro](https://docs.snowplow.io/docs/testing-debugging/snowplow-micro/what-is-micro/)
configured for testing purposes. This can be done in one of two ways:

1. [Configure Snowplow micro using the GitLab-Development-Kit (GDK)](https://docs.gitlab.com/ee/development/internal_analytics/internal_event_instrumentation/local_setup_and_debugging.html)
1. [Spin up a dockerized version of Snowplow micro](https://gitlab.com/gitlab-org/snowplow-micro-configuration)

In order to redirect Snowplow to send metrics to your local machine, the `SNOWPLOW_COLLECTOR_URL` and
`SNOWPLOW_EMITTER_BATCH_SIZE` environment variables should be set to `http://localhost:9090` and
`1` respectively when building the plugin. These variables are already set for the `Run Plugin` task.

After building the plugin with these variables set, follow the instructions in whichever way you have decided to spin up
Snowplow micro locally.
