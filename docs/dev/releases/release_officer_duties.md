# JetBrains Release Officer Duties

This documentation exists to describe the responsibilities of a Release Officer for the JetBrains plugin.

A JetBrains Release Officer has three main duties:

1. [Creating the Release Schedule](#creating-the-release-schedule)
1. [Creating Releases](#creating-releases)
1. [Establishing the Next Release Officer](#establishing-the-next-release-officer)

## Creating the Release Schedule

The Release Officer is responsible for creating the release schedule and including the release schedule in the planning
issue for the milestone.

As a general rule:

1. A stable JetBrains release will be created the 2nd and 4th Tuesday of the milestone.

If a Release Officer is unable to create a release in line with the schedule that is created for the milestone, post in
the `#f_jetbrains_plugin` to assign someone. Make sure the schedule reflects the reassignment of responsibilities.

The format for the schedule looks like (this example is taken from [planning issue for 17.1](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/338#note_1891499969)):

| Date         | Channel | Status         | Release Officer | Comments |
|--------------|---------|----------------|-----------------|----------|
| 15/May/2024  | Alpha   | ❌ **Skipped**  | @gitlabUser    | No major updates, release not created. |
| 22/May/2024  | Stable  | ✅ **Released** | @gitlabUser    | [Marketplace stable version 2.0.2](https://plugins.jetbrains.com/plugin/22325-gitlab-duo/edit/versions/stable/524319) |
| 28/May/2024  | Alpha   |                | @gitlabUser     |          |
| 04/June/2024 | Stable  |                | @gitlabUser     |          |

## Creating Releases

With a release officer and release cadence established, it is time to create a release.

1. Once the extension is ready for a release, create a calendar event and invite any stakeholders or release officer shadows (if applicable).
1. When ready to create the release:
   1. Post in the `#f_jetbrains_plugin` Slack channel that a new release is being created
      - Make sure the MR including the version increment is in the message: [Instructions to increment the version](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/blob/main/docs/dev/releases/release_process.md?ref_type=heads#increment-the-version).
   1. Follow the steps for the [Alpha Release Process](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/blob/main/docs/dev/release_process.md?ref_type=heads#create-a-new-pre-release-build) or [Stable Release Process](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/blob/main/docs/dev/release_process.md?ref_type=heads#create-a-new-stable-build)
   1. Update the Slack thread with the release.
   1. Update the milestone release schedule with the required details.

## Establishing the Next Release Officer

Each milestone a Release Officer will be chosen to create releases for the JetBrains plugin. The release officer can be
a volunteer or selected by the Release Officer from the previous milestone.

A Release Officer should:

- be involved with a prior release as a shadow release officer
- be a maintainer of the project (or have a maintainer available at the time of creating the release to merge)
- have contributed to the JetBrains plugin during the previous milestone

Once a Release Officer has been selected, point them to this documentation for learning their responsibilities.
