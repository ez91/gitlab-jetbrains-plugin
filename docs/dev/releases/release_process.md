# JetBrains GitLab Duo Plugin releases

This process describes the steps for a successful release for the JetBrains GitLab Duo plugin. If any questions come
up during this process, reach out in the `#f_jetbrains_plugin` Slack channel.

## Increment the version

1. Determine if the release is a `major`, `minor`, or `patch`:
   1. Releasing a new major feature, like Code Suggestions, GitLab Duo Chat? Perform a `major` version increment.
   1. Check all commit titles since the last release with this command:

      ```shell
      git log --format='%s' $(git describe --abbrev=0 --tags HEAD)..HEAD | grep -v 'Merge branch'
      ```

      - Do any commit messages contain `feat:`? Perform a `minor` version increment.
      - If none do, perform a `patch` version increment.
1. Create a merge request to increment the version (in `gradle.properties`, `plugin.version`)
   greater than last stable version. For an example, see
   [!399](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/399).

## Create a new pre-release build

1. In GitLab, on the left sidebar, select **Search or go to** and find your project.
1. Select **Build > Pipelines**.
1. In **Filter pipelines**, filter by `Branch name`, and find the latest pipeline for the `main` branch.
1. On that pipeline, select **Run manual or delayed jobs**, then select the `plugin:publish-alpha` job.

The job builds the plugin and publishes it to the
[`Alpha` release channel](https://plugins.jetbrains.com/plugin/22325-gitlab-duo/edit/versions/alpha)
in JetBrains Marketplace.

## Create a new stable build

### Create a new release

Prerequisites:

- Go through merged MRs to get an understanding of what should be in the `CHANGELOG` for this release.
- Ensure that the changes described reflect those in the `CHANGELOG`.

To do this:

1. [Create the branch and merge request](#create-the-branch-and-merge-request).
1. [Tag the new version](#tag-the-new-version).
1. [Upload the plugin](#upload-the-plugin).

#### Create the branch and merge request

1. Create a branch with a name similar to `yourname/prepare-0.5.5-release` that contains these changes:

   - In `gradle.properties`, ensure that the `plugin.version` number is incremented above the last stable version,
     according to the [Semantic Version 2.0](https://semver.org/) guidelines.
   - Move the changes in `CHANGELOG.md` from the `[Unreleased]` section to
      the section for the new version with this command:

      ```shell
      ./gradlew patchChangelog
      ```

   1. Review the changes to `CHANGELOG.md`.
      - Do the changes look sensible?
      - Do the links work?
      - Is the versioning correct for the intended release. See
        [Keep a Changelog](https://keepachangelog.com/en/1.1.0/) for format examples.
   1. Ensure that the Version Compatibility table in `README.md` is up-to-date.
   1. Review the plugin's marketplace description in `src/main/resources/META-INF/plugin.xml`.
     - Are any major features being released which should be featured?
   1. Create a merge request containing these changes.
   1. Optional. Ask the Editor Extensions PM if anything else should be included in the changelog.
   1. Request a docs review from the Editor Extensions Tech Writing
      [stable counterpart](https://handbook.gitlab.com/handbook/engineering/development/dev/create/editor-extensions/#stable-counterparts).
      If they're unavailable request a review in the `#docs` Slack channel.

### Tag the new version

Prerequisites:

- You are a code owner or maintainer for the [GitLab Duo Plugin for JetBrains](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin) project.
  If you are not, ask in `#f_jetbrains_plugin` for help.

After the merge request is merged:

1. Tag the merged commit with the new version:

   ```shell
   # Tag the release version with the same version as in the gradle.properties file
   git tag v0.5.5

   # Push tags:
   git push origin v0.5.5
   ```

   These commands send a Slack message to the `#f_jetbrains_plugin` channel, saying
   that the release is now in progress.

1. In the GitLab UI, confirm the tag was added correctly:

   1. On the left sidebar, select **Search or go to** and find this project.
   1. Select **Code > Tags**, and confirm your new tag exists.
   1. Confirm that your tag has a pipeline in progress, or completed.

1. After the tag pipeline finishes, select the pipeline status icon to view the jobs
   in this pipeline.
1. After the preliminary pipeline steps finish successfully, select **Play** on
   the `plugin:publish` job in the `release` stage to start it manually.
1. After the tag pipeline finishes, on the left sidebar, select **Deploy > Releases** and find your release.
1. From the release, download `gitlab-jetbrains-plugin-0.5.5.zip` under `Assets > Packages`
1. Install the plugin locally and test to ensure it works properly.

### Upload the plugin

Prerequisites:

- You have permission to upload the new version of the plugin to the JetBrains marketplace.
  If you do not, ask in `#f_jetbrains_plugin` for help.

If the plugin works as expected:

1. Go to the [JetBrains marketplace](https://plugins.jetbrains.com/) and search for `GitLab Duo`.
1. In the upper right corner of the page, select **Upload Update** and upload the `.zip`
   file from the GitLab release.
1. In the JetBrains marketplace, take a screenshot of the release information.
1. In Slack, go to the `#f_jetbrains_plugin` channel, and find the Slack thread
   containing the auto-generated release message.
1. In the thread, include:
   - A link to the GitLab release.
   - The release notes.
   - The screenshot of the release information from the JetBrains marketplace.

The new version is submitted for moderation, which takes up to two business days. During this time, the version is not
available for users to download. The newly uploaded version is shown in the [versions tab of the JetBrains Marketplace listing](https://plugins.jetbrains.com/plugin/22325-gitlab-duo/edit/versions).
The compatibility verification results are populated after 5-10 minutes.
