# Add developer to JetBrains GitLab Duo plugin

1. Go to the [plugin overview page](https://plugins.jetbrains.com/plugin/22325-gitlab-duo/edit).
1. Scroll to **Vendor Information** and click **Edit Section**.
1. Under **Developers**, enter the username of the person you want to add.
1. From the dropdown list, select the developer.
1. Select **Save**.
